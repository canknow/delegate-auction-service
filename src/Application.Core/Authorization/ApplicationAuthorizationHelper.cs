﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Application.Authorization.Users;

namespace Application.Authorization
{
    public class ApplicationAuthorizationHelper: ITransientDependency
    {
        public IAbpSession AbpSession { get; set; }
        public IRepository<User,long> UserRepository { get; set; }

        private User _currentUser = null;

        public User CurrentUser
        {
            get
            {
                if (_currentUser == null && AbpSession.UserId.HasValue)
                {
                    _currentUser=UserRepository.FirstOrDefault(model=>model.Id==AbpSession.UserId.Value);
                }
                return _currentUser;
            }
        }
    }
}
