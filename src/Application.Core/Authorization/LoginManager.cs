﻿using Microsoft.AspNetCore.Identity;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Zero.Configuration;
using Application.Authorization.Roles;
using Application.Authorization.Users;
using Application.MultiTenancy;
using System.Threading.Tasks;

namespace Application.Authorization
{
    public class LogInManager : AbpLogInManager<Tenant, Role, User>
    {
        public IRepository<User, long> UserRepository { get; set; }
        public TenantHelper TenantHelper { get; set; }

        public LogInManager(
            UserManager userManager, 
            IMultiTenancyConfig multiTenancyConfig,
            IRepository<Tenant> tenantRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ISettingManager settingManager, 
            IRepository<UserLoginAttempt, long> userLoginAttemptRepository, 
            IUserManagementConfig userManagementConfig,
            IIocResolver iocResolver,
            IPasswordHasher<User> passwordHasher, 
            RoleManager roleManager,
            UserClaimsPrincipalFactory claimsPrincipalFactory) 
            : base(
                  userManager, 
                  multiTenancyConfig,
                  tenantRepository, 
                  unitOfWorkManager, 
                  settingManager, 
                  userLoginAttemptRepository, 
                  userManagementConfig, 
                  iocResolver, 
                  passwordHasher, 
                  roleManager, 
                  claimsPrincipalFactory)
        {
        }

        public async Task<AbpLoginResult<Tenant, User>> LoginByPhoneNumberAsync(string phoneNumber, string tenancyName)
        {
            var tenant = TenantRepository.FirstOrDefault(model => model.TenancyName == tenancyName);
            UnitOfWorkManager.Current.SetTenantId(tenant.Id);

            User user = await UserRepository.FirstOrDefaultAsync(model => model.PhoneNumber == phoneNumber);
            if (user == null)
            {
                return new AbpLoginResult<Tenant, User>(AbpLoginResultType.InvalidUserNameOrEmailAddress, tenant);
            }

            if (await UserManager.IsLockedOutAsync(user))
            {
                return new AbpLoginResult<Tenant, User>(AbpLoginResultType.LockedOut, tenant, user);
            }

            await UserManager.ResetAccessFailedCountAsync(user);

            return await CreateLoginResultAsync(user, tenant);
        }

        public async Task<AbpLoginResult<Tenant, User>> LoginByProviderAsync(string providerName, string providerKey)
        {
            Tenant tenant = TenantHelper.GetTenant();
            UserLoginInfo userLoginInfo = new UserLoginInfo(providerName, providerKey, providerName);
            return await LoginAsync(userLoginInfo, tenant.TenancyName);
        }
    }
}
