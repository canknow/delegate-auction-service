﻿namespace Application.Authorization
{
    public static class AppPermissions
    {
        public static class Administration
        {
            public const string Value = "Pages.Administration";

            public static class Common
            {
                public const string Value = "Pages.Administration.Common";
                public const string OrganizationUnits = "Pages.Administration.Common.OrganizationUnits";
                public const string Users = "Pages.Administration.Common.Users";
                public const string Roles = "Pages.Administration.Common.Roles";
                public const string Files = "Pages.Administration.Common.Files";
                public const string Settings = "Pages.Administration.Common.Settings";
            }

            public static class Host
            {
                public const string Value = "Pages.Administration.Host";
                public const string Settings = "Pages.Administration.Host.Settings";
                public const string Tenants = "Pages.Administration.Host.Tenants";
                public const string Editions = "Pages.Administration.Host.Editions";
                public const string Maintenance = "Pages.Administration.Host.Maintenance";
                public const string AudtiLogs = "Pages.Administration.Host.AudtiLogs";
                public const string BackgroundJobs = "Pages.Administration.Host.BackgroundJobs";
            }

            public static class Tenant
            {
                public const string Value = "Pages.Administration.Tenant";
                public const string Settings = "Pages.Administration.Tenant.Settings";

                public const string Package = "Pages.Administration.Tenant.Package";
                public const string Order = "Pages.Administration.Tenant.Order";
                public const string Article = "Pages.Administration.Tenant.Article";

                public const string Finance = "Pages.Administration.Tenant.Finance";
                public const string PublicWechat = "Pages.Administration.Tenant.PublicWechat";
                public const string PublicWechatManager = "Pages.Administration.Tenant.PublicWechatManager";
            }
        }
    }
}