﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Application.Authorization
{
    public class ApplicationAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //Common permissions
            var administration = context.GetPermissionOrNull(AppPermissions.Administration.Value);

            if (administration == null)
            {
                administration = context.CreatePermission(AppPermissions.Administration.Value, L("Administration"));
            }

            var common = administration.CreateChildPermission(AppPermissions.Administration.Common.Value, L("Common"));
            common.CreateChildPermission(AppPermissions.Administration.Common.OrganizationUnits, L("OrganizationUnits"));
            common.CreateChildPermission(AppPermissions.Administration.Common.Users, L("Users"));
            common.CreateChildPermission(AppPermissions.Administration.Common.Roles, L("Roles"));
            common.CreateChildPermission(AppPermissions.Administration.Common.Files, L("Files"));
            common.CreateChildPermission(AppPermissions.Administration.Common.Settings, L("Settings"));

            var host = administration.CreateChildPermission(AppPermissions.Administration.Host.Value, L("Host"), multiTenancySides: MultiTenancySides.Host);
            host.CreateChildPermission(AppPermissions.Administration.Host.Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            host.CreateChildPermission(AppPermissions.Administration.Host.Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            host.CreateChildPermission(AppPermissions.Administration.Host.Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            host.CreateChildPermission(AppPermissions.Administration.Host.Maintenance, L("Maintenance"), multiTenancySides: MultiTenancySides.Host);
            host.CreateChildPermission(AppPermissions.Administration.Host.AudtiLogs, L("AudtiLogs"), multiTenancySides: MultiTenancySides.Host);
            host.CreateChildPermission(AppPermissions.Administration.Host.BackgroundJobs, L("BackgroundJobs"), multiTenancySides: MultiTenancySides.Host);

            var tenant = administration.CreateChildPermission(AppPermissions.Administration.Tenant.Value, L("Tenant"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.Package, L("Package"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.Order, L("Order"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.Article, L("Article"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.Finance, L("Finance"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.PublicWechat, L("PublicWechat"), multiTenancySides: MultiTenancySides.Tenant);
            tenant.CreateChildPermission(AppPermissions.Administration.Tenant.PublicWechatManager, L("PublicWechatManager"), multiTenancySides: MultiTenancySides.Tenant);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ApplicationConsts.LocalizationSourceName);
        }
    }
}
