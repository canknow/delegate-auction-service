﻿namespace Application.Authorization.Users
{
    public enum UserType
    {
        Admin,
        User,
        WeixinInteraction
    }
}
