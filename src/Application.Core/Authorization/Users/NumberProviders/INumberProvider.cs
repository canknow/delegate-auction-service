﻿using Abp.Dependency;

namespace Application.Authorization.Users.NumberProviders
{
    public interface INumberProvider: ISingletonDependency
    {
        string BuildNumber();
    }
}
