﻿using Abp.Events.Bus;
using System;

namespace Application.Authorization.Users.Events
{
    [Serializable]
    public class BindParentEventData : EventData 
    {
        public User SourceUser { get; private set; }
        public User ParentUser { get; private set; }

        public BindParentType BindParentType { get; set; }

        public BindParentEventData(User sourceUser, User parentUser, BindParentType bindParentType)
        {
            SourceUser = sourceUser;
            ParentUser = parentUser;
            BindParentType = bindParentType;
        }
    }
}
