using Abp.Dependency;
using Abp.Events.Bus.Handlers;
using Application.Authorization.Users.Events;
using Application.BusinessNotifiers;
using Application.Notifications;
using System.Threading.Tasks;

namespace Application.Authorization.Users.EventHandlers
{
    public class BindParentEventHandler : ApplicationDomainServiceBase, IAsyncEventHandler<BindParentEventData>, ISingletonDependency
    {
        public BusinessNotiferManager BusinessNotiferManager { get; set; }
        public IAppNotifier AppNotifier { get; set; }

        public virtual async Task HandleEventAsync(BindParentEventData eventData)
        {
            await AppNotifier.NewCustomerAsync(eventData.SourceUser, eventData.ParentUser, eventData.BindParentType);

            foreach (BusinessNotifier businessNotifier in BusinessNotiferManager.GetBusinessNotifiers())
            {
                await AppNotifier.NewCustomerForBusinessAsync(eventData.SourceUser, eventData.ParentUser, eventData.BindParentType, businessNotifier);
            }
        }
    }
}