﻿using Abp;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Events.Bus;
using Abp.Localization;
using Abp.Notifications;
using Abp.ObjectMapping;
using Abp.Organizations;
using Abp.Runtime.Caching;
using Abp.Threading;
using Abp.UI;
using Application.Authorization.Roles;
using Application.Authorization.Users.Events;
using Application.MultiTenancy;
using Application.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Authorization.Users
{
    public class UserManager : AbpUserManager<Role, User>
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public IObjectMapper ObjectMapper { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        private IEventBus EventBus;
        protected readonly IAppNotifier _appNotifier;
        protected readonly IUserEmailer _userEmailer;
        protected readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        protected ISettingManager _settingManager;

        public IRepository<UserLogin, long> UserLoginRepository { get; set; }
        public TenantManager TenantManager { get; set; }

        public UserManager(
            RoleManager roleManager,
            UserStore store, 
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<User> passwordHasher, 
            IEnumerable<IUserValidator<User>> userValidators, 
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            IServiceProvider services, 
            ILogger<UserManager<User>> logger, 
            IPermissionManager permissionManager, 
            IUnitOfWorkManager unitOfWorkManager, 
            ICacheManager cacheManager, 
            IRepository<OrganizationUnit, long> organizationUnitRepository, 
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository, 
            IOrganizationUnitSettings organizationUnitSettings, 
            ISettingManager settingManager,

            IEventBus eventBus,
            ILocalizationManager localizationManager,
            IAppNotifier appNotifier,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IUserEmailer userEmailer)
            : base(
                roleManager, 
                store, 
                optionsAccessor, 
                passwordHasher, 
                userValidators, 
                passwordValidators, 
                keyNormalizer, 
                errors, 
                services, 
                logger, 
                permissionManager, 
                unitOfWorkManager, 
                cacheManager,
                organizationUnitRepository, 
                userOrganizationUnitRepository, 
                organizationUnitSettings, 
                settingManager)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _settingManager = settingManager;
            EventBus = eventBus;
            _appNotifier = appNotifier;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _userEmailer = userEmailer;
        }

        private string L(string name)
        {
            return LocalizationManager.GetString(ApplicationConsts.LocalizationSourceName, name);
        }

        public string CreateDefaultUserEmail(string userName)
        {
            return userName + "@canknow.com";
        }

        public int GetUserCount()
        {
            return UserRepository.Count();
        }

        [UnitOfWork]
        public virtual async Task<User> GetUserOrNullAsync(UserIdentifier userIdentifier)
        {
            using (_unitOfWorkManager.Current.SetTenantId(userIdentifier.TenantId))
            {
                return await FindByIdAsync(userIdentifier.UserId.ToString());
            }
        }


        public User GetUserOrNull(UserIdentifier userIdentifier)
        {
            return AsyncHelper.RunSync(() => GetUserOrNullAsync(userIdentifier));
        }

        public async Task<User> GetUserAsync(UserIdentifier userIdentifier)
        {
            var user = await GetUserOrNullAsync(userIdentifier);

            if (user == null)
            {
                throw new ApplicationException(L("ThereIsNoUser") + userIdentifier);
            }
            return user;
        }

        public User GetUser(UserIdentifier userIdentifier)
        {
            return AsyncHelper.RunSync(() => GetUserAsync(userIdentifier));
        }


        public override async Task SetGrantedPermissionsAsync(User user, IEnumerable<Permission> permissions)
        {
            CheckPermissionsToUpdate(user, permissions);

            await base.SetGrantedPermissionsAsync(user, permissions);
        }

        private void CheckPermissionsToUpdate(User user, IEnumerable<Permission> permissions)
        {
            if (user.Name == AbpUserBase.AdminUserName &&
                (!permissions.Any(p => p.Name == AppPermissions.Administration.Common.Roles) ||
                !permissions.Any(p => p.Name == AppPermissions.Administration.Common.Users)))
            {
                throw new UserFriendlyException(L("YouCannotRemoveUserRolePermissionsFromAdminUser"));
            }
        }

        public async Task<User> GetUserByEmail(string emailAddress)
        {
            return await Users.Where(
               u => u.EmailAddress == emailAddress
               ).FirstOrDefaultAsync();
        }

        public async Task ManualBindParent(User sourceUser, User parentUser)
        {
            await BindParentAsync(sourceUser, parentUser, true, true, BindParentType.Manual);
            UserRepository.Update(sourceUser);
        }

        public async Task CheckCanBind(User sourceUser, User parentUser)
        {
            if (sourceUser.ParentUserId != null)
            {
                throw new AbpException(L("SourceUserHasParent"));
            }

            if (parentUser.ParentUserId == sourceUser.Id)
            {
                throw new AbpException(L("SourceUserIsTargetUserParent"));
            }

            if (sourceUser.Id == parentUser.Id)
            {
                throw new AbpException(L("SourceUserIsTargetUser"));
            }
        }

        [UnitOfWork]
        public async Task BindParentAsync(User sourceUser, User parentUser, bool throwException = false, bool forceBind = false, BindParentType bindParentType = BindParentType.ScanQrcode)
        {
            if (!forceBind)
            {
                try
                {
                    await CheckCanBind(sourceUser, parentUser);
                }
                catch (Exception e)
                {
                    if (throwException)
                    {
                        throw e;
                    }
                    return;
                }
            }

            if (sourceUser.ParentUserId.HasValue)
            {
                UserRepository.Update(sourceUser.ParentUser);
                sourceUser.ParentUserId = null;
                UserRepository.Update(sourceUser);
            }

            if (parentUser != null)
            {
                //source
                sourceUser.ParentUserId = parentUser.Id;
                UserRepository.Update(sourceUser);
                UserRepository.Update(parentUser);
            }
            await _appNotifier.NewCustomerAsync(sourceUser, parentUser, bindParentType);
            await EventBus.TriggerAsync(new BindParentEventData(sourceUser, parentUser, bindParentType));
        }

        public async Task BindParentAsync(User sourceUser, long parentUserId, bool throwException = false, bool forceBind = false, BindParentType bindParentType = BindParentType.ScanQrcode)
        {
            User parentUser = UserRepository.Get(parentUserId);
            await BindParentAsync(sourceUser, parentUser, throwException, forceBind, bindParentType);
        }

        public User GetParentUserOfDepth(User sourceUser, int depth)
        {
            if (sourceUser.ParentUserId.HasValue)
            {
                if (depth == 1)
                {
                    depth--;
                    return sourceUser.ParentUser;
                }
                else
                {
                    depth--;
                    return GetParentUserOfDepth(sourceUser.ParentUser, depth);
                }
            }
            else
            {
                depth--;
                return null;
            }
        }
    }
}
