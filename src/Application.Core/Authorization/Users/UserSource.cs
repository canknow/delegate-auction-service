﻿namespace Application.Authorization.Users
{
    public enum UserSource
    {
        System,
        Regist,
        ExternalLogin,
        WeixinInteraction
    }
}
