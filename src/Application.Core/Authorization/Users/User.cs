﻿using Abp.Authorization.Users;
using Abp.Extensions;
using Abp.Timing;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Application.Authorization.Users
{
    public enum WorkStatus
    {
        Busy,
        Free,
        Leave
    }

    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123456";
        public const string DefaultAvatar = "/Content/Images/avatar.png";
        public const int MinPlainPasswordLength = 6;
        public const int MaxNickNameLength = 32;
        public const int PhoneCodeLength = 4;

        public long? ParentUserId { get; set; }

        [ForeignKey("ParentUserId")]
        public virtual User ParentUser { get; set; }

        public bool IsSpreader { get; set; } = false;

        public DateTime? SignInTokenExpireTimeUtc { get; set; }

        public string SignInToken { get; set; }

        public UserType UserType { get; set; }

        public string Avatar { get; set; }

        public string Cover { get; set; }

        public string Say { get; set; }

        public virtual Guid? ProfilePictureId { get; set; }

        public string Number { get; set; }

        public string NickName { get; set; }

        public virtual bool ShouldChangePasswordOnNextLogin { get; set; }

        [Description("WhetherHideTheUserInTheRank")]
        public bool IsHide { get; set; }

        [Description("TheUserCreatedFrom")]
        public UserSource Source { get; set; }

        public string GetSourceText()
        {
            return Source.ToString();
        }

        public string LastLoginIp { get; set; }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public void Unlock()
        {
            AccessFailedCount = 0;
            LockoutEndDateUtc = null;
        }

        public static string CreateRandomUserName()
        {
            return Guid.NewGuid().ToString("N").Truncate(MaxUserNameLength);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                NickName = "Administrator",
                EmailAddress = emailAddress,
                Source = UserSource.System
            };
            user.SetNormalizedNames();

            return user;
        }

        public User()
        {
            Avatar = DefaultAvatar;
            Source = UserSource.Regist;
            IsLockoutEnabled = true;
            IsTwoFactorEnabled = true;
        }

        public void SetSignInToken()
        {
            SignInToken = Guid.NewGuid().ToString();
            SignInTokenExpireTimeUtc = Clock.Now.AddMinutes(1).ToUniversalTime();
        }
    }
}
