﻿namespace Application.Authorization.Users
{
    public enum BindParentType
    {
        ShareAccess,
        ScanQrcode,
        Admin,
        Manual,
    }
}
