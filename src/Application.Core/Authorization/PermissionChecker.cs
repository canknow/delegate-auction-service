﻿using Abp.Authorization;
using Application.Authorization.Roles;
using Application.Authorization.Users;

namespace Application.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
