﻿using System.Runtime.InteropServices;

namespace Application.SystemInfos
{
    public class SystemInfo
    {
        public string FrameworkDescription { get; set; }

        public Architecture OSArchitecture { get; set; }

        public string OSDescription { get; set; }

        public Architecture ProcessArchitecture { get; set; }
    }
}
