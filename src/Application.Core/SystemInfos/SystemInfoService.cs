﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Application.SystemInfos
{
    public class SystemInfoService
    {
        public SystemInfo GetSystemInfo()
        {
            var systemInfo = new SystemInfo()
            {
                FrameworkDescription = RuntimeInformation.FrameworkDescription,
                OSArchitecture = RuntimeInformation.OSArchitecture,
                OSDescription = RuntimeInformation.OSDescription,
                ProcessArchitecture = RuntimeInformation.ProcessArchitecture,
            };
            return systemInfo;
        }

        /// <summary>
        /// Solution borrowed from:
        /// https://github.com/dotnet/BenchmarkDotNet/issues/448#issuecomment-308424100
        /// </summary>
        private static string GetNetCoreVersion()
        {
            Assembly assembly = typeof(System.Runtime.GCSettings).GetTypeInfo().Assembly;
            string[] assemblyPath = assembly.CodeBase.Split(new[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
            int netCoreAppIndex = Array.IndexOf(assemblyPath, "Microsoft.NETCore.App");

            if (netCoreAppIndex > 0 && netCoreAppIndex < assemblyPath.Length - 2)
                return assemblyPath[netCoreAppIndex + 1];
            return "";
        }
    }
}
