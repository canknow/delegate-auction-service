﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Application.Authorization.Users;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Shares
{
    public class ShareManager : ApplicationDomainServiceBase
    {
        public IRepository<User, long> UserRepository { get; set; }
        public IRepository<Share> ShareRepository { get; set; }
        public IRepository<ShareAccess> ShareAccessRepository { get; set; }
        public UserManager UserManager { get; set; }
        private readonly IBackgroundJobManager _backgroundJobManager;

        public ShareManager(IBackgroundJobManager backgroundJobManager)
        {
            _backgroundJobManager = backgroundJobManager;
        }


        [UnitOfWork]
        public async Task<Share> ProcessShareAccessAsync(string shareNo, long sourceUserId)
        {
            User sourceUser = UserRepository.Get(sourceUserId);
            return await ProcessShareAccessAsync(shareNo, sourceUser);
        }

        [UnitOfWork]
        public async Task<Share> ProcessShareAccessAsync(string shareNo, User sourceUser)
        {
            Share share = ShareRepository.GetAll().Where(model => model.No == shareNo).FirstOrDefault();

            if (share == null)
            {
                return null;
            }
            ShareAccess shareAccess = ShareAccessRepository.GetAll().Where(model => model.CreatorUserId == sourceUser.Id && model.ShareId == share.Id).FirstOrDefault();

            if (shareAccess != null)
            {
                return share;
            }
            await UserManager.BindParentAsync(sourceUser, share.CreatorUser, false, false, BindParentType.ShareAccess);
            shareAccess = new ShareAccess()
            {
                ShareId = share.Id
            };
            ShareAccessRepository.Insert(shareAccess);
            return share;
        }
    }
}
