﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.Shares
{
    public class ShareAccess: AuditedEntity, IMustHaveTenant
    {
        public int ShareId { get; set; }

        public int TenantId { get; set; }
    }
}
