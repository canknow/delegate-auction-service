﻿namespace Application.Shares
{
    public class ShareBase
    {
        public string ShareTitle { get; set; }

        public string ShareDescription { get; set; }

        public string SharePicture { get; set; }
    }
}
