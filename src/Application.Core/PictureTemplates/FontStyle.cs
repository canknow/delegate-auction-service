﻿using Microsoft.EntityFrameworkCore;

namespace Application.PictureTemplates
{

    [Owned]
    public class FontStyle
    {
        public int Size { get; set; } = 24;

        public string Color { get; set; }

        public string FontFamily { get; set; }
    }
}
