﻿using Abp.Dependency;

namespace Application.PictureTemplates
{
    public interface IPicturePathCreator<TData>:ISingletonDependency
    {
        string GetRelativePath(TData data);
    }
}
