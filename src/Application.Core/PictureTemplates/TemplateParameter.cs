﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Application.PictureTemplates
{
    public enum TemplateParameterType
    {
        Text,
        Picture
    }

    public class TemplateParameter : Entity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string DisplayName { get; set; }

        public string DemoText { get; set; }

        public string DemoPicture { get; set; }

        [Required]
        public TemplateParameterType Type { get; set; }

        public string Value { get; set; }

        public virtual FontStyle FontStyle { get; set; }

        public virtual Coordinate Coordinate { get; set; }

        public TemplateParameter()
        {
            FontStyle = new FontStyle();
            Coordinate = new Coordinate();
        }
    }
}
