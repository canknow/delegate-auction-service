﻿using Microsoft.EntityFrameworkCore;

namespace Application.PictureTemplates
{

    [Owned]
    public class Coordinate
    {
        public float Width { get; set; }

        public float Height { get; set; }

        public float StartX { get; set; }

        public float StartY { get; set; }
    }
}
