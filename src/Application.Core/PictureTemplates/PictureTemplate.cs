﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Application.PictureTemplates
{
    public enum PictureTemplateType
    {
        ChannelAgentCertificate,
        AreaAgentCertificate,
        SpreadPoster
    }

    public class PictureTemplate : FullAuditedAggregateRoot, IMustHaveTenant
    {
        [Required]
        public string Name { get; set; }

        public PictureTemplateType Type { get; set; }

        [Required]
        public string Template { get; set; }

        public bool Status { get; set; }

        public virtual List<TemplateParameter> Parameters { get; set; }

        public int TenantId { get; set; }
    }
}
