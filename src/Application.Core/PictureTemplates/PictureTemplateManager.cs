﻿using Abp.Authorization;
using Application.Authorization;
using Application.Authorization.Users;
using Application.Web;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System.IO;
using System.Threading.Tasks;
using Utility.IO;

namespace Application.PictureTemplates
{
    public class PictureTemplateManager<TPictureTemplate, TData>
        : ApplicationDomainServiceBase
        where TPictureTemplate : PictureTemplate
    {
        protected ITemplateParametersParser<TData> _templateParametersParser;
        protected IPicturePathCreator<TData> _picturePathCreator;
        public AppFolderHelper AppFolderHelper { get; set; }
        public PathHelper PathHelper { get; set; }
        public ApplicationAuthorizationHelper AuthorizationHelper { get; set; }

        public PictureTemplateManager(
            ITemplateParametersParser<TData> templateParametersParser,
            IPicturePathCreator<TData> picturePathCreator
            )
        {
            _templateParametersParser = templateParametersParser;
            _picturePathCreator = picturePathCreator;
        }

        private Image<Rgba32> GetTarget(TPictureTemplate pictureTemplate)
        {
            Image<Rgba32> imgSrc;
            if (pictureTemplate.Template.Contains("//"))
            {
                Stream responseStream = CommonFileHelper.LoadNetFile(pictureTemplate.Template);
                imgSrc = Image.Load(responseStream);
            }
            else
            {
                string serverPathOfSource = PathHelper.GetAbsolutePath(pictureTemplate.Template);
                using (var fileStream = File.Open(serverPathOfSource, FileMode.Open))
                {
                    imgSrc = Image.Load(fileStream);
                }
            }
            return imgSrc;
        }

        public async Task<string> GetPictureAsync(TPictureTemplate pictureTemplate, TData data)
        {
            string path = _picturePathCreator.GetRelativePath(data);

            if (!File.Exists(PathHelper.GetAbsolutePath(path)))
            {
                await CreatePictureAsync(pictureTemplate, data);
            };
            return path;
        }

        public async Task<string> ReCreatePictureAsync(TPictureTemplate pictureTemplate, TData data)
        {
            string path = await CreatePictureAsync(pictureTemplate, data);
            return path;
        }

        public async Task<string> CreatePictureAsync(TPictureTemplate pictureTemplate, TData data)
        {
            string path = _picturePathCreator.GetRelativePath(data);
            Image<Rgba32> imgSrc = GetTarget(pictureTemplate);
            _templateParametersParser.Parse(pictureTemplate.Parameters, data);
            User user = AuthorizationHelper.CurrentUser;

            foreach (TemplateParameter parameter in pictureTemplate.Parameters)
            {
                if (parameter.Type == TemplateParameterType.Picture)
                {
                    Image<Rgba32> image;
                    string imagePath;

                    if (parameter.Name.ToLower() == "avatar")
                    {
                        imagePath = user.Avatar;
                    }
                    else if (string.IsNullOrEmpty(parameter.Value))
                    {
                        continue;
                    }
                    else
                    {
                        imagePath = parameter.Value;
                    }
                    if (imagePath.Contains("//"))
                    {
                        image = ImageSharpExtension.Load(imagePath);
                    }
                    else
                    {
                        image = Image.Load(PathHelper.GetAbsolutePath(imagePath));
                    }
                    image.Mutate(a =>
                    {
                        a.Resize(new Size((int)parameter.Coordinate.Width, (int)parameter.Coordinate.Height));
                    });
                    imgSrc.Mutate(a =>
                    {
                        a.DrawImage(
                            image, 
                            1, 
                            new Point((int)parameter.Coordinate.StartX,
                            (int)parameter.Coordinate.StartY));
                    });
                }
                else
                {
                    var fontFamily = new FontCollection().Install(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/bak", "bmjh.ttf"));
                    var font = new Font(fontFamily, parameter.FontStyle.Size);
                    Rgba32 color = Rgba32.Black;
                    if (!string.IsNullOrEmpty(parameter.FontStyle.Color))
                    {
                        color = Rgba32.FromHex(parameter.FontStyle.Color);
                    }
                    imgSrc.Mutate(a =>
                    {
                        a.DrawText(
                            parameter.Value,
                            font,
                            color,
                            new Point((int)parameter.Coordinate.StartX, (int)parameter.Coordinate.StartY));
                    });
                }
            }
            string serverPath = PathHelper.GetAbsolutePath(path);
            imgSrc.Save(serverPath);
            return path;
        }
    }
}
