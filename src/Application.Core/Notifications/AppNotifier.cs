﻿using Abp;
using Abp.Localization;
using Abp.Notifications;
using Application.Authorization.Users;
using Application.Finance.Wallets;
using Application.Groups;
using Application.MultiTenancy;
using Application.Orders;
using Application.Wechat.TemplateMessages;
using Application.Wechat.TemplateMessages.TemplateMessageDatas;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Date;
using Application.Wallets;
using Application.BusinessNotifiers;

namespace Application.Notifications
{
    public class AppNotifier : ApplicationDomainServiceBase, IAppNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;
        public TemplateMessageManager TemplateMessageManager{get; set;}
        public GroupHelper GroupHelper { get; set; }
        public BusinessNotiferManager BusinessNotiferManager { get; set; }

        public AppNotifier(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
        }

        public async Task WelcomeToTheApplicationAsync(User user)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.WelcomeToTheApplication,
                new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
                severity: NotificationSeverity.Success,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }

        public async Task NewUserRegisteredAsync(User user)
        {
            var notificationData = new LocalizableMessageNotificationData(
               new LocalizableString(
                    "NewUserRegisteredNotificationMessage",
                    ApplicationConsts.LocalizationSourceName
                    )
                );
            notificationData["id"] = user.Id;
            notificationData["avatar"] = user.Avatar;
            notificationData["userName"] = user.UserName;
            notificationData["nickName"] = user.NickName;
            notificationData["emailAddress"] = user.EmailAddress;
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.NewUserRegistered,
                notificationData,
                tenantIds: new[] { user.TenantId });
        }

        public async Task NewCustomerForBusinessAsync(User user, User parentUser, BindParentType bindParentType, BusinessNotifier businessNotifier)
        {
            string openid = GetOpenid(businessNotifier.User);

            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            NewCustomerTemplateMessageData newCustomerTemplateMessageData = new NewCustomerTemplateMessageData(
                              new TemplateDataItem(L("YouCustomerHasANewCustomer", parentUser.NickName, L("BindParentBy" + bindParentType.ToString()))),
                              new TemplateDataItem(user.Number),
                              new TemplateDataItem(user.NickName),
                              new TemplateDataItem(user.CreationTime.ToString("yyyy-MM-dd hh:mm:ss")),
                              new TemplateDataItem(L("ThankYouForYourPatronage"))
                              );
            await TemplateMessageManager.SendTemplateMessageOfNewCustomerAsync(parentUser.TenantId.Value, openid, "", newCustomerTemplateMessageData);
        }

        public async Task NewTenantRegisteredAsync(Tenant tenant)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewTenantRegisteredNotificationMessage",
                    ApplicationConsts.LocalizationSourceName
                    )
                );
            notificationData["tenancyName"] = tenant.TenancyName;
            await _notificationPublisher.PublishAsync(AppNotificationNames.NewTenantRegistered, notificationData);
        }

        //This is for test purposes
        public async Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info)
        {
            await _notificationPublisher.PublishAsync(
                "App.SimpleMessage",
                new MessageNotificationData(message),
                severity: severity,
                userIds: new[] { user }
                );
        }

        public async Task OrderCreatedAsync(Order order)
        {
            var userLogin = order.User.Logins.Where(model => model.LoginProvider == "Weixin").FirstOrDefault();
            if (userLogin != null)
            {
                string openid = userLogin.ProviderKey;

                OrderCreatedTemplateMessageData data = new OrderCreatedTemplateMessageData(
                    new TemplateDataItem(order.Title),
                    new TemplateDataItem(order.Number),
                    new TemplateDataItem("1"),
                    new TemplateDataItem(order.Money.ToString("#0.00")),
                    new TemplateDataItem(L("ThankYouForYourPatronage"))
                    );
                await TemplateMessageManager.SendTemplateMessageOfOrderCreatedAsync(order.TenantId, openid, OrderHelper.GetOrderDetailUrl(order.Id), data);
            }
        }

        public async Task OrderPayedAsync(Order order)
        {
            string openid = GetOpenid(order.User);
            if (openid != null)
            {
                OrderPayedTemplateMessageData data = new OrderPayedTemplateMessageData(
                       new TemplateDataItem(order.Title),
                       new TemplateDataItem(order.Number),
                       new TemplateDataItem(order.PaymentDatetime.ToString()),
                       new TemplateDataItem(order.Money.ToString("#0.00")),
                       new TemplateDataItem(order.PayType.ToString()),
                       new TemplateDataItem(L("ThankYouForYourPatronage"))
                       );
                await TemplateMessageManager.SendTemplateMessageOfOrderPayedAsync(order.TenantId, openid, OrderHelper.GetOrderDetailUrl(order.Id), data);
            }
        }

        public async Task NewCustomerAsync(User user, User parentUser, BindParentType bindParentType)
        {
            string openid = GetOpenid(parentUser);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            NewCustomerTemplateMessageData data = new NewCustomerTemplateMessageData(
                new TemplateDataItem(L("YouHasANewCustomer", L("BindParentBy" + bindParentType.ToString()))),
                new TemplateDataItem(user.Number),
                new TemplateDataItem(user.NickName),
                new TemplateDataItem(user.CreationTime.ToString("yyyy-MM-dd hh:mm:ss")),
                new TemplateDataItem(L("ThankYouForYourPatronage"))
                );
            await TemplateMessageManager.SendTemplateMessageOfNewCustomerAsync(parentUser.TenantId.Value,
                openid,
                GroupHelper.GetGroupUrl(user.TenantId.Value),
                data);
        }

        public async Task PassOrderAsync(Order order)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            AuditTemplateMessageData data = new AuditTemplateMessageData(
                new TemplateDataItem(L("YourOrderHasPassedAudit")),
                new TemplateDataItem(order.Number),
                new TemplateDataItem(L("AuditPass")),
                new TemplateDataItem(order.CreationTime.ToString("yyyy-MM-dd hh:mm:ss")),
                new TemplateDataItem(L("ThankYouForYourPatronage"))
                );
            await TemplateMessageManager.SendTemplateMessageOfAuditAsync(order.User.TenantId.Value,
                openid,
                OrderHelper.GetOrderDetailUrl(order.Id),
                data);
        }

        private string GetOpenid(User user)
        {
            var userLogin = user.Logins.Where(model => model.LoginProvider == "Weixin").FirstOrDefault();
            if (userLogin != null)
            {
                return userLogin.ProviderKey;
            }
            return null;
        }

        public async Task DenyOrderAsync(Order order)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            AuditTemplateMessageData data = new AuditTemplateMessageData(
                new TemplateDataItem(L("YourOrderHasUnPassAudit")),
                new TemplateDataItem(order.Number),
                new TemplateDataItem(L("AuditDeny")),
                new TemplateDataItem(order.CreationTime.ToString("yyyy-MM-dd hh:mm:ss")),
                new TemplateDataItem(L("ThankYouForYourPatronage")));
            await TemplateMessageManager.SendTemplateMessageOfAuditAsync(order.User.TenantId.Value,
                openid,
                OrderHelper.GetOrderDetailUrl(order.Id),
                data);
        }

        public async Task OrderRebateAsync(Order order, string remark)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            OrderRebateTemplateMessageData data = new OrderRebateTemplateMessageData(
                  new TemplateDataItem(L("YouGetIncome", remark)),
                  new TemplateDataItem(order.Number),
                  new TemplateDataItem(order.Money.ToString("#0.00")),
                  new TemplateDataItem(order.PaymentDatetime.ToLocalString()),
                  new TemplateDataItem(order.Money.ToString("#0.00")),
                  new TemplateDataItem(L("ThankYouForYourPatronage"))
                  );
            await TemplateMessageManager.SendTemplateMessageOfOrderRebateAsync(order.TenantId, openid, WalletHelper.GetWalletUrl(order.TenantId), data);
        }

        public async Task CompleteAuction(Order order)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            ServiceProgressTemplateMessageData data = new ServiceProgressTemplateMessageData(
               new TemplateDataItem(L("YourAuctionHasPassed")),
               new TemplateDataItem(L("DelegateAuction")),
               new TemplateDataItem(order.User.PhoneNumber),
               new TemplateDataItem(L("ThankYouForYourPatronage"))
               );
            await TemplateMessageManager.SendTemplateMessageOfServiceProgressAsync(order.User.TenantId.Value,
                openid,
                OrderHelper.GetOrderDetailUrl(order.Id),
                data);
        }

        public async Task OrderCompensatedAsync(Order order)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            ServiceProgressTemplateMessageData data = new ServiceProgressTemplateMessageData(
               new TemplateDataItem(L("YourOrderHasCompensated")),
               new TemplateDataItem(L("DelegateAuction")),
               new TemplateDataItem(order.User.PhoneNumber),
               new TemplateDataItem(L("ThankYouForYourPatronage"))
               );
            await TemplateMessageManager.SendTemplateMessageOfServiceProgressAsync(order.User.TenantId.Value,
                openid,
                OrderHelper.GetOrderDetailUrl(order.Id),
                data);
        }

        public async Task LoseAuction(Order order)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            ServiceProgressTemplateMessageData data = new ServiceProgressTemplateMessageData(
               new TemplateDataItem(L("YourOrderLoseAuction")),
               new TemplateDataItem(L("DelegateAuction")),
               new TemplateDataItem(order.User.PhoneNumber),
               new TemplateDataItem(L("ThankYouForYourPatronage"))
               );
            await TemplateMessageManager.SendTemplateMessageOfServiceProgressAsync(order.User.TenantId.Value,
                openid,
                OrderHelper.GetOrderDetailUrl(order.Id),
                data);
        }

        public async Task FailAuction(Order order)
        {
            string openid = GetOpenid(order.User);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            ServiceProgressTemplateMessageData data = new ServiceProgressTemplateMessageData(
               new TemplateDataItem(L("YourOrderFailAuction")),
               new TemplateDataItem(L("DelegateAuction")),
               new TemplateDataItem(order.User.PhoneNumber),
               new TemplateDataItem(L("ThankYouForYourPatronage"))
               );
            await TemplateMessageManager.SendTemplateMessageOfServiceProgressAsync(order.User.TenantId.Value,
                openid,
                OrderHelper.GetOrderDetailUrl(order.Id),
                data);
        }

        public async Task WithdrawSuccess(User user, WithdrawApply withdrawApply)
        {
            string openid = GetOpenid(user);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            WalletWithdrawTemplateMessageData data = new WalletWithdrawTemplateMessageData(
                             new TemplateDataItem(L("WithdrawSuccessfully")),
                             new TemplateDataItem(user.NickName),
                             new TemplateDataItem(withdrawApply.Money.ToString("#0.00")),
                             new TemplateDataItem(L("ThankYouForYourPatronage"))
                             );
            await TemplateMessageManager.SendTemplateMessageOfWalletWithdrawAsync(withdrawApply.TenantId, openid, WalletHelper.GetWalletUrl(), data);
        }

        public async Task WithdrawFail(User user, WithdrawApply withdrawApply, string failReason)
        {
            string openid = GetOpenid(user);
            if (string.IsNullOrEmpty(openid))
            {
                return;
            }
            WalletWithdrawTemplateMessageData data = new WalletWithdrawTemplateMessageData(
                             new TemplateDataItem(L("WithdrawFailed") + ":" + failReason),
                             new TemplateDataItem(user.NickName),
                             new TemplateDataItem(withdrawApply.Money.ToString("#0.00")),
                             new TemplateDataItem(L("ThankYouForYourPatronage"))
                             );
            await TemplateMessageManager.SendTemplateMessageOfWalletWithdrawAsync(withdrawApply.TenantId, openid, WalletHelper.GetWalletUrl(), data);
        }

        public async Task OrderCreatedForBusinessAsync(Order order, BusinessNotifier businessNotifier)
        {
            string openid = GetOpenid(businessNotifier.User);

            if (!string.IsNullOrEmpty(openid))
            {
                OrderCreatedTemplateMessageData data = new OrderCreatedTemplateMessageData(
                        new TemplateDataItem(L("CustomerOrderCreated", order.User.NickName, order.Title)),
                        new TemplateDataItem(order.Number),
                        new TemplateDataItem("1"),
                        new TemplateDataItem(order.Money.ToString("#0.00")),
                        new TemplateDataItem(L("ThankYouForYourPatronage"))
                        );
                await TemplateMessageManager.SendTemplateMessageOfOrderCreatedAsync(order.TenantId, openid, null, data);
            }
        }
    }
}
