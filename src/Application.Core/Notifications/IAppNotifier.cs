﻿using Abp;
using Abp.Notifications;
using Application.Authorization.Users;
using Application.BusinessNotifiers;
using Application.MultiTenancy;
using Application.Orders;
using Application.Wallets;
using System.Threading.Tasks;

namespace Application.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);

        Task OrderCreatedAsync(Order order);

        Task OrderPayedAsync(Order order);

        Task NewCustomerAsync(User user, User parentUser, BindParentType bindParentType);

        Task CompleteAuction(Order order);

        Task PassOrderAsync(Order order);

        Task DenyOrderAsync(Order order);

        Task OrderRebateAsync(Order order, string remark);

        Task OrderCompensatedAsync(Order order);

        Task WithdrawSuccess(User user, WithdrawApply withdrawApply);

        Task WithdrawFail(User user, WithdrawApply withdrawApply, string failReason);

        Task LoseAuction(Order order);

        Task FailAuction(Order order);

        Task NewCustomerForBusinessAsync(User user, User parentUser, BindParentType bindParentType, BusinessNotifier businessNotifier);

        Task OrderCreatedForBusinessAsync(Order order, BusinessNotifier businessNotifier);
    }
}
