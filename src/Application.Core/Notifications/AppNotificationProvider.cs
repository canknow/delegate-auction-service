﻿using Abp.Localization;
using Abp.Notifications;

namespace Application.Notifications
{
    public class AppNotificationProvider : NotificationProvider
    {
        public override void SetNotifications(INotificationDefinitionContext context)
        {
            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.NewUserRegistered,
                    displayName: L("NewUserRegisteredNotificationDefinition")
                    )
                );

            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.NewTenantRegistered,
                    displayName: L("NewTenantRegisteredNotificationDefinition")
                    )
                );

            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.NewConsultantCome,
                    displayName: L("NewConsultantComeNotificationDefinition")
                    )
                    );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ApplicationConsts.LocalizationSourceName);
        }
    }
}
