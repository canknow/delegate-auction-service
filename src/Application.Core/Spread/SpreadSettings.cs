namespace Application.Configuration
{
    public static class SpreadSettings
    {
        public static class General
        {
            public const string MaxManualBindCount = "Spread.General.MaxManualBindCount";
            public const string UpgradeOrderMoney = "Spread.General.UpgradeOrderMoney";
            public const string NewCustomerScoreAward = "Spread.General.NewCustomerScoreAward";
            public const string MustBeSpreaderForSpread= "Spread.General.MustBeSpreaderForSpread";
        }
    }
}