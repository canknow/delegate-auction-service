﻿using Abp.Configuration;
using System.Collections.Generic;
using System.Configuration;

namespace Application.Configuration
{
    public class SpreadSettingProvider : ApplicationSettingProvider
    {
        public SpreadSettingProvider(IAppConfigurationAccessor configurationAccessor) :base(configurationAccessor)
        {
        }

        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                //Tenant settings
               new SettingDefinition(
                    SpreadSettings.General.MaxManualBindCount,
                    ConfigurationManager.AppSettings[SpreadSettings.General.MaxManualBindCount] ?? "1",
                    scopes: SettingScopes.Tenant, isVisibleToClients: true),
                new SettingDefinition(
                    SpreadSettings.General.UpgradeOrderMoney,
                    ConfigurationManager.AppSettings[SpreadSettings.General.UpgradeOrderMoney] ?? "0", 
                    scopes: SettingScopes.Tenant, isVisibleToClients: true),
                new SettingDefinition(
                    SpreadSettings.General.NewCustomerScoreAward,
                    ConfigurationManager.AppSettings[SpreadSettings.General.NewCustomerScoreAward] ?? "0",
                    scopes: SettingScopes.Tenant, isVisibleToClients: true),
                new SettingDefinition(
                    SpreadSettings.General.MustBeSpreaderForSpread,
                    ConfigurationManager.AppSettings[SpreadSettings.General.MustBeSpreaderForSpread] ?? "true",
                    scopes: SettingScopes.Tenant, isVisibleToClients: true)
            };
        }
    }
}
