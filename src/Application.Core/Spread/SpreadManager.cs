﻿using Abp;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Application.Authorization.Users;
using Application.Configuration;
using System;
using System.Threading.Tasks;

namespace Application.Spread
{
    public class SpreadManager : ApplicationDomainServiceBase
    {
        public IRepository<User, long> UserRepository { get; set; }

        public async Task CanSpreadAsync(UserIdentifier userIdentifier)
        {
            using (CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId))
            {
                User user = UserRepository.Get(userIdentifier.UserId);
                await CanSpreadAsync(user);
            }
        }

        public async Task CanSpreadAsync(User user)
        {
            bool mustBeSpreaderForSpread = await SettingManager.GetSettingValueForTenantAsync<bool>(SpreadSettings.General.MustBeSpreaderForSpread, user.TenantId.Value);

            if (mustBeSpreaderForSpread && !user.IsSpreader)
            {
                throw new Exception(L("YouMustBeSpreaderToSpread"));
            }
        }

        public async Task CanGetSpreadPoster(UserIdentifier userIdentifier)
        {
            using (CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId))
            {
                User user = UserRepository.Get(userIdentifier.UserId);
                await CanGetSpreadPoster(user);
            }
        }

        public async Task CanGetSpreadPoster(User user)
        {
            await CanSpreadAsync(user);
        }

        [UnitOfWork]
        public async Task SetAsSpreader(UserIdentifier userIdentifier)
        {
            using (CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId.Value))
            {
                User user = UserRepository.Get(userIdentifier.UserId);
                await SetAsSpreader(user);
            }
        }

        [UnitOfWork]
        public async Task SetAsSpreader(User user)
        {
            if (user.IsSpreader)
            {
                return;
            }
            user.IsSpreader = true;
            UserRepository.Update(user);

            // todo: send message by weixin
        }
    }
}
