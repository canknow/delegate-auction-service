﻿using Abp.Domain.Repositories;

namespace Application.Bids
{
    public class BidManage: ApplicationDomainServiceBase
    {
        public IRepository<Bid> BidRepository { get; set; }

        public Bid UpdateBid(string idNumber, string fullName, string phoneNumber, string account, string password, string bidPicture, string frontOfIDCard, string backOfIDCard)
        {
            var bid = BidRepository.FirstOrDefault(model => model.UserId == AbpSession.UserId.Value);
            if (bid != null)
            {
                bid.IDNumber = idNumber;
                bid.FullName = fullName;
                bid.PhoneNumber = phoneNumber;
                bid.Account = account;
                bid.Password = password;
                bid.BidPicture = bidPicture;
                bid.BackOfIDCard = backOfIDCard;
                bid.FrontOfIDCard = frontOfIDCard;
                BidRepository.Update(bid);
            }
            else
            {
                bid = new Bid()
                {
                    UserId = AbpSession.UserId.Value
                };
                bid.IDNumber = idNumber;
                bid.FullName = fullName;
                bid.PhoneNumber = phoneNumber;
                bid.Account = account;
                bid.Password = password;
                bid.BidPicture = bidPicture;
                bid.BackOfIDCard = backOfIDCard;
                bid.FrontOfIDCard = frontOfIDCard;
                BidRepository.Insert(bid);
            }
            return bid;
        }
    }
}
