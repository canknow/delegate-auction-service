using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.Bids
{
    public class Bid: FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string FullName{get;set;}

        public string PhoneNumber{get;set;}
        
        public string IDNumber{get;set;}

        public string Account{get;set;}

        public string Password{get;set;}

        public long UserId { get; set; }

        public string BidPicture { get; set; }

        public string FrontOfIDCard { get; set; }

        public string BackOfIDCard { get; set; }
    }
}