using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Application.Configuration;
using Application.Orders;
using Application.Orders.NumberProviders;
using Application.Web;
using Application.Wechat;
using Senparc.CO2NET;
using Senparc.CO2NET.HttpUtility;
using Senparc.Weixin.TenPay.V3;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Application.Pays.PayProviders
{
    public class WeixinPayProvider : ApplicationDomainServiceBase, IPayProvider
    {
        public WechatCommonManager WechatCommonManager { get; set; }
        public OrderManager OrderManager { get; set; }
        private TenPayV3Info _tenPayV3Info;
        public INumberProvider NumberProvider { get; set; }
        private const string RefundUrl= "https://api.mch.weixin.qq.com/secapi/pay/refund";
        public PathHelper PathHelper { get; set; }
        public IRepository<UserLogin, long> userLoginRepository { get; set; }
        public ServerContextHelper ServerContextHelper { get; set; }

        public async Task<TenPayV3Info> GetTenPayV3Info(int? tenantId=null)
        {
            if (_tenPayV3Info == null)
            {
                _tenPayV3Info =await WechatCommonManager.GetTenPayV3InfoAsync(tenantId.HasValue ? tenantId.Value : AbpSession.TenantId.Value, null);
            }
            return _tenPayV3Info;
        }

        public async Task CloseOrder(Order order)
        {
            TenPayV3Info TenPayV3Info = await GetTenPayV3Info(order.TenantId);
            string nonceStr = TenPayV3Util.GetNoncestr();
            TenPayV3CloseOrderRequestData data = new TenPayV3CloseOrderRequestData(TenPayV3Info.AppId,
                TenPayV3Info.MchId,
                order.OutTradeNo,
                TenPayV3Info.Key,
                nonceStr
                );

            var result = TenPayV3.CloseOrder(data);
        }

        public async Task Refund(Order order, decimal refundFee)
        {
            TenPayV3Info TenPayV3Info = await GetTenPayV3Info();
            string nonceStr = TenPayV3Util.GetNoncestr();
            string outTradeNo = order.Number;
            OrderManager.ResetRefundNumber(order);
            int totalFee = (int)(order.Money * 100);
            string opUserId = TenPayV3Info.MchId;
            var dataInfo = new TenPayV3RefundRequestData(TenPayV3Info.AppId, TenPayV3Info.MchId, TenPayV3Info.Key,
                null, nonceStr, null, order.OutTradeNo, order.RefundNumber, totalFee, (int)(refundFee * 100), opUserId, null);
            var cert = PathHelper.GetAbsolutePath(await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.SslcertPath, order.TenantId));
            var password = TenPayV3Info.MchId;//默认为商户号，建议修改
            var result = TenPayV3.Refund(dataInfo, cert, password);

            if(result.result_code== "FAIL")
            {
                throw new Exception(result.err_code_des);
            }
        }

        private bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            if (errors == SslPolicyErrors.None)
                return true;

            return false;
        }

        [UnitOfWork]
        public async Task<bool> PayToUser(long userId, int tenantId, decimal money, string number, string remark)
        {
            TenPayV3Info tenPayV3Info = await GetTenPayV3Info(tenantId);
            UserLogin userLogin = userLoginRepository.FirstOrDefault(model => model.UserId == userId
               && model.LoginProvider == "Weixin");

            string openId = userLogin.ProviderKey;
            string nonce = TenPayV3Util.GetNoncestr();

            TenPayV3TransfersRequestData dataInfo = new TenPayV3TransfersRequestData(
                tenPayV3Info.AppId,
                tenPayV3Info.MchId,
                null,
                nonce,
                number,
                openId,
                tenPayV3Info.Key,
                "NO_CHECK",
                null,
                money,
                L("Withdraw"),
                "127.0.0.1");

            var urlFormat = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
            var data = dataInfo.PackageRequestHandler.ParseXML();
            var formDataBytes = data == null ? new byte[0] : Encoding.UTF8.GetBytes(data);
            MemoryStream memoryStream = new MemoryStream();
            memoryStream.Write(formDataBytes, 0, formDataBytes.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);//设置指针读取位置

            //本地或者服务器的证书位置（证书在微信支付申请成功发来的通知邮件中）
            string cert = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.SslcertPath, tenantId);
            //私钥（在安装证书时设置）
            string password = dataInfo.MchId;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            //调用证书
            X509Certificate2 x509Certificate2 = new X509Certificate2(
                PathHelper.GetAbsolutePath(cert),
                password,
                X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);

            string resultXml = RequestUtility.HttpPost(urlFormat, null, memoryStream, null, null, Encoding.UTF8, x509Certificate2, false, Config.TIME_OUT);
            TransfersResult transfersResult = new TransfersResult(resultXml);

            if (transfersResult.return_code == "FAIL")
            {
                throw new Exception(transfersResult.return_msg);
            }
       
            bool isSuccess = transfersResult.result_code == "FAIL" ? false : true;
            if (!isSuccess)
            {
                throw new Exception(transfersResult.err_code_des);
            }
            return isSuccess;
        }
    }
}
