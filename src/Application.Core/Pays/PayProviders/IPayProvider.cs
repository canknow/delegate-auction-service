﻿using Application.Orders;
using System.Threading.Tasks;

namespace Application.Pays.PayProviders
{
    public interface IPayProvider
    {
        Task Refund(Order order, decimal refundFee);

        Task CloseOrder(Order order);

        Task<bool> PayToUser(long userId, int tenantId, decimal money, string number, string remark);
    }
}
