﻿namespace Application
{
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; }

        string SampleProfileImagesFolder { get; }

        string WebLogsFolder { get; set; }

        string UserResourceFolderBase { get; set; }

        string UploadFolder { get; set; }

        string MpFolder { get; set; }
    }
}