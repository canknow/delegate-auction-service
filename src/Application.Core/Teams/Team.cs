﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Application.Packages;
using System.Collections.Generic;

namespace Application.Teams
{
    public class Team: FullAuditedEntity, IMustHaveTenant
    {
        public string Name { get; set; }

        public string Logo { get; set; }

        public string Level { get; set; }

        public string Introduce { get; set; }

        public string Illustration { get; set; }

        public float Rate { get; set; }

        public int DealCount { get; set; }

        public int HintCount { get; set; }

        public int Quota { get; set; }

        public virtual ICollection<Package> Packages { get; set; }

        public int TenantId { get; set; }

        public bool IsHot { get; set; }

        public bool IsShow { get; set; }
    }
}
