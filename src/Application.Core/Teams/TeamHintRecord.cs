﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Teams
{
    public class TeamHintRecord: FullAuditedEntity
    {
        public long UserId { get; set; }

        public int TeamId { get; set; }
    }
}
