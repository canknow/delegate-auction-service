﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Application.Authorization.Users;
using Application.Orders;
using Application.Packages;
using Application.Teams;
using Microsoft.EntityFrameworkCore;
using System;

namespace Application.AuctionRecords
{
    public class AuctionRecord: FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual Order Order { get; set; }

        public string Shot { get; set; }

        public int OrderId { get; set; }

        public long? UserId { get; set; }

        public virtual User User { get; set; }

        public virtual AuctionRecordBid Bid { get; set; }

        public int? BidId { get; set; }

        public int AttemptCount { get; set; }

        public DateTime? WinDateTime { get; set; }

        public int? PackageId { get; set; }

        public virtual Package Package { get; set; }

        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }

        [Owned]
        public class AuctionRecordBid
        {
            public string FullName { get; set; }

            public string PhoneNumber { get; set; }

            public string IDNumber { get; set; }

            public string Account { get; set; }

            public string Password { get; set; }
        }
    }
}
