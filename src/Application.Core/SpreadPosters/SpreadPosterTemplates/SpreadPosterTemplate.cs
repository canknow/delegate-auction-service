﻿using Application.PictureTemplates;

namespace Application.SpreadPosters.SpreadPosterTemplates
{
    public class SpreadPosterTemplate : PictureTemplate
    {
        public bool IsDefault { get; set; }

        public SpreadPosterTemplate()
        {
            Type = PictureTemplateType.SpreadPoster;
        }
    }
}
