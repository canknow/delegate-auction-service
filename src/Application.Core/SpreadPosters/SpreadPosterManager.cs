using Abp;
using Abp.Domain.Repositories;
using Abp.IO;
using Application.Authorization.Users;
using Application.PictureTemplates;
using Application.SpreadPosters.SpreadPosterTemplates;
using Application.Web;
using Application.Wechat.Qrcodes;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utility.IO;
using Application.Spread;

namespace Application.SpreadPosters
{
    public class SpreadPosterManager : ApplicationDomainServiceBase
    {
        public IRepository<SpreadPosterTemplate> SpreadPosterTemplateRepository { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        public SpreadManager SpreadManager { get; set; }

        public QrcodeManager QrcodeManager { get; set; }

        public AppFolderHelper AppFolderHelper { get; set; }
        public PathHelper PathHelper { get; set; }

        public SpreadPosterTemplate GetDefaultSpreadPosterTemplate(UserIdentifier userIdentifier)
        {
            return SpreadPosterTemplateRepository.GetAll().Where(model => model.IsDefault == true).FirstOrDefault();
        }

        public async Task<string> GetDefaultSpreadPosterAsync(UserIdentifier userIdentifier)
        {
            SpreadPosterTemplate spreadPosterTemplate = GetDefaultSpreadPosterTemplate(userIdentifier);

            if (spreadPosterTemplate == null)
            {
                throw new Exception(L("ThereHasNoDefaultSpreadPostTemplate"));
            }
            string path = await GetSpreadPosterAsync(spreadPosterTemplate.Id, userIdentifier);
            return path;
        }

        private string GetSpreadPosterFolderPathOfUser(UserIdentifier userIdentifier)
        {
            string userResourceFolder = AppFolderHelper.GetUserResourcePath(userIdentifier);
            string userSpreadPosterFolderPath = userResourceFolder + "/SpreadPoster";
            DirectoryHelper.CreateIfNotExists(PathHelper.GetAbsolutePath(userSpreadPosterFolderPath));
            return userSpreadPosterFolderPath;
        }

        private string GetSpreadPosterPathOfUser(int spreadPosterTemplateId, UserIdentifier userIdentifier)
        {
            string userSpreadPosterFolderPath = GetSpreadPosterFolderPathOfUser(userIdentifier);
            return userSpreadPosterFolderPath + "/" + spreadPosterTemplateId + ".jpg";
        }

        public async Task<string> GetSpreadPosterAsync(int spreadPosterTemplateId, UserIdentifier userIdentifier)
        {
            await SpreadManager.CanGetSpreadPoster(userIdentifier);
            string path = GetSpreadPosterPathOfUser(spreadPosterTemplateId, userIdentifier);

            if (!File.Exists(PathHelper.GetAbsolutePath(path)))
            {
                await CreateSpreadPosterAsync(spreadPosterTemplateId, userIdentifier);
            };
            return path;
        }

        public async Task<string> ReCreateSpreadPosterAsync(int spreadPosterTemplateId, UserIdentifier userIdentifier)
        {
            string path = await CreateSpreadPosterAsync(spreadPosterTemplateId, userIdentifier);
            return path;
        }

        public async Task<string> ReCreateDefaultSpreadPosterAsync(UserIdentifier userIdentifier)
        {
            SpreadPosterTemplate spreadPosterTemplate = GetDefaultSpreadPosterTemplate(userIdentifier);

            if (spreadPosterTemplate == null)
            {
                throw new Exception(L("ThereHasNoDefaultSpreadPostTemplate"));
            }
            string path = await CreateSpreadPosterAsync(spreadPosterTemplate, userIdentifier);
            return path;
        }

        public async Task<string> CreateSpreadPosterAsync(int spreadPosterTemplateId, UserIdentifier userIdentifier)
        {
            SpreadPosterTemplate spreadPosterTemplate = SpreadPosterTemplateRepository.Get(spreadPosterTemplateId);

            if (spreadPosterTemplate == null)
            {
                throw new Exception(L("ThereNotExitSpreadPostTemplateWithThisId"));
            }
            string path = await CreateSpreadPosterAsync(spreadPosterTemplate, userIdentifier);
            return path;
        }

        private Image<Rgba32> GetTarget(SpreadPosterTemplate spreadPosterTemplate)
        {
            Image<Rgba32> imgSrc;

            if (spreadPosterTemplate.Template.Contains("//"))
            {
                Stream responseStream = CommonFileHelper.LoadNetFile(spreadPosterTemplate.Template);
                imgSrc = Image.Load(responseStream);
            }
            else
            {
                string serverPathOfSource = PathHelper.GetAbsolutePath(spreadPosterTemplate.Template);
                using(var fileStream = File.Open(serverPathOfSource, FileMode.Open))
                {
                    imgSrc = Image.Load(fileStream);
                }
            }
            return imgSrc;
        }

        public async Task<string> CreateSpreadPosterAsync(SpreadPosterTemplate spreadPosterTemplate, UserIdentifier userIdentifier)
        {
            string path = GetSpreadPosterPathOfUser(spreadPosterTemplate.Id, userIdentifier);

            using (CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId))
            {
                User user = UserRepository.Get(userIdentifier.UserId);
                Image<Rgba32> imgSrc = GetTarget(spreadPosterTemplate);

                foreach (TemplateParameter parameter in spreadPosterTemplate.Parameters)
                {
                    if (parameter.Type == TemplateParameterType.Picture)
                    {
                        Image<Rgba32> image;
                        string imagePath;

                        if (parameter.Name.ToLower() == "qrcode")
                        {
                            Qrcode qrcode = await QrcodeManager.GetQrcodeAsync(userIdentifier, false);
                            imagePath = qrcode.Path;
                        }
                        else if (parameter.Name.ToLower() == "avatar")
                        {
                            imagePath = user.Avatar;
                        }
                        else if (string.IsNullOrEmpty(parameter.Value))
                        {
                            continue;
                        }
                        else
                        {
                            imagePath = parameter.Value;
                        }

                        if (imagePath.Contains("//"))
                        {
                            image = ImageSharpExtension.Load(imagePath);
                        }
                        else
                        {
                            image = Image.Load(PathHelper.GetAbsolutePath(imagePath));
                        }
                   
                        image.Mutate(a =>
                        {
                            a.Resize(new Size((int)parameter.Coordinate.Width, (int)parameter.Coordinate.Height));
                        });
                        imgSrc.Mutate(a =>
                        {
                            a.DrawImage(
                                image,
                                1,
                                new Point((int)parameter.Coordinate.StartX,
                                (int)parameter.Coordinate.StartY));
                        });
                    }
                    else
                    {
                        var fontFamily = new FontCollection().Install(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/bak", "bmjh.ttf"));
                        var font = new Font(fontFamily, parameter.FontStyle.Size);
                        //var font = SystemFonts.CreateFont("Arial", parameter.FontStyle.Size);
                        string text = parameter.Value;
                        if (parameter.Name == "nickName")
                        {
                            text = user.NickName;
                        }
                        Rgba32 color = Rgba32.Black;
                        if (!string.IsNullOrEmpty(parameter.FontStyle.Color))
                        {
                            color = Rgba32.FromHex(parameter.FontStyle.Color);
                        }
                        imgSrc.Mutate(a =>
                        {
                            a.DrawText(
                                text,
                                font,
                                color,
                                new Point((int)parameter.Coordinate.StartX, (int)parameter.Coordinate.StartY));
                        });
                    }
                }
                string serverPath = PathHelper.GetAbsolutePath(path);
                imgSrc.Save(serverPath);
                return path;
            }
        }
    }
}
