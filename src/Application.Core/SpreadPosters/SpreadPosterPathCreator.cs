﻿using Application.Authorization.Users;
using Application.PictureTemplates;
using Abp.IO;
using Application.Web;

namespace Application.SpreadPosters
{
    public class SpreadPosterPathCreator : IPicturePathCreator<User>
    {
        public AppFolderHelper AppFolderHelper { get; set; }
        public PathHelper PathHelper { get; set; }

        public string GetRelativePath(User data)
        {
            string folderPath = GetFolderPathOfUser(data);
            return folderPath + "/" + data.Id + ".jpg";
        }

        private string GetFolderPathOfUser(User data)
        {
            string userResourceFolder = AppFolderHelper.GetUserResourcePath(data.ToUserIdentifier());
            string userPictureFolderPath = userResourceFolder + "/spreadPoseter";
            DirectoryHelper.CreateIfNotExists(PathHelper.GetAbsolutePath(userPictureFolderPath));
            return userPictureFolderPath;
        }
    }
}
