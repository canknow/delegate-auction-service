﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.ShopTemplates
{
    public class ShopTemplate : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public bool IsDefault { get; set; }

        public string Name { get; set; }

        public string Template { get; set; }
    }
}
