﻿using Abp;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Application.Authorization.Users;
using Application.BackgroundJob;
using Application.Configuration;
using Application.Notifications;
using Application.Orders.NumberProviders;
using Application.Wallets.WithdrawProviders;
using System;
using System.Threading.Tasks;

namespace Application.Wallets
{
    public class WithdrawManager : ApplicationDomainServiceBase
    {
        public WalletManager WalletManager { get; set; }
        public IRepository<WithdrawApply> WithdrawApplyRepository { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        public IRepository<WalletRecord> WalletRecordRepository { get; set; }
        public INumberProvider NumberProvider { get; set; }
        public BackgroundJobManager BackgroundJobManager { get; set; }
        public IAppNotifier AppNotifier { get; set; }

        public void AddWithdrawAllBalanceJob(UserIdentifier userIdentifier)
        {
            UnitOfWorkManager.Current.Completed += (sender, args) =>
            {
                BackgroundJobManager.Enqueue<WithdrawAllBalanceJob, WithdrawAllBalanceJobArgs>(new WithdrawAllBalanceJobArgs()
                {
                    TenantId = userIdentifier.TenantId.Value,
                    UserId = userIdentifier.UserId
                });
            };
        }

        [UnitOfWork]
        public async Task<WalletRecord> WithdrawAllBalanceAsync(UserIdentifier userIdentifier, int? financeAccountId)
        {
            using (CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId))
            {
                decimal usableMoeny = WalletManager.GetUsableMoney(userIdentifier);

                if (usableMoeny <= 0)
                {
                    throw new Exception("NoEnoughMoneyForWithdraw");
                }
                else if (usableMoeny < 1)
                {
                    throw new Exception("WithdrawMoneyMustGreateThen1.00");
                }
                return await WithdrawAsync(userIdentifier, usableMoeny, L("Withdraw"), financeAccountId);
            }
        }

        [UnitOfWork]
        public async Task<WalletRecord> WithdrawAsync(UserIdentifier userIdentifier, decimal money, string remark, int? financeAccountId)
        {
            CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId);
            WalletRecord walletRecord = WalletManager.CreateWithdrawWalletRecord(userIdentifier, money, remark);
            WithdrawApply withdrawApply = new WithdrawApply()
            {
                SerialNumber = NumberProvider.BuildNumber(),
                WalletRecord = walletRecord,
                Money = money,
                Status = WithdrawStatus.Withdrawing,
                UserId = userIdentifier.UserId,
                FinanceAccountId = financeAccountId
            };
            WithdrawApplyRepository.Insert(withdrawApply);
            CurrentUnitOfWork.SaveChanges();
            bool autoPayForWithdraw = SettingManager.GetSettingValueForTenant<bool>(AppSettings.Wallet.AutoPayForWithdraw, userIdentifier.TenantId.Value);

            if (autoPayForWithdraw)
            {
                try
                {
                    WithdrawMethod withdrawMethod = (WithdrawMethod)Enum.Parse(typeof(WithdrawMethod), 
                        SettingManager.GetSettingValueForTenant(AppSettings.Wallet.DefaultWithdrawMethod, userIdentifier.TenantId.Value));
                    await ProcessWithdrawAsync(withdrawApply, withdrawMethod);
                }
                catch (Exception e)
                {

                }
            }
            return walletRecord;
        }

        [UnitOfWork]
        public async Task<WithdrawApply> ProcessWithdrawAsync(int withdrawApplyId)
        {
            WithdrawApply withdrawApply = WithdrawApplyRepository.Get(withdrawApplyId);
            WithdrawMethod withdrawMethod = (WithdrawMethod)Enum.Parse(typeof(WithdrawMethod),
                                   SettingManager.GetSettingValueForTenant(AppSettings.Wallet.DefaultWithdrawMethod, withdrawApply.TenantId));
        
            return await ProcessWithdrawAsync(withdrawApply, withdrawMethod);
        }

        [UnitOfWork]
        public async Task<WithdrawApply> ProcessWithdrawAsync(int withdrawApplyId, WithdrawMethod withdrawMethod)
        {
            WithdrawApply withdrawApply = WithdrawApplyRepository.Get(withdrawApplyId);
            return await ProcessWithdrawAsync(withdrawApply, withdrawMethod);
        }

        [UnitOfWork]
        public async Task<WithdrawApply> ProcessWithdrawAsync(WithdrawApply withdrawApply, WithdrawMethod withdrawMethod = WithdrawMethod.WeixinQiyePay)
        {
            CurrentUnitOfWork.SetTenantId(withdrawApply.TenantId);
            withdrawApply.WithdrawMethod = withdrawMethod;
            WithdrawApplyRepository.Update(withdrawApply);
            IWithdrawProvider withdrawProvider = IocManager.Instance.Resolve<IWithdrawProvider>();

            switch (withdrawMethod)
            {
                case WithdrawMethod.WeixinQiyePay:
                    withdrawProvider = IocManager.Instance.Resolve<WeixinWithdrawProvider>();
                    break;
                case WithdrawMethod.WeixinRedPack:
                    withdrawProvider = IocManager.Instance.Resolve<RedPackWithdrawProvider>();
                    break;
                case WithdrawMethod.Manual:
                    withdrawProvider = IocManager.Instance.Resolve<ManualWithdrawProvider>();
                    break;
            }

            try
            {
                await withdrawProvider.Withdraw(withdrawApply);
            }
            catch (WithdrawException exception)
            {
                await HandleOnWithdraw(withdrawApply.Id, exception.PayType, false, exception.Message);
                throw exception;
            }
            return withdrawApply;
        }

        [UnitOfWork]
        public async Task<WithdrawApply> HandleOnWithdraw(int withdrawApplyId, PayType payType, bool success, string failReason = null)
        {
            WithdrawApply withdrawApply = WithdrawApplyRepository.Get(withdrawApplyId);
            withdrawApply.PayType = payType;
            User user = withdrawApply.User;

            if (user == null)
            {
                user = UserRepository.Get(withdrawApply.UserId);
            }

            if (success)
            {
                withdrawApply.WalletRecord.PayType = payType;
                withdrawApply.WalletRecord.FetchStatus = FetchStatus.Success;
                withdrawApply.WalletRecord.FailReason = "";
                WalletRecordRepository.Update(withdrawApply.WalletRecord);

                withdrawApply.FailReason = "";
                withdrawApply.Status = WithdrawStatus.Success;
                WithdrawApplyRepository.Update(withdrawApply);
                CurrentUnitOfWork.SaveChanges();

                await AppNotifier.WithdrawSuccess(user, withdrawApply);
            }
            else
            {
                if (string.IsNullOrEmpty(failReason))
                {
                    failReason = L("UnKnowFail");
                }
                withdrawApply.WalletRecord.FetchStatus = FetchStatus.Fail;
                withdrawApply.WalletRecord.FailReason = failReason;
                WalletRecordRepository.Update(withdrawApply.WalletRecord);

                withdrawApply.FailReason = failReason;
                withdrawApply.Status = WithdrawStatus.Fail;
                WithdrawApplyRepository.Update(withdrawApply);
                CurrentUnitOfWork.SaveChanges();

                await AppNotifier.WithdrawFail(user, withdrawApply, failReason);
            }
            return withdrawApply;
        }
    }
}
