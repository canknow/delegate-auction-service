﻿using Application.Entities;
using Application.FinanceAccounts.Entities;

namespace Application.Wallets
{
    public enum WithdrawStatus
    {
        Withdrawing,
        Success,
        Fail,
        Invalid
    }

    public enum WithdrawMethod
    {
        WeixinQiyePay,
        WeixinRedPack,
        Manual
    }

    public class WithdrawApply: FullAuditedUserIdentifierEntity
    {
        public string SerialNumber { get; set; }

        public decimal Money { get; set; }

        public PayType? PayType { get; set; }

        public WithdrawStatus Status { get; set; }

        public WithdrawMethod WithdrawMethod { get; set; }

        public virtual WalletRecord WalletRecord { get; set; }

        public string FailReason { get; set; }

        public int? FinanceAccountId { get; set; }

        public virtual FinanceAccount FinanceAccount { get; set; }

        public string GetPayTypeText()
        {
            return PayType.HasValue? PayType.ToString() : null;
        }

        public string GetWithdrawMethodText ()
        {
            return WithdrawMethod.ToString();
        }

        public string GetStatusText()
        {
            return Status.ToString();
        }
    }
}
