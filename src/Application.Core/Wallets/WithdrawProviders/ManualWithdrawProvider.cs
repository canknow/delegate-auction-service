﻿using Application.Wallets;
using Abp.Domain.Uow;
using System.Threading.Tasks;

namespace Application.Wallets.WithdrawProviders
{
    public class ManualWithdrawProvider : WithdrawProviderBase, IWithdrawProvider
    {
        public new static PayType PayType { get; set; } = PayType.Undetermined;
        public WithdrawManager WithdrawManager { get; set; }

        [UnitOfWork]
        public async Task Withdraw(WithdrawApply withdrawApply)
        {
            WithdrawManager.HandleOnWithdraw(withdrawApply.Id, PayType, true);
        }
    }
}
