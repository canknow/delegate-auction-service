﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Application.Pays.PayProviders;
using Application.Web;
using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Application.Wallets.WithdrawProviders
{
    public class WeixinWithdrawProvider : WithdrawProviderBase, IWithdrawProvider
    {
        public IRepository<UserLogin, long> userLoginRepository { get; set; }
        public WithdrawManager WithdrawManager { get; set; }
        public PathHelper PathHelper { get; set; }
        public ServerContextHelper ServerContextHelper { get; set; }
        public WeixinPayProvider WechatPayProvider { get; set; }
        public new static PayType PayType { get; set; } = PayType.WeixinQiyePay;

        [UnitOfWork]
        public async Task Withdraw(WithdrawApply withdrawApply)
        {
            using (CurrentUnitOfWork.SetTenantId(withdrawApply.TenantId))
            {
                try
                {
                    bool isSuccess = await WechatPayProvider.PayToUser(withdrawApply.UserId, withdrawApply.TenantId, withdrawApply.Money, withdrawApply.SerialNumber, L("Withdraw"));
                    WithdrawManager.HandleOnWithdraw(withdrawApply.Id, PayType, isSuccess);
                }
                catch(Exception e)
                {
                    WithdrawManager.HandleOnWithdraw(withdrawApply.Id, PayType, false, e.Message);
                }
            }
        }

        private bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
