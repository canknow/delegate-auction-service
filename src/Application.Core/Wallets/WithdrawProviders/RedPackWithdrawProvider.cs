﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Application.Configuration;
using Application.Web;
using Senparc.Weixin.TenPay.V3;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Wallets.WithdrawProviders
{
    public class RedPackWithdrawProvider : WithdrawProviderBase, IWithdrawProvider
    {
        public IRepository<UserLogin, long> userLoginRepository { get; set; }
        public WithdrawManager WithdrawManager { get; set; }
        public PathHelper PathHelper { get; set; }
        public ServerContextHelper ServerContextHelper { get; set; }
        public new static PayType PayType { get; set; } = PayType.WeixinRedPack;

        [UnitOfWork]
        public async Task Withdraw(WithdrawApply withdrawApply)
        {
            using (CurrentUnitOfWork.SetTenantId(withdrawApply.TenantId))
            {
                string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, withdrawApply.TenantId);
                string mchId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.MchId, withdrawApply.TenantId);
                string key = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.Key, withdrawApply.TenantId);

                UserLogin userLogin = userLoginRepository.GetAll().Where(model => model.UserId == withdrawApply.UserId
                && model.LoginProvider == "Weixin").FirstOrDefault();

                if (userLogin == null)
                {
                    throw new WithdrawException(PayType, L("TheUserHasNoWeixinLogin"));
                }
                string openId = userLogin.ProviderKey;
                string nonce = TenPayV3Util.GetNoncestr();

                string nonceStr;//随机字符串
                string paySign;//签名

                //本地或者服务器的证书位置（证书在微信支付申请成功发来的通知邮件中）
                string cert = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.SslcertPath, withdrawApply.TenantId);

                var sendNormalRedPackResult = RedPackApi.SendNormalRedPack(
                    appId, 
                    mchId, 
                    key,
                    PathHelper.GetAbsolutePath(cert),     //证书物理地址
                    openId,   //接受收红包的用户的openId
                    mchId,             //红包发送者名称
                    ServerContextHelper.GetUserAddressIP(),      //IP
                    (int) withdrawApply.Money*100,                          //付款金额，单位分
                    L("ThankYouForYourPatronage"),                 //红包祝福语
                    L("WalletWithdaw"),                   //活动名称
                    L("WalletWithdaw"),                   //备注信息
                    out nonceStr,
                    out paySign,
                    null,                         //场景id（非必填）
                    null,                         //活动信息（非必填）
                    null                          //资金授权商户号，服务商替特约商户发放时使用（非必填）
                    );

                if (sendNormalRedPackResult.return_code == "FAIL")
                {
                    WithdrawManager.HandleOnWithdraw(withdrawApply.Id, PayType, false, sendNormalRedPackResult.return_msg);
                }
                bool success = sendNormalRedPackResult.result_code == "FAIL" ? false : true;
                WithdrawManager.HandleOnWithdraw(withdrawApply.Id, PayType, success, sendNormalRedPackResult.err_code_des);
            }
        }
    }
}
