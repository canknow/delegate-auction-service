﻿using Application.MultiTenancy;
using Abp.Dependency;

namespace Application.Finance.Wallets
{
    public class WalletHelper
    {
        public static string GetWalletUrl(int? tenantId=null)
        {
            TenantHelper TenantHelper = IocManager.Instance.Resolve<TenantHelper>();
            return TenantHelper.GetTenantBaseUrl(tenantId) + "/app/mobile/index.html#/wallet/index";
        }
    }
}
