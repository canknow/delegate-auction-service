﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.MultiTenancy;

namespace Application.MultiTenancy
{
    public static class TenantStoreExtensions
    {
        public static TenantInfo FindByIndependentDomain(this ITenantStore tenantStore, string domain)
        {
            IRepository<Tenant> tenantRepository = IocManager.Instance.Resolve<IRepository<Tenant>>();
            var tenent = tenantRepository.FirstOrDefault(model => model.IndependentDomain == domain);
            if (tenent != null)
            {
                return new TenantInfo(tenent.Id, tenent.TenancyName);
            }
            else
            {
                return null;
            }
        }
    }
}
