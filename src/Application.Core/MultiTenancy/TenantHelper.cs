﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Application.Url;
using System;
using System.Linq;

namespace Application.MultiTenancy
{
    public class TenantHelper : ITransientDependency
    {
        private Tenant _tenant;
        public IRepository<Tenant> TenantRepository { get; set; }
        public IWebUrlService WebUrlService { get; set; }

        public string GetTenantDomain(string tenancyName)
        {
            return GetTenantBaseUrl(tenancyName).Replace("http://", "").Replace("/", "");
        }

        public string GetTenantBaseUrl(string tenancyName)
        {
            Tenant tenant = TenantRepository.GetAll().Where(model => model.TenancyName == tenancyName).FirstOrDefault();

            if (!string.IsNullOrEmpty(tenant.IndependentDomain))
            {
                return "http://" + tenant.IndependentDomain + "/";
            }
            return WebUrlService.GetWebSiteRootAddress(tenant.TenancyName);
        }

        public string GetTenantBaseUrl(int? tenantId = null)
        {
            Tenant tenant = GetTenant(tenantId);
            return GetTenantBaseUrl(tenant.TenancyName);
        }

        public string GetTenantAdminUrl(Tenant tenant)
        {
            string baseUrl = GetTenantBaseUrl(tenant.TenancyName);
            return baseUrl + "app/manage/index.html";
        }

        public string GetCurrentTenancyNameOrNull()
        {
            Tenant tenant = GetTenant();
            if (tenant == null)
            {
                return null;
            }
            return tenant.TenancyName;
        }

        [UnitOfWork]
        public Tenant GetTenant(int? tenantId = null)
        {
            if (_tenant != null)
            {
                return _tenant;
            }
            IAbpSession abpSession = IocManager.Instance.Resolve<IAbpSession>();
            IRepository<Tenant> tenantRepository = IocManager.Instance.Resolve<IRepository<Tenant>>();

            if (tenantId.HasValue)
            {
                _tenant = tenantRepository.Get(tenantId.Value);
                if (_tenant == null)
                {
                    throw new Exception("UnknownTenantId");
                }

                if (!_tenant.IsActive)
                {
                    throw new Exception("TenantIdIsNotActive");
                }
            }
            else if (abpSession.TenantId.HasValue)
            {
                _tenant = tenantRepository.Get(abpSession.TenantId.Value);
            }
            return _tenant;
        }
    }
}
