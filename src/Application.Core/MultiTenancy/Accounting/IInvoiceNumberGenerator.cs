﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace Application.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}