﻿using Application.Authorization.Users;
using Application.MultiTenancy;
using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Application.Groups
{
    public class GroupManager: ApplicationDomainServiceBase
    {
        public TenantManager TenantManager { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
    }
}
