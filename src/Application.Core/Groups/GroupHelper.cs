﻿using Application.MultiTenancy;
using Abp.Dependency;

namespace Application.Groups
{
    public class GroupHelper
    {
        public static string GetGroupUrl(int tenantId)
        {
            TenantHelper TenantHelper = IocManager.Instance.Resolve<TenantHelper>();
            return TenantHelper.GetTenantBaseUrl(tenantId) + "/app/mobile/index.html#/group/index";
        }
    }
}
