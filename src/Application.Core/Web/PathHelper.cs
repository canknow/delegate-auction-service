﻿using Abp.Dependency;

namespace Application.Web
{
    public class PathHelper: ISingletonDependency
    {
        public ServerContextHelper ServerContextHelper { get; set; }

        public string GetAbsolutePath(string path, bool isAppRoot=false)
        {
            if(string.IsNullOrEmpty(path))
            {
                return path;
            }
            if (path.Contains("//"))
            {
                return path;
            }
            return ServerContextHelper.GetMapPath(path, isAppRoot);
        }

        public string GetAbsolutePathByApp(string path)
        {
            return GetAbsolutePath(path, true);
        }
    }
}
