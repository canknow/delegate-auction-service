﻿using Abp.Dependency;

namespace Application.Web
{
    public static class UrlHelper
    {
        // 根据当前request获取完整url
        public static string GetFullUrl(string url)
        {
            ServerContextHelper serverContextHelper = IocManager.Instance.Resolve<ServerContextHelper>();
            var request = serverContextHelper.HttpContext.Request;
            return request.Scheme + System.Uri.SchemeDelimiter + request.Host + url;
        }
    }
}
