﻿using Abp.Dependency;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using Utility.Web;

namespace Application.Web
{
    public class ServerContextHelper: ITransientDependency
    {
        private string _appRootPath;
        private string _webRootPath;
        protected IHttpContextAccessor HttpContextAccessor;
        protected IHostingEnvironment HostingEnvironment;

        public ServerContextHelper(IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment)
        {
            HttpContextAccessor = httpContextAccessor;
            HostingEnvironment = hostingEnvironment;
        }

        public HttpContext HttpContext
        {
            get
            {
                return HttpContextAccessor.HttpContext;
            }
        }

        public string GetUserAddressIP()
        {
              return HttpContext.GetClientUserIp();
        }

        public string GetServerIp()
        {
            string ip = HttpContext.Connection.LocalIpAddress.ToString();
            return HttpContext.Request.Headers["X-Real-IP"].ToString();
        }

        public string AppRootPath
        {
            get
            {
                if (_appRootPath == null)
                {
                    _appRootPath = HostingEnvironment.ContentRootPath;
                }
                return _appRootPath;
            }
            set
            {
                _appRootPath = value;
            }
        }

        public string WebRootPath
        {
            get
            {
                if (_webRootPath == null)
                {
                    _webRootPath = HostingEnvironment.WebRootPath;
                }
                return _webRootPath;
            }
            set
            {
                _webRootPath = value;
            }
        }

        public string GetAppMapPath(string virtualPath)
        {
            return GetMapPath(virtualPath, true);
        }

        public string GetMapPath(string virtualPath, bool isAppRoot = false)
        {
            string rootPath = isAppRoot ? AppRootPath : WebRootPath;
            string path = "";

            if (virtualPath == null)
            {
                return path;
            }
            else if (virtualPath.StartsWith("~/"))
            {
                path= virtualPath.Replace("~/", rootPath).Replace('/', Path.DirectorySeparatorChar);
            }
            else
            {
                path = rootPath + virtualPath.Replace('/', Path.DirectorySeparatorChar);
            }
            return path;
        }
    }
}
