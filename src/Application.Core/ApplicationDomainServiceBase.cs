﻿using Abp.Domain.Services;
using Abp.Events.Bus;
using Abp.Runtime.Session;
using System;
using System.Threading.Tasks;

namespace Application
{
    public abstract class ApplicationDomainServiceBase : DomainService
    {
        public IAbpSession AbpSession { get; set; }
        public IEventBus EventBus { get; set; }

        /* Add your common members for all your domain services. */

        protected ApplicationDomainServiceBase()
        {
            LocalizationSourceName = ApplicationConsts.LocalizationSourceName;
        }

        protected virtual async Task TriggerEventWithEntity(Type genericEventType, object entity, bool triggerInCurrentUnitOfWork)
        {
            var entityType = entity.GetType();
            var eventType = genericEventType.MakeGenericType(entityType);

            if (triggerInCurrentUnitOfWork || CurrentUnitOfWork == null)
            {
                await EventBus.TriggerAsync(eventType, (IEventData)Activator.CreateInstance(eventType, new[] { entity }));
                return;
            }
            CurrentUnitOfWork.Completed += async (sender, args) =>await EventBus.TriggerAsync(eventType, (IEventData)Activator.CreateInstance(eventType, new[] { entity }));
        }
    }
}
