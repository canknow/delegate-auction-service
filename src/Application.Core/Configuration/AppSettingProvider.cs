﻿using Abp.Configuration;
using Abp.Zero.Configuration;
using Application.Distributions;
using Application.Orders;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Application.Configuration
{
    public class AppSettingProvider : ApplicationSettingProvider
    {
        public AppSettingProvider(IAppConfigurationAccessor configurationAccessor) :base(configurationAccessor)
        {
        }

        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            //Disable TwoFactorLogin by default (can be enabled by UI)
            context.Manager.GetSettingDefinition(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled).DefaultValue = false.ToString().ToLowerInvariant();

            return GetHostSettings().Union(GetTenantSettings()).Union(GetSharedSettings());

        }

        private IEnumerable<SettingDefinition> GetHostSettings()
        {
            return new[] {
                new SettingDefinition(AppSettings.General.WebSiteRootAddress, "http://{TENANCY_NAME}.domain/"),
                new SettingDefinition(AppSettings.General.WebSiteStatus, "true"),
                new SettingDefinition(AppSettings.General.AppName, ConfigurationManager.AppSettings[AppSettings.General.AppName] ?? "Canknow"),
                new SettingDefinition(AppSettings.General.AppLogo, ConfigurationManager.AppSettings[AppSettings.General.AppLogo] ?? ""),
                new SettingDefinition(AppSettings.General.AppTitleLogo, ConfigurationManager.AppSettings[AppSettings.General.AppTitleLogo] ?? ""),

                new SettingDefinition(AppSettings.TenantManagement.AllowSelfRegistration, GetFromAppSettings(AppSettings.TenantManagement.AllowSelfRegistration, "true"), isVisibleToClients: true),
                new SettingDefinition(AppSettings.TenantManagement.IsNewRegisteredTenantActiveByDefault, GetFromAppSettings(AppSettings.TenantManagement.IsNewRegisteredTenantActiveByDefault, "false")),
                new SettingDefinition(AppSettings.TenantManagement.UseCaptchaOnRegistration, GetFromAppSettings(AppSettings.TenantManagement.UseCaptchaOnRegistration, "true"), isVisibleToClients: true),
                new SettingDefinition(AppSettings.TenantManagement.DefaultEdition, GetFromAppSettings(AppSettings.TenantManagement.DefaultEdition, "")),
                new SettingDefinition(AppSettings.TenantManagement.SubscriptionExpireNotifyDayCount, GetFromAppSettings(AppSettings.TenantManagement.SubscriptionExpireNotifyDayCount, "7"), isVisibleToClients: true),

                new SettingDefinition(AppSettings.HostManagement.BillingLegalName, GetFromAppSettings(AppSettings.HostManagement.BillingLegalName, "")),
                new SettingDefinition(AppSettings.HostManagement.BillingAddress, GetFromAppSettings(AppSettings.HostManagement.BillingAddress, "")),

                new SettingDefinition(AppSettings.UserManagement.SmsVerificationEnabled, GetFromAppSettings(AppSettings.UserManagement.SmsVerificationEnabled, "false"), isVisibleToClients: true),
                new SettingDefinition(AppSettings.Recaptcha.SiteKey, GetFromSettings("Recaptcha:SiteKey"), isVisibleToClients: true),
            };
        }

        private IEnumerable<SettingDefinition> GetTenantSettings()
        {
            return new[]
            {
                new SettingDefinition(AppSettings.UserManagement.AllowSelfRegistration, GetFromAppSettings(AppSettings.UserManagement.AllowSelfRegistration, "true"), scopes: SettingScopes.Tenant, isVisibleToClients: true),
                new SettingDefinition(AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault, GetFromAppSettings(AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault, "false"), scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.UserManagement.UseCaptchaOnRegistration, GetFromAppSettings(AppSettings.UserManagement.UseCaptchaOnRegistration, "true"), scopes: SettingScopes.Tenant, isVisibleToClients: true),
                new SettingDefinition(AppSettings.TenantManagement.BillingLegalName, GetFromAppSettings(AppSettings.TenantManagement.BillingLegalName, ""), scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.TenantManagement.BillingAddress, GetFromAppSettings(AppSettings.TenantManagement.BillingAddress, ""), scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.TenantManagement.BillingTaxVatNo, GetFromAppSettings(AppSettings.TenantManagement.BillingTaxVatNo, ""), scopes: SettingScopes.Tenant),

                new SettingDefinition(AppSettings.Wallet.NeedFinanceAccountForWithdraw,ConfigurationManager.AppSettings[AppSettings.Wallet.NeedFinanceAccountForWithdraw] ?? "true",scopes: SettingScopes.Tenant,isVisibleToClients:true),
                new SettingDefinition(AppSettings.Wallet.DefaultWithdrawMethod, ConfigurationManager.AppSettings[AppSettings.Wallet.DefaultWithdrawMethod] ??DistributionWhen.Payed.ToString(),scopes: SettingScopes.Tenant,isVisibleToClients:true),
                new SettingDefinition(AppSettings.Wallet.AutoWithdraw, "true",scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.Wallet.AutoPayForWithdraw, "true",scopes: SettingScopes.Tenant),

                new SettingDefinition(AppSettings.Order.AutoReceiveLimit, "0",scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.Order.ShouldHasParentForBuy, "false",scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.Order.OverTimeForDelete, "0",scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.Order.DecreaseStockWhen, DecreaseStockWhen.Create.ToString(),scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.Order.RebateGuarantee, "false",scopes: SettingScopes.Tenant),
                new SettingDefinition(AppSettings.Order.DistributionWhen, "0",scopes: SettingScopes.Tenant),
            };
        }

        private IEnumerable<SettingDefinition> GetSharedSettings()
        {
            return new[]
            {
                new SettingDefinition(AppSettings.UserManagement.TwoFactorLogin.IsGoogleAuthenticatorEnabled, GetFromAppSettings(AppSettings.UserManagement.TwoFactorLogin.IsGoogleAuthenticatorEnabled, "false"), scopes: SettingScopes.Application | SettingScopes.Tenant, isVisibleToClients: true)
            };
        }
    }
}
