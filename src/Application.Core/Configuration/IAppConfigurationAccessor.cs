﻿using Microsoft.Extensions.Configuration;

namespace Application.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
