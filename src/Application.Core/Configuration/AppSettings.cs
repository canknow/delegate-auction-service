﻿namespace Application.Configuration
{
    public static class AppSettings
    {
        public const string UiTheme = "App.UiTheme";

        public static class HostManagement
        {
            public const string BillingLegalName = "App.HostManagement.BillingLegalName";
            public const string BillingAddress = "App.HostManagement.BillingAddress";
        }

        public static class General
        {
            public const string WebSiteRootAddress = "App.General.WebSiteRootAddress";
            public const string WebSiteStatus = "App.General.WebSiteStatus";
            public const string AppName = "App.General.Name";
            public const string AppLogo = "App.General.Logo";
            public const string AppTitleLogo = "App.General.TitleLogo";
        }

        public static class TenantManagement
        {
            public const string AllowSelfRegistration = "App.TenantManagement.AllowSelfRegistration";
            public const string IsNewRegisteredTenantActiveByDefault = "App.TenantManagement.IsNewRegisteredTenantActiveByDefault";
            public const string UseCaptchaOnRegistration = "App.TenantManagement.UseCaptchaOnRegistration";
            public const string DefaultEdition = "App.TenantManagement.DefaultEdition";
            public const string TrialTime = "App.TenantManagement.TrialTime";
            public const string SubscriptionExpireNotifyDayCount = "App.TenantManagement.SubscriptionExpireNotifyDayCount";
            public const string BillingLegalName = "App.UserManagement.BillingLegalName";
            public const string BillingAddress = "App.UserManagement.BillingAddress";
            public const string BillingTaxVatNo = "App.UserManagement.BillingTaxVatNo";
        }

        public static class UserManagement
        {
            public static class TwoFactorLogin
            {
                public const string IsGoogleAuthenticatorEnabled = "App.UserManagement.TwoFactorLogin.IsGoogleAuthenticatorEnabled";
            }
            public const string AllowSelfRegistration = "App.UserManagement.AllowSelfRegistration";
            public const string IsNewRegisteredUserActiveByDefault = "App.UserManagement.IsNewRegisteredUserActiveByDefault";
            public const string UseCaptchaOnRegistration = "App.UserManagement.UseCaptchaOnRegistration";
            public const string SmsVerificationEnabled = "App.UserManagement.SmsVerificationEnabled";
        }

        public static class Security
        {
            public const string PasswordComplexity = "App.Security.PasswordComplexity";
            public const string SmsTemplateCommonCode = "App.Security.SmsTemplateCommonCode";
        }

        public static class Recaptcha
        {
            public const string SiteKey = "Recaptcha.SiteKey";
        }

        public static class CacheKeys
        {
            public const string TenantRegistrationCache = "TenantRegistrationCache";
        }

        public static class Wallet
        {
            public const string NeedFinanceAccountForWithdraw = "App.Wallet.NeedFinanceAccountForWithdraw";
            public const string DefaultWithdrawMethod = "App.Wallet.DefaultWithdrawMethod";
            public const string AutoWithdraw = "App.Wallet.AutoWithdraw";
            public const string AutoPayForWithdraw = "App.Wallet.AutoPayForWithdraw";
        }

        public static class Order
        {
            public const string OverTimeForDelete = "App.Order.OverTimeForDelete";
            public const string ShouldHasParentForBuy = "App.Order.ShouldHasParentForBuy";
            public const string AutoReceiveLimit = "App.Order.AutoReceiveLimit";
            public const string DecreaseStockWhen = "App.General.DecreaseStockWhen";
            public const string RebateGuarantee = "App.Distribution.RebateGuarantee";
            public const string DistributionWhen = "App.Distribution.DistributionWhen";
        }
    }
}
