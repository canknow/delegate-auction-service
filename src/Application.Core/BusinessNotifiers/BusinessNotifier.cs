﻿using Application.Authorization.Users;
using Application.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.BusinessNotifiers
{
    public class BusinessNotifier : FullAuditedEntity, IUserIdentifierEntity
    {
        public int TenantId { get; set; }

        public bool Status { get; set; }

        public long UserId { get; set; }

        public virtual User User { get;set;}
    }
}
