﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System.Collections.Generic;
using System.Linq;

namespace Application.BusinessNotifiers
{
    public class BusinessNotiferManager: ApplicationDomainServiceBase
    {
        public IRepository<BusinessNotifier> BusinessNotifierRepository { get; set; }

        [UnitOfWork]
        public List<BusinessNotifier> GetBusinessNotifiers()
        {
            List<BusinessNotifier> _businessNotifiers = BusinessNotifierRepository.GetAll().Where(model => model.Status).ToList();
    
            return _businessNotifiers;
        }
    }
}
