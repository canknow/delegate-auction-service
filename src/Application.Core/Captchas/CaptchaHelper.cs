﻿using Abp.Domain.Entities.Auditing;
using System;

namespace Application.Captchas
{
    public class CaptchaHelper: FullAuditedEntity
    {
        public static string CreateCode()
        {
            Random random = new Random();
            int value = random.Next(1000, 10000);
            return value.ToString();
        }
    }
}
