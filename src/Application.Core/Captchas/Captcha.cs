﻿using Abp.Domain.Entities.Auditing;

namespace Application.Captchas
{
    public class Captcha: FullAuditedEntity
    {
        public string Code { get; set; }

        public string ProviderKey { get; set; }

        public string Provider { get; set; }

        public VerificationState VerificationState { get; set; }
    }

    public enum VerificationState
    {
        UnVerified,
        Verified,
        Expired
    }
}
