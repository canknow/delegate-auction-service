﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.Articles
{
    public class ArticleCategory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Name { get; set; }

        public string Icon { get; set; }
    }
}
