﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.Articles
{
    public class Article: FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Title { get; set; }

        public virtual ArticleCategory ArticleCategory { get; set; }

        public int ArticleCategoryId { get; set; }

        public ArticleStatus ArticleStatus { get; set; }

        public string Description { get; set; }

        public string Thumbnail { get; set; }

        public string Author { get; set; }

        public string Content { get; set; }

        public int Hint { get; set; }

        public int Like { get; set; }

        public int DisLike { get; set; }

        public ArticleStatus Status { get; set; }
    }
}
