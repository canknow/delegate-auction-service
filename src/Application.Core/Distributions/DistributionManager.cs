﻿using Application.Authorization.Users;
using Application.Orders;
using Application.Wallets;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Distributions
{
    public class DistributionManager: ApplicationDomainServiceBase
    {
        public WalletManager WalletManager { get; set; }
        public UserManager UserManager { get; set; }
        private IRepository<User,long> _userRepository;
        private IRepository<OrderDistribution> _orderDistributionRepository;
        private IRepository<Order> _orderRepository;

        public DistributionManager(
            IRepository<User,long> userRepository,
            IRepository<OrderDistribution> orderDistributionRepository,
            IRepository<Order> orderRepository)
        {
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _orderDistributionRepository = orderDistributionRepository;
        }

        [UnitOfWork]
        public Task<List<OrderDistribution>> TryAndCreateOrderDistributionAsync(int orderId)
        {
            Order order = _orderRepository.Get(orderId);
            return TryAndCreateOrderDistributionAsync(order);
        }

        [UnitOfWork]
        public async Task<List<OrderDistribution>> TryAndCreateOrderDistributionAsync(Order order)
        {
            List<OrderDistribution> orderDistributions = new List<OrderDistribution>();

            using (CurrentUnitOfWork.SetTenantId(order.TenantId))
            {
                if (order.Package.Distributions.Count() > 0)
                {
                    foreach (Distribution distribution in order.Package.Distributions)
                    {
                        User user = GetDistributionUser(order.User, distribution);

                        if (user != null)
                        {
                            OrderDistribution orderDistribution = await CreateOrderDistributionAsync(user, distribution, order);

                            if (orderDistribution != null)
                            {
                                orderDistributions.Add(orderDistribution);
                            }
                        }
                    }
                }
                return orderDistributions;
            }
        }

        private decimal GetDistributionMoney(Distribution distribution, Order order)
        {
            return distribution.Money > 0 ? distribution.Money: (decimal)distribution.Ratio * order.Money;
        }

        [UnitOfWork]
        public async Task<OrderDistribution> CreateOrderDistributionAsync(User user,Distribution distribution,Order order)
        {
            decimal money = GetDistributionMoney(distribution,order);

            if (money == 0)
            {
                return null;
            }
            OrderDistribution orderDistribution = new OrderDistribution()
            {
                OrderId= order.Id,
                Money= money,
                OrderMoney=order.Money,
                UserId=user.Id,
                DistributionId=distribution.Id
            };
            _orderDistributionRepository.Insert(orderDistribution);
            CurrentUnitOfWork.SaveChanges();
            await WalletManager.IncomeOfOrderRebateAsync(
                user.ToUserIdentifier(), 
                money,
                string.Format(L("DistributionOfLevel"),order.User.NickName,distribution.Level),
                order);
            return orderDistribution;
        }

        [UnitOfWork]
        public User GetDistributionUser(User buyerUser, Distribution distribution)
        {
            User distributionUser = UserManager.GetParentUserOfDepth(buyerUser, distribution.Level);
            return distributionUser;
        }
    }
}
