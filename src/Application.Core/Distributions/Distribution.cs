﻿using Abp.Domain.Entities;
using Application.Packages;
using System.ComponentModel.DataAnnotations;

namespace Application.Distributions
{
    public enum DistributionWhen
    {
        Create,
        Passed,
        Payed,
        Complete
    }

    public enum BuyWhen
    {
        NoLimit,
        First,
        Next
    }

    public class Distribution:Entity
    {
        public int? PackageId { get; set; }

        public virtual Package Package { get; set; }

        public BuyWhen BuyWhen { get; set; }

        [Required]
        public int Level { get; set; }

        public decimal Money { get; set; }

        public float Ratio { get; set; }
    }
}
