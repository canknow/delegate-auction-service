﻿using Abp;
using Abp.Configuration;
using Application.Configuration;
using Application.Orders;
using Application.Wallets;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Distributions
{
    public class RebateManage: ApplicationDomainServiceBase
    {
        public DistributionManager DistributionManager { get; set; }
        public WalletManager WalletManager { get; set; }
        public WithdrawManager WithdrawManager { get; set; }

        public async Task ProcessRebate(Order order)
        {
            List<OrderDistribution> orderDistributions = await DistributionManager.TryAndCreateOrderDistributionAsync(order);
            bool rebateGuarantee = SettingManager.GetSettingValueForTenant<bool>(AppSettings.Order.RebateGuarantee, order.TenantId);
            bool autoWithdraw = SettingManager.GetSettingValueForTenant<bool>(AppSettings.Wallet.AutoWithdraw, order.TenantId);

            List<long> userList = new List<long>();

            foreach (OrderDistribution orderDistribution in orderDistributions)
            {
                if (!userList.Contains(orderDistribution.UserId))
                {
                    userList.Add(orderDistribution.UserId);
                }
            }

            try
            {
                foreach (long userId in userList)
                {
                    if (!rebateGuarantee && autoWithdraw)
                    {
                        try
                        {
                            WithdrawManager.AddWithdrawAllBalanceJob(new UserIdentifier(order.TenantId, userId));
                        }
                        catch
                        {

                        }
                    }
                }
            }
            catch(Exception e)
            {

            }
        }
    }
}
