﻿namespace Application.BackgroundJob
{
    public class WithdrawAllBalanceJobArgs
    {
        public int TenantId { get; set; }

        public long UserId { get; set; }
    }
}
