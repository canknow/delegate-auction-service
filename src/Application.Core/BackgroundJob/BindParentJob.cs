﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Application.Authorization.Users;

namespace Application.BackgroundJob
{
    public class BindParentJob : BackgroundJob<BindParentJobArgs>, ITransientDependency
    {
        public IRepository<User, long> UserRepository { get; set; }
        public UserManager UserManager { get; set; }

        public BindParentJob()
        {
        }

        [UnitOfWork]
        public override void Execute(BindParentJobArgs args)
        {
            CurrentUnitOfWork.SetTenantId(args.TenantId);
            AsyncHelper.RunSync(async () =>
            {
                var sourceUser = UserRepository.Get(args.SourceUserId);
                var parentUser = UserRepository.Get(args.ParentUserId);
                await UserManager.BindParentAsync(sourceUser, parentUser, false, false, args.BindParentType);
            });
        }
    }
}
