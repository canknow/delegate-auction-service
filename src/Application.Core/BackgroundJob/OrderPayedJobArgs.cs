﻿namespace Application.BackgroundJob
{
    public class OrderPayedJobArgs
    {
        public int TenantId { get; set; }

        public int OrderId { get; set; }
    }
}
