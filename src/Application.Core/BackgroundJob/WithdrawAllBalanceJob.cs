﻿using Abp;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using Application.Wallets;

namespace Application.BackgroundJob
{
    public class WithdrawAllBalanceJob : BackgroundJob<WithdrawAllBalanceJobArgs>, ITransientDependency
    {
        public WithdrawManager WithdrawManager { get; set; }

        [UnitOfWork]
        public override void Execute(WithdrawAllBalanceJobArgs args)
        {
            CurrentUnitOfWork.SetTenantId(args.TenantId);
            AsyncHelper.RunSync(async () =>
            {
                await WithdrawManager.WithdrawAllBalanceAsync(new UserIdentifier(args.TenantId, args.UserId), null);
            });
        }
    }
}
