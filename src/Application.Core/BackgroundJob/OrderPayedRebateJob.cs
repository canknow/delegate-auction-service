﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Application.Distributions;
using Application.Orders;

namespace Application.BackgroundJob
{
    public class OrderPayedRebateJob : BackgroundJob<OrderPayedJobArgs>, ITransientDependency
    {
        public IRepository<Order> OrderRepository { get; set; }
        public RebateManage RebateManage { get; set; }

        [UnitOfWork]
        public override void Execute(OrderPayedJobArgs args)
        {
            CurrentUnitOfWork.SetTenantId(args.TenantId);
            AsyncHelper.RunSync(async () =>
            {
                Order order = OrderRepository.Get(args.OrderId);
                await RebateManage.ProcessRebate(order);
            });
        }
    }
}
