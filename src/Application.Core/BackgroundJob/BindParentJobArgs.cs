﻿using Application.Authorization.Users;

namespace Application.BackgroundJob
{
    public class BindParentJobArgs
    {
        public int TenantId { get; set; }

        public long SourceUserId { get; set; }

        public long ParentUserId { get; set; }

        public BindParentType BindParentType { get; set; }
    }
}
