﻿using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Application.Packages
{
    public interface IPackageRepository: IRepository<Package>
    {
        DbContext GetContext();
    }
}
