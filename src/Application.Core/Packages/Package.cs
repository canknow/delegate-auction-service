﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Application.Distributions;
using Application.Teams;
using System.Collections.Generic;

namespace Application.Packages
{
    public class Package : FullAuditedEntity, IMustHaveTenant
    {
        public string Name { get; set; }

        public int TenantId { get; set; }

        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }

        public string GetTeamName ()
        {
            return Team?.Name;
        }

        public string Cover { get; set; }

        public PackageType PackageType { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public float Score { get; set; }

        public int HintCount { get; set; }

        public int OrderCount { get; set; }

        public virtual List<CompensationRule> CompensationRules { get; set; }

        public virtual ICollection<Distribution> Distributions { get; set; }

        public bool IsHot { get; set; }
    }
}
