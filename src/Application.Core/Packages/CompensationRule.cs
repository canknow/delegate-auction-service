﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Packages
{
    public class CompensationRule:Entity
    {
        public int Count { get; set; }

        public int Money { get; set; }
    }
}
