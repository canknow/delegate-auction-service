﻿using Abp.Configuration;
using Application.Configuration;
using Microsoft.Extensions.Configuration;

namespace Application
{
    public abstract class ApplicationSettingProvider: SettingProvider
    {
        protected IConfigurationRoot _appConfiguration;

        public ApplicationSettingProvider(IAppConfigurationAccessor configurationAccessor)
        {
            _appConfiguration = configurationAccessor.Configuration;
        }


        protected string GetFromAppSettings(string name, string defaultValue = null)
        {
            return GetFromSettings("App:" + name, defaultValue);
        }

        protected string GetFromSettings(string name, string defaultValue = null)
        {
            return _appConfiguration[name] ?? defaultValue;
        }
    }
}
