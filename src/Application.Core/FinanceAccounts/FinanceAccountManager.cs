﻿using Application.FinanceAccounts.Entities;
using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Application.FinanceAccounts
{
    public class FinanceAccountManager : ApplicationDomainServiceBase
    {
        public IRepository<FinanceAccount> FinanceAccountRespository { get; set; }

        public FinanceAccount GetDefaultFinanceAccount(long userId)
        {
            FinanceAccount financeAccount = FinanceAccountRespository.GetAll().Where(model => model.UserId == userId&&model.IsDefault == true).FirstOrDefault();
            return financeAccount;
        }

        public FinanceAccount SetAsDefaultFinanceAccount(int financeAccountId, long userId)
        {
            List<FinanceAccount> defaultFinanceAccounts = FinanceAccountRespository.GetAll().Where(model => model.UserId == userId && model.IsDefault == true).ToList();

            foreach(FinanceAccount defaultFinanceAccount in defaultFinanceAccounts)
            {
                defaultFinanceAccount.IsDefault = false;
                FinanceAccountRespository.Update(defaultFinanceAccount);
            }
            FinanceAccount financeAccount = FinanceAccountRespository.Get(financeAccountId);
            financeAccount.IsDefault = true;
            FinanceAccountRespository.Update(financeAccount);
            return financeAccount;
        }
    }
}
