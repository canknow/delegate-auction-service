﻿using Abp.Domain.Entities;
using Application.Banks;
using Application.Entities;
using System;

namespace Application.FinanceAccounts.Entities
{
    public enum FinanceAccountType
    {
        BankCard,
        Alipay,
        Weixin
    }
    public class FinanceAccount: FullAuditedUserIdentifierEntity, IMustHaveTenant
    {
        public string Account { get; set; }
        public bool IsDefault { get; set; }
        public string FullName { get; set; }
        public FinanceAccountType Type { get; set; }
        public int? BankId { get; set; }
        public virtual Bank Bank { get; set; }
        public string GetTypeText()
        {
            return Enum.GetName(typeof(FinanceAccountType), Type);
        }
    }
}