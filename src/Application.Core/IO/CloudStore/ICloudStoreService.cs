﻿using Abp.Dependency;
using System.IO;
using System.Threading.Tasks;

namespace Application.IO.CloudStore
{
    public interface ICloudStoreService: ITransientDependency
    {
        Task<string> GetToken();

        Task<string> Upload(Stream stream, string fileName);

        Task<string> Upload(string filePath, string fileName);

        Task<string> GetPublicUrl(string fileName);
    }
}
