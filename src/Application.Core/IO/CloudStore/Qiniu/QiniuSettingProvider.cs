﻿using Abp.Configuration;
using Application.Configuration;
using System.Collections.Generic;
using System.Configuration;

namespace Application.IO.CloudStore.Qiniu
{
    /// <summary>
    /// Defines settings for the application.
    /// See <see cref="AppSettings"/> for setting names.
    /// </summary>
    public class QiniuSettingProvider : ApplicationSettingProvider
    {
        public QiniuSettingProvider(IAppConfigurationAccessor configurationAccessor) : base(configurationAccessor)
        {

        }

        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                new SettingDefinition(QiniuSettings.Status,ConfigurationManager.AppSettings[QiniuSettings.Status] ?? "false",scopes: SettingScopes.All),
                new SettingDefinition(QiniuSettings.AccessKey,ConfigurationManager.AppSettings[QiniuSettings.AccessKey] ?? "",scopes: SettingScopes.All),
                new SettingDefinition(QiniuSettings.SecretKey,ConfigurationManager.AppSettings[QiniuSettings.SecretKey] ?? "",scopes: SettingScopes.All),
                new SettingDefinition(QiniuSettings.Bucket,ConfigurationManager.AppSettings[QiniuSettings.Bucket] ?? "",scopes: SettingScopes.All),
                new SettingDefinition(QiniuSettings.Domain,ConfigurationManager.AppSettings[QiniuSettings.Domain] ?? "",scopes: SettingScopes.All),
            };
        }
    }
}
