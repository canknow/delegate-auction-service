﻿namespace Application.IO.CloudStore.Qiniu
{
    public static class QiniuSettings
    {
        public const string Status = "Qiniu.Status";
        public const string AccessKey = "Qiniu.AccessKey";
        public const string SecretKey = "Qiniu.SecretKey";
        public const string Bucket = "Qiniu.Bucket";
        public const string Domain = "Qiniu.Domain";
    }
}
