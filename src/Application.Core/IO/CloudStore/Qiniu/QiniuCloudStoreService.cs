﻿using Qiniu.Common;
using Qiniu.Http;
using Qiniu.IO;
using Qiniu.IO.Model;
using Qiniu.Util;
using System.IO;
using System.Threading.Tasks;

namespace Application.IO.CloudStore.Qiniu
{
    public class QiniuCloudStoreService : ApplicationDomainServiceBase, ICloudStoreService
    {
        public async Task<string> Upload(Stream stream, string fileName)
        {
            string accessKey = await SettingManager.GetSettingValueAsync(QiniuSettings.AccessKey);
            string secretKey = await SettingManager.GetSettingValueAsync(QiniuSettings.SecretKey);
            string bucket = await SettingManager.GetSettingValueAsync(QiniuSettings.Bucket);

            var policy = new PutPolicy();
            // 上传策略
            PutPolicy putPolicy = new PutPolicy();

            // 设置要上传的目标空间
            putPolicy.Scope = bucket;

            // 上传策略的过期时间(单位:秒)
            putPolicy.SetExpires(3600);

            //// 文件上传完毕后，在多少天后自动被删除
            //putPolicy.DeleteAfterDays = 1;

            // 生成上传凭证
            //参考地址:https://segmentfault.com/q/1010000008205978
            string jsonParam = putPolicy.ToJsonString();
            Mac mac = new Mac(accessKey, secretKey);
            string uploadToken = Auth.CreateUploadToken(mac, jsonParam);

            Config config = new Config();
            FormUploader upload = new FormUploader();
            HttpResult result = await upload.UploadStreamAsync(stream, fileName, uploadToken);

            return await GetPublicUrl(fileName);
        }

        public async Task<string> GetToken()
        {
            string accessKey = await SettingManager.GetSettingValueAsync(QiniuSettings.AccessKey);
            string secretKey = await SettingManager.GetSettingValueAsync(QiniuSettings.SecretKey);
            string bucket = await SettingManager.GetSettingValueAsync(QiniuSettings.Bucket);

            var policy = new PutPolicy();
            // 上传策略
            PutPolicy putPolicy = new PutPolicy();

            // 设置要上传的目标空间
            putPolicy.Scope = bucket;

            // 上传策略的过期时间(单位:秒)
            putPolicy.SetExpires(3600);

            //// 文件上传完毕后，在多少天后自动被删除
            //putPolicy.DeleteAfterDays = 1;

            // 生成上传凭证
            //参考地址:https://segmentfault.com/q/1010000008205978
            string jsonParam = putPolicy.ToJsonString();
            Mac mac = new Mac(accessKey, secretKey);
            string uploadToken = Auth.CreateUploadToken(mac, jsonParam);
            return uploadToken;
        }

        public async Task<string> Upload(string filePath, string fileName)
        {
            string accessKey = await SettingManager.GetSettingValueAsync(QiniuSettings.AccessKey);
            string secretKey = await SettingManager.GetSettingValueAsync(QiniuSettings.SecretKey);
            string bucket = await SettingManager.GetSettingValueAsync(QiniuSettings.Bucket);

            var policy = new PutPolicy();
            // 上传策略
            PutPolicy putPolicy = new PutPolicy();

            // 设置要上传的目标空间
            putPolicy.Scope = bucket;

            // 上传策略的过期时间(单位:秒)
            putPolicy.SetExpires(3600);

            //// 文件上传完毕后，在多少天后自动被删除
            //putPolicy.DeleteAfterDays = 1;

            // 生成上传凭证
            //参考地址:https://segmentfault.com/q/1010000008205978
            string jsonParam = putPolicy.ToJsonString();
            Mac mac = new Mac(accessKey, secretKey);
            string uploadToken = Auth.CreateUploadToken(mac, jsonParam);

            Config config = new Config();
            FormUploader upload = new FormUploader();
            HttpResult result = await upload.UploadFileAsync(filePath, fileName, uploadToken);

            return await GetPublicUrl(fileName);
        }

        public async Task<string> GetPublicUrl(string fileName)
        {
            string accessKey = await SettingManager.GetSettingValueAsync(QiniuSettings.AccessKey);
            string secretKey = await SettingManager.GetSettingValueAsync(QiniuSettings.SecretKey);
            string bucket = await SettingManager.GetSettingValueAsync(QiniuSettings.Bucket);
            string domain = await SettingManager.GetSettingValueAsync(QiniuSettings.Domain);
            string path = domain + "/" + fileName;
            // string publicUrl = DownloadManager.CreateSignedUrl(new Mac(accessKey, secretKey), path);
            return path;
        }
    }
}
