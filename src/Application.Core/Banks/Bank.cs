﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;

namespace Application.Banks
{
    public class Bank : FullAuditedEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Icon { get; set; }
    }
}
