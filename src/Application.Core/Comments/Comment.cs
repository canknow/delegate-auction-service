﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Application.Authorization.Users;
using Application.Orders;
using Application.Packages;
using Application.Teams;

namespace Application.Comments
{
    public class Comment:FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Content { get; set; }

        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }

        public int? PackageId { get; set; }

        public virtual Package Package { get; set; }

        public int? OrderId { get; set; }

        public virtual Order Order { get; set; }

        public int Rate { get; set; }

        public virtual User CreatorUser { get; set; }

        public string Avatar { get; set; }

        public string NickName { get; set; }
    }
}
