﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Application.Authorization.Users;
using Application.Orders;
using Application.Packages;
using Application.Teams;
using System;

namespace Application.AuctionAttempts
{
    public enum AuctionAttemptResult
    {
        Fail,
        Success,
        Lose
    }

    public class AuctionAttempt: FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual Order Order { get; set; }

        public int OrderId { get; set; }

        public long? UserId { get; set; }

        public virtual User User { get; set; }

        public AuctionAttemptResult Result { get; set; }

        public string Shot { get; set; }

        public DateTime AuctionDateTime { get; set; }

        public int? BidId { get; set; }

        public int? PackageId { get; set; }

        public virtual Package Package { get; set; }

        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }
    }
}
