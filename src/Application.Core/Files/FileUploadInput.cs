﻿using Microsoft.AspNetCore.Http;

namespace Application.Files
{
    public class FileUploadInput
    {
        public IFormFile File { get; set; }

        public int? Ratio { get; set; }

        public int? ShouldBeRatio { get; set; }

        public int? MaxFileSize { get; set; }

        public bool UseQiniu { get; set; } = false;
    }
}
