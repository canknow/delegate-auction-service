﻿using Abp.Dependency;
using Abp.Domain.Repositories;

namespace Application.Files
{
    public class FileManager: ISingletonDependency
    {
        public IRepository<AbpFileInfo> _repository { get; set; }

        public AbpFileInfo Create(AbpFileInfo fileInfo)
        {
            _repository.Insert(fileInfo);
            return fileInfo;
        }
    }
}
