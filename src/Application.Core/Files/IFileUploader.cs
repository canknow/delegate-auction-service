﻿using Abp.Dependency;
using System.Threading.Tasks;

namespace Application.Files
{
    public interface IFileUploader : ITransientDependency
    {
        Task<AbpFileInfo> Save(FileUploadInput input);
    }
}
