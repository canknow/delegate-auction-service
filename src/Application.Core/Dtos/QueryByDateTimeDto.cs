﻿using System;

namespace Application.Dtos
{
    public enum RankDateTimeType
    {
        DateTimeRange,
        DateTimeType
    }

    public enum DateTimeType
    {
        CurrentDay,
        CurrentWeek,
        CurrentMonth,
        CurrentYear
    }

    public class QueryByDateTimeDto
    {
        public RankDateTimeType RankDateTimeType { get; set; }

        public DateTimeType DateTimeType { get; set; }

        public DateTime? StartDateTime { get; set; }

        public DateTime? EndDateTime { get; set; }
    }
}
