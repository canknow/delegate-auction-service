﻿using Abp;
using Abp.Dependency;
using Abp.IO;
using Application.Web;

namespace Application
{
    public class AppFolderHelper : ISingletonDependency
    {
        public AppFolders AppFolders { get; set; }
        public PathHelper PathHelper { get; set; }

        public string GetUserResourcePath(UserIdentifier userIdentifier)
        {
            string userResourceFolderBase = AppFolders.UserResourceFolderBase;
            string userResourceFolder = userResourceFolderBase + userIdentifier.TenantId + "/" + userIdentifier.UserId;

            DirectoryHelper.CreateIfNotExists(PathHelper.GetAbsolutePath(userResourceFolder));
            return userResourceFolder;
        }
    }
}
