using Abp;
using Abp.Authorization.Users;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Notifications;
using Application.Authorization.Users;
using Application.BackgroundJob;
using Application.Notifications;
using Application.Wechat.Qrcodes;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Wechat
{
    public class WechatUserManager : ApplicationDomainServiceBase
    {
        public IRepository<UserLogin, long> UserLoginRepository { get; set; }
        public IRepository<Qrcode> QrcodeRepository { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        public CustomerServiceMessageHelper CustomerServiceMessageHelper { get; set; }
        public WechatCommonManager WeixinCommonManager { get; set; }
        public UserRegistrationManager UserRegistrationManager { get; set; }

        protected UserManager _userManager;
        protected IAppNotifier _appNotifier;
        protected INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IBackgroundJobManager _backgroundJobManager;

        public WechatUserManager(
            UserManager userManager,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IBackgroundJobManager backgroundJobManager)
        {
            _userManager = userManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _backgroundJobManager = backgroundJobManager;
        }

        [UnitOfWork]
        public async Task<User> GetUserFromOpenIdAsync(string openId)
        {
            UserLogin userLogin = await UserLoginRepository.FirstOrDefaultAsync(model => model.ProviderKey == openId);

            if (userLogin == null)
            {
                return null;
            }
            User user = UserRepository.Get(userLogin.UserId);
            return user;
        }

        [UnitOfWork]
        public string GetOpenid(UserIdentifier userIdentifier)
        {
            using (CurrentUnitOfWork.SetTenantId(userIdentifier.TenantId))
            {
                UserLogin weixinUserLogin = UserLoginRepository.GetAll().Where(model => model.UserId == userIdentifier.UserId
                && model.LoginProvider == "Weixin").FirstOrDefault();

                if (weixinUserLogin == null)
                {
                    return null;
                }
                return weixinUserLogin.ProviderKey;
            }
        }

        [UnitOfWork]
        public async Task<User> UpdateUserAsync(int tenantId, UserInfoJson userInfoJson)
        {
            User user = await GetUserFromOpenIdAsync(userInfoJson.openid);
            user.Avatar = userInfoJson.headimgurl;
            user.NickName = userInfoJson.nickname;
            UserRepository.Update(user);
            return user;
        }

        [UnitOfWork]
        public async Task<User> CreateUserWhenSubscribeAsync(int tenantId, UserInfoJson userInfoJson)
        {
            //Switch to the tenant
            CurrentUnitOfWork.SetTenantId(tenantId);

            var checkUserLogin = UserLoginRepository.FirstOrDefault(model => model.LoginProvider == "Weixin"
            && model.ProviderKey == userInfoJson.openid);

            if (checkUserLogin != null)
            {
                throw new Exception("ConcurrencyError");
            }
            string userName = userInfoJson.openid;

            var user = await UserRegistrationManager.RegisterAsync("Wexin",
                userInfoJson.openid,
                userInfoJson.headimgurl,
                _userManager.CreateDefaultUserEmail(userName),
                null,
                userName,
                User.DefaultPassword,
                false
                );
            user.UserType = UserType.User;
            user.Source = UserSource.WeixinInteraction;
            user.NickName = userInfoJson.nickname;
            user.Logins = new List<UserLogin>
            {
                new UserLogin
                {
                    TenantId = tenantId,
                    LoginProvider ="Weixin",
                    ProviderKey = userInfoJson.openid
                }
            };
            CurrentUnitOfWork.SaveChanges();
            return user;
        }

        [UnitOfWork]
        public Qrcode ProcessSceneId(int sceneId, User user)
        {
            Qrcode qrcode = QrcodeRepository.FirstOrDefault(model => model.SceneId == sceneId);
            if (user.ParentUserId.HasValue)
            {
                return qrcode;
            }
            if (qrcode != null)
            {
                BindParentJobArgs bindParentJobArgs = new BindParentJobArgs()
                {
                    TenantId = user.TenantId.Value,
                    SourceUserId = user.Id,
                    ParentUserId = qrcode.UserId,
                    BindParentType = BindParentType.ScanQrcode
                };
                UnitOfWorkManager.Current.Completed += (sender, args) =>
                {
                    _backgroundJobManager.Enqueue<BindParentJob, BindParentJobArgs>(bindParentJobArgs);
                };
            }
            return qrcode;
        }

        [UnitOfWork]
        public Qrcode ProcessSceneId(int sceneId, long userId)
        {
            User user = UserRepository.Get(userId);
            return ProcessSceneId(sceneId, user);
        }

        [UnitOfWork]
        public async Task RefreshWeixinUserInfo(long userId)
        {
            User user = UserRepository.Get(userId);
            string openid = GetOpenid(user.ToUserIdentifier());

            string accessToken = await WeixinCommonManager.GetAccessTokenAsync(user.TenantId.Value);
            UserInfoJson userInfoJson = UserApi.Info(accessToken, openid);
            user.Avatar = userInfoJson.headimgurl;
            user.NickName = userInfoJson.nickname;
            UserRepository.Update(user);
        }
    }
}
