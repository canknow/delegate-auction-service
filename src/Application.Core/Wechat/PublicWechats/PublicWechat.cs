﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;

namespace Application.Wechat.PublicWechats
{
    public class PublicWechat : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Qrcode { get; set; }

        public string Name { get; set; }

        public string Account { get; set; }

        public string Password { get; set; }

        public string Description { get; set; }

        public virtual General General { get; set; }

        public virtual Pay Pay { get; set; }

        public virtual CustomerService CustomerService { get; set; }

        public PublicWechat()
        {
            General = new General();
            Pay = new Pay();
            CustomerService = new CustomerService();
        }
    }

    [Owned]
    public class General
    {
        public string Token { get; set; }

        public string AppId { get; set; }

        public string Secret { get; set; }

        public string EncodingAESKey { get; set; }

        public string MPVerifyFile { get; set; }

        public string SubscribeLink { get; set; }
    }

    [Owned]
    public class Pay
    {
        public string MchId { get; set; }

        public string Key { get; set; }

        public string SslcertPath { get; set; }
    }

    [Owned]
    public class CustomerService
    {
        public string TriggerWord { get; set; }
    }
}
