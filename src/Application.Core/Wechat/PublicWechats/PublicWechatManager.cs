﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System.Linq;

namespace Application.Wechat.PublicWechats
{
    public class PublicWechatManager : ApplicationDomainServiceBase
    {
        public IRepository<PublicWechat> PublicWechatRepository { get; set; }

        [UnitOfWork]
        public PublicWechat GetPublicWechatByTenantId(int tenantId)
        {
            PublicWechat publicWechat = PublicWechatRepository.GetAll().Where(model => model.TenantId == tenantId).FirstOrDefault();
            if (publicWechat == null)
            {
                publicWechat = new PublicWechat()
                {
                    General=new General()
                    {
                        Token = "weixin"
                    }
                };
                PublicWechatRepository.Insert(publicWechat);
                CurrentUnitOfWork.SaveChanges();
            }
            return publicWechat;
        }
    }
}
