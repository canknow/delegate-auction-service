﻿using Abp.Configuration;
using System.Collections.Generic;
using System.Configuration;

namespace Application.Configuration
{
    public class WechatSettingProvider : ApplicationSettingProvider
    {
        public WechatSettingProvider(IAppConfigurationAccessor configurationAccessor) :base(configurationAccessor)
        {
        }

        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return GetSettings();

        }

        private IEnumerable<SettingDefinition> GetSettings()
        {
            return new[] {
                 new SettingDefinition(WechatSettings.General.Token,ConfigurationManager.AppSettings[WechatSettings.General.Token] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.General.AppId,ConfigurationManager.AppSettings[WechatSettings.General.AppId] ?? "",scopes: SettingScopes.Tenant, isVisibleToClients: true),
                new SettingDefinition(WechatSettings.General.Secret,ConfigurationManager.AppSettings[WechatSettings.General.Secret] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.General.MPVerifyFile,ConfigurationManager.AppSettings[WechatSettings.General.MPVerifyFile] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.General.EncodingAESKey,ConfigurationManager.AppSettings[WechatSettings.General.EncodingAESKey] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.General.SubscribeLink,ConfigurationManager.AppSettings[WechatSettings.General.SubscribeLink] ?? "",scopes: SettingScopes.Tenant),

                new SettingDefinition(WechatSettings.CustomerService.TriggerWord,ConfigurationManager.AppSettings[WechatSettings.CustomerService.TriggerWord] ?? "",scopes: SettingScopes.Tenant),

                new SettingDefinition(WechatSettings.Pay.MchId,ConfigurationManager.AppSettings[WechatSettings.Pay.MchId] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.Pay.Key,ConfigurationManager.AppSettings[WechatSettings.Pay.Key] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.Pay.SslcertPath,ConfigurationManager.AppSettings[WechatSettings.Pay.SslcertPath] ?? "",scopes: SettingScopes.Tenant),

                new SettingDefinition(WechatSettings.TemplateMessage.NewCustomer,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.NewCustomer] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.OrderCreated,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.OrderCreated] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.OrderPayed,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.OrderPayed] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.OrderShiped,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.OrderShiped] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.OrderRebate,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.OrderRebate] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.WalletWithdraw,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.WalletWithdraw] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.Upgrade,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.Upgrade] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.Audit,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.Audit] ?? "",scopes: SettingScopes.Tenant),
                new SettingDefinition(WechatSettings.TemplateMessage.ServiceProgress,ConfigurationManager.AppSettings[WechatSettings.TemplateMessage.ServiceProgress] ?? "",scopes: SettingScopes.Tenant),

                new SettingDefinition(WechatSettings.Open.AppId, GetFromAppSettings(WechatSettings.Open.AppId, ""), isVisibleToClients: true),
                new SettingDefinition(WechatSettings.Open.AppSecret, GetFromAppSettings(WechatSettings.Open.AppSecret, ""))};
        }
    }
}
