﻿using Application.Authorization.Users;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Senparc.Weixin.MP;

namespace Application.Wechat.Qrcodes
{
    public class Qrcode:FullAuditedEntity,IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int SceneId { get; set; }

        public string SceneStr { get; set; }

        public string Ticket { get; set; }

        public QrCode_ActionName Type { get; set; }

        public string Url { get; set; }

        public int ExpireSeconds { get; set; }

        public string Path { get; set; }

        public string Name { get; set; }

        public long UserId { get; set; }

        public virtual User User { get; set; }
    }
}
