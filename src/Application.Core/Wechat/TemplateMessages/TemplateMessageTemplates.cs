﻿namespace Application.Wechat.TemplateMessages
{
    public static class TemplateMessageTemplates
    {
        public const string NewCustomer = "OPENTM203447669";
        public const string OrderCreated = "TM00598";
        public const string OrderPayed = "OPENTM200444326";
        public const string OrderShiped = "OPENTM201541214";
        public const string OrderRebate = "OPENTM201050487";
        public const string WalletWithdraw = "OPENTM203192847";
        public const string Audit = "OPENTM416235956";
        public const string ServiceProgress = "OPENTM407196327";
    }
}
