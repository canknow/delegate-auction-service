﻿using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace Application.Wechat.TemplateMessages.TemplateMessageDatas
{
    public class OrderShipedTemplateMessageData : TemplateMessageData
    {
        public TemplateDataItem keyword1 { get; set; }
        public TemplateDataItem keyword2 { get; set; }
        public TemplateDataItem keyword3 { get; set; }
        public OrderShipedTemplateMessageData()
        {

        }

        public OrderShipedTemplateMessageData(
            TemplateDataItem first,
            TemplateDataItem keyword1,
            TemplateDataItem keyword2,
            TemplateDataItem keyword3,
            TemplateDataItem remark) : base(first, remark)
        {
            this.keyword1 = keyword1;
            this.keyword2 = keyword2;
            this.keyword3 = keyword3;
        }
    }
}
