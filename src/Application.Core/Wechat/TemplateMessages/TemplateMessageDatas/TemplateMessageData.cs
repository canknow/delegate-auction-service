﻿using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace Application.Wechat.TemplateMessages.TemplateMessageDatas
{
    public class TemplateMessageData
    {
        public TemplateDataItem first { get; set; }
        public TemplateDataItem remark { get; set; }

        public TemplateMessageData()
        {

        }

        public TemplateMessageData(TemplateDataItem first, TemplateDataItem remark)
        {
            this.first = first;
            this.remark = remark;
        }
    }
}
