﻿using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System;

namespace Application.Wechat.TemplateMessages.TemplateMessageDatas
{
    public class WexinExceptionTemplateMessageData : TemplateMessageData
    {
        /// <summary>
        /// Time
        /// </summary>
        public TemplateDataItem keyword1 { get; set; }
        /// <summary>
        /// Host
        /// </summary>
        public TemplateDataItem keyword2 { get; set; }
        /// <summary>
        /// Service
        /// </summary>
        public TemplateDataItem keyword3 { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public TemplateDataItem keyword4 { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public TemplateDataItem keyword5 { get; set; }


        public WexinExceptionTemplateMessageData(TemplateDataItem first, TemplateDataItem host, TemplateDataItem service, TemplateDataItem status, TemplateDataItem message,
            TemplateDataItem remark, TemplateDataItem url = null)
            : base(first, remark)
        {
            keyword1 = new TemplateDataItem(DateTime.Now.ToString());
            keyword2 = host;
            keyword3 = service;
            keyword4 = status;
            keyword5 = message;
        }
    }
}
