﻿using Application.Configuration;
using Application.Wechat.TemplateMessages.TemplateMessageDatas;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System.Threading.Tasks;

namespace Application.Wechat.TemplateMessages
{
    public class TemplateMessageManager : ApplicationDomainServiceBase
    {
        public WechatCommonManager WechatCommonManager { get; set; }

        public async Task<SendTemplateMessageResult> SendTemplateMessageAsync<T>(string accessToken, string openId, string templateId, string url, T data)
            where T : TemplateMessageData
        {
            try
            {
                return await TemplateApi.SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            }
            catch
            {
                return null;
            }
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfNewCustomerAsync(int tenantId, string openId, string url, NewCustomerTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.NewCustomer, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfOrderCreatedAsync(int tenantId, string openId, string url, OrderCreatedTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderCreated, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfOrderPayedAsync(int tenantId, string openId, string url, OrderPayedTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderPayed, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfOrderShipedAsync(int tenantId, string openId, string url, OrderShipedTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderShiped, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfOrderRebateAsync(int tenantId, string openId, string url, OrderRebateTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderRebate, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfWalletWithdrawAsync(int tenantId, string openId, string url, WalletWithdrawTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.WalletWithdraw, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfUpgradeAsync(int tenantId, string openId, string url, UpgradeTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.Upgrade, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfAuditAsync(int tenantId, string openId, string url, AuditTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.Audit, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task<SendTemplateMessageResult> SendTemplateMessageOfServiceProgressAsync(int tenantId, string openId, string url, ServiceProgressTemplateMessageData data)
        {
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);
            string templateId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.Audit, tenantId);
            SendTemplateMessageResult sendResult = await SendTemplateMessageAsync(accessToken, openId, templateId, url, data);
            return sendResult;
        }

        public async Task Install(int tenantId)
        {
            AddtemplateJsonResult addtemplateJsonResult;
            string accessToken = await WechatCommonManager.GetAccessTokenAsync(tenantId);

            GetPrivateTemplateJsonResult result = await TemplateApi.GetPrivateTemplateAsync(accessToken);
            foreach (var item in result.template_list)
            {
                await TemplateApi.DelPrivateTemplateAsync(accessToken, item.template_id);
            }
            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.NewCustomer);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.NewCustomer, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.OrderCreated);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.OrderCreated, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.OrderPayed);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.OrderPayed, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.OrderShiped);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.OrderShiped, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.OrderRebate);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.OrderRebate, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.WalletWithdraw);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.WalletWithdraw, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.Audit);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.Audit, addtemplateJsonResult.template_id);

            addtemplateJsonResult = await TemplateApi.AddtemplateAsync(accessToken, TemplateMessageTemplates.ServiceProgress);
            await UpdateTemplate(tenantId, WechatSettings.TemplateMessage.ServiceProgress, addtemplateJsonResult.template_id);
        }

        public async Task UpdateTemplate(int tenantId, string name, string template_id)
        {
            await SettingManager.ChangeSettingForTenantAsync(tenantId, name, template_id);
        }
    }
}
