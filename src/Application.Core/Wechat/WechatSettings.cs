﻿namespace Application.Configuration
{
    public static class WechatSettings
    {
        public static class General
        {
            public const string Token = "Weixin.General.Token";
            public const string AppId = "Weixin.General.AppId";
            public const string Secret = "Weixin.General.Secret";
            public const string EncodingAESKey = "Weixin.General.EncodingAESKey";
            public const string MPVerifyFile = "Weixin.General.MPVerifyFile";
            public const string SubscribeLink = "Weixin.General.SubscribeLink";
            public const string Qrcode = "Weixin.General.Qrcode";
            public const string Name = "Weixin.General.Name";
            public const string Description = "Weixin.General.Description";
        }

        public static class Pay
        {
            public const string MchId = "Weixin.Pay.MchId";
            public const string Key = "Weixin.Pay.Key";
            public const string SslcertPath = "Weixin.Pay.SslcertPath";
        }

        public static class CustomerService
        {
            public const string TriggerWord = "Weixin.CustomerService.TriggerWord";
        }

        public static class Open
        {
            public const string AppId = "Weixin.Open.AppId";
            public const string AppSecret = "Weixin.Open.AppSecret";
        }

        public static class TemplateMessage
        {
            public const string NewCustomer = "Weixin.TemplateMessage.NewCustomer";
            public const string OrderCreated = "Weixin.TemplateMessage.OrderCreated";
            public const string OrderPayed = "Weixin.TemplateMessage.OrderPayed";
            public const string OrderShiped = "Weixin.TemplateMessage.OrderShiped";
            public const string OrderRebate = "Weixin.TemplateMessage.OrderRebate";
            public const string WalletWithdraw = "Weixin.TemplateMessage.WalletWithdraw";
            public const string Upgrade = "Weixin.TemplateMessage.Upgrade";
            public const string Audit = "Weixin.TemplateMessage.Audit";
            public const string ServiceProgress = "Weixin.TemplateMessage.ServiceProgress";
        }
    }
}
