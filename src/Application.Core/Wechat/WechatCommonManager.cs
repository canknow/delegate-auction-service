﻿using Abp.Configuration;
using Application.Configuration;
using Application.MultiTenancy;
using Application.Web;
using Senparc.Weixin.MP.Containers;
using Senparc.Weixin.TenPay.V3;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Application.Wechat
{
    public class WechatCommonManager : ApplicationDomainServiceBase
    {
        private string _accessToken;
        public TenantHelper TenantHelper { get; set; }
        public PathHelper PathHelper { get; set; }

        public async Task<TenPayV3Info> GetTenPayV3InfoAsync(int tenantId, string tenPayV3Notify)
        {
            string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, tenantId);
            string appSecret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, tenantId);
            string mchId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.MchId, tenantId);
            string key = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.Key, tenantId);
            TenPayV3Info tenPayV3Info = new TenPayV3Info(appId, appSecret, mchId, key, tenPayV3Notify, tenPayV3Notify);
            return tenPayV3Info;
        }

        public string GetAccessToken()
        {
            if (_accessToken != null)
            {
                return _accessToken;
            }
            Tenant tenant = TenantHelper.GetTenant();
            string appId = SettingManager.GetSettingValueForTenant(WechatSettings.General.AppId, tenant.Id);
            string secret = SettingManager.GetSettingValueForTenant(WechatSettings.General.Secret, tenant.Id);
            _accessToken = AccessTokenContainer.TryGetAccessToken(appId, secret);
            return _accessToken;
        }

        public async Task<string> RefreshAccessToken()
        {
            Tenant tenant = TenantHelper.GetTenant();
            string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, tenant.Id);
            string secret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, tenant.Id);
            return AccessTokenContainer.TryGetAccessToken(appId, secret, true);
        }

        public async Task<string> GetAccessTokenAsync(int tenantId)
        {
            if (_accessToken != null)
            {
                return _accessToken;
            }
            string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, tenantId);
            string secret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, tenantId);
            _accessToken = await AccessTokenContainer.TryGetAccessTokenAsync(appId, secret);
            return _accessToken;
        }

        public async Task InstallCertificate(string password)
        {
            var cert = PathHelper.GetAbsolutePath(await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.SslcertPath, AbpSession.TenantId.Value));

            //添加个人证书
            X509Certificate2 certificate = new X509Certificate2(cert, password, X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);

            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            //X509Store store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadWrite);
            store.Remove(certificate);   //可省略
            store.Add(certificate);

            store.Close();
        }
    }
}
