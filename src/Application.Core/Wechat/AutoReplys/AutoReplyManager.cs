﻿using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Linq;
namespace Application.Wechat.AutoReplys
{
    public class AutoReplyManager : ApplicationDomainServiceBase
    {
        public IRepository<AutoReply> AutoReplyRepository { get; set; }

        public List<AutoReply> GetAutoReplysOfRequestMsgType(RequestType requestType, string key = null)
        {
            return AutoReplyRepository.GetAll()
                .Where(model => model.RequestType == requestType)
                .WhereIf(key != null, model => (model.Key == null) || (model.MatchingType == MatchingType.Exact && model.Key == key) || (model.MatchingType == MatchingType.Fuzzy && key.Contains(model.Key)))
                .OrderBy(model => model.Sort).ToList();
        }
    }
}
