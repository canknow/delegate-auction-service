﻿using Application.Dtos;
using Abp.Domain.Entities.Auditing;
using Abp.Linq.Extensions;
using System;
using System.Linq;

namespace Application.Entities.Extensions
{
    public static class DateTimeQueryExtension
    {
        public static IQueryable<T> GetQueryByDateTime<T>(this IQueryable<T> query, QueryByDateTimeDto input) where T : FullAuditedEntity
        {
            if (input.RankDateTimeType == RankDateTimeType.DateTimeType)
            {
                if (input.DateTimeType == DateTimeType.CurrentDay)
                {
                    DateTime today = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                    DateTime nextDay = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"));

                    query = query.Where(model => model.CreationTime > today && model.CreationTime < nextDay);
                }
                if (input.DateTimeType == DateTimeType.CurrentWeek)
                {
                    DateTime week = DateTime.Now.AddDays(-Convert.ToInt32(DateTime.Now.Date.DayOfWeek));
                    query = query.Where(model => model.CreationTime > week);
                }
                if (input.DateTimeType == DateTimeType.CurrentMonth)
                {
                    DateTime month = DateTime.Now.AddDays(-Convert.ToInt32(DateTime.Now.Date.Day));
                    query = query.Where(model => model.CreationTime > month);
                }
                if (input.DateTimeType == DateTimeType.CurrentYear)
                {
                    query = query.Where(model => model.CreationTime.Year == DateTime.Now.Year);
                }
            }
            else if (input.RankDateTimeType == RankDateTimeType.DateTimeRange)
            {
                query = query.WhereIf(input.StartDateTime.HasValue, model => model.CreationTime >= input.StartDateTime.Value)
                    .WhereIf(input.EndDateTime.HasValue, model => model.CreationTime <= input.EndDateTime.Value);
            }
            return query;
        }
    }
}
