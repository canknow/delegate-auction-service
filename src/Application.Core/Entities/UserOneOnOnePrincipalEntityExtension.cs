﻿using Abp;

namespace Application.Entities
{
    public static class UserOneOnOnePrincipalEntityExtension
    {
        public static UserIdentifier GetUserIdentifier(this IUserOneOnOnePrincipalEntity entity)
        {
            return new UserIdentifier(entity.TenantId, entity.Id);
        }
    }
}
