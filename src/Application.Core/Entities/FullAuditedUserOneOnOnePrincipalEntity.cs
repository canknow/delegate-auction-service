﻿using Abp;
using Abp.Domain.Entities.Auditing;

namespace Application.Entities
{
    public class FullAuditedUserOneOnOnePrincipalEntity : FullAuditedEntity<long>, IUserOneOnOnePrincipalEntity
    {
        public int TenantId { get; set; }

        public FullAuditedUserOneOnOnePrincipalEntity()
        {

        }

        public FullAuditedUserOneOnOnePrincipalEntity(UserIdentifier userIdentifier)
        {
            TenantId = userIdentifier.TenantId.Value;
            Id = userIdentifier.UserId;
        }
    }
}
