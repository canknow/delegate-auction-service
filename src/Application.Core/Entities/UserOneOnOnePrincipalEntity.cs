﻿using Abp.Domain.Entities;

namespace Application.Entities
{
    public class UserOneOnOnePrincipalEntity :Entity<long>, IUserOneOnOnePrincipalEntity
    {
        public int TenantId { get; set; }
    }
}
