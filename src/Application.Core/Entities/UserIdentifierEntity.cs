﻿using Abp;
using Abp.Domain.Entities;

namespace Application.Entities
{
    public class UserIdentifierEntity: UserIdentifierEntity<int>
    {
        public UserIdentifierEntity(UserIdentifier userIdentifier): base(userIdentifier)
        {

        }

        public UserIdentifierEntity(): base()
        {
        }
    }

    public class UserIdentifierEntity<TPrimaryKey>: Entity<TPrimaryKey>, IUserIdentifierEntity
    {
        public int TenantId { get; set; }

        public long UserId { get; set; }

        public UserIdentifierEntity()
        {

        }

        public UserIdentifierEntity(UserIdentifier userIdentifier)
        {
            TenantId = userIdentifier.TenantId.Value;
            UserId = userIdentifier.UserId;
        }

        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(TenantId,UserId);
        }
    }
}
