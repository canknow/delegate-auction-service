﻿using Abp.Domain.Entities;

namespace Application.Entities
{
    public interface IUserOneOnOnePrincipalEntity : IEntity<long>, IMustHaveTenant
    {
    }
}
