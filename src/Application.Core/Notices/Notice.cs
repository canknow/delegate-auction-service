﻿using Abp.Domain.Entities.Auditing;

namespace Application.Notices
{
    public enum NoticeStatus
    {
        Off,
        On
    }

    public class Notice: FullAuditedEntity
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public NoticeStatus Status { get; set; }
    }
}
