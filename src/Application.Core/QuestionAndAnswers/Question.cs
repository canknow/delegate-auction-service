﻿using Abp.Domain.Entities.Auditing;

namespace Application.QuestionAndAnswers
{
    public class QuestionAndAnswer: FullAuditedEntity
    {
        public string Question { get; set; }

        public string Answer { get; set; }
    }
}
