﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Sms
{
    public class AliyunSmsResult
    {
        public string BizId { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public string RequestId { get; set; }
    }
}
