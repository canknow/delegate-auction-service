﻿using Abp.Net.Sms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Abp;
using Abp.UI;

namespace Application.Sms
{
    public class AliyunSmsSender : SmsSenderBase
    {
        public AliyunSmsSender(ISmsSenderConfiguration configuration) : base(configuration)
        {
        }

        protected override void SendSms(SmsMessage sms)
        {
            // Create a client used for initiating a request
            IClientProfile profile = DefaultProfile.GetProfile(
            "default",
            _configuration.GetAppKey(),
            _configuration.GetAppSecret());
            DefaultAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.Method = MethodType.POST;
            request.Domain = "dysmsapi.aliyuncs.com";
            request.Version = "2017-05-25";
            request.Action = "SendSms";

            request.AddQueryParameters("PhoneNumbers", sms.To);
            request.AddQueryParameters("SignName", sms.FreeSignName);
            request.AddQueryParameters("TemplateCode", sms.TemplateCode);
            request.AddQueryParameters("TemplateParam", sms.TemplateParams);

            CommonResponse response = client.GetCommonResponse(request);
            string resultJson = System.Text.Encoding.Default.GetString(response.HttpResponse.Content);
            AliyunSmsResult result = JsonConvert.DeserializeObject<AliyunSmsResult>(resultJson);
            if (result.Code!= "OK")
            {
                throw new UserFriendlyException(result.Message);
            }
        }


        protected override Task SendSmsAsync(SmsMessage sms)
        {
            SendSms(sms);
            return Task.FromResult(0);
        }
    }
}
