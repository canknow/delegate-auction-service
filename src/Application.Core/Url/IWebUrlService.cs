﻿using System.Collections.Generic;

namespace Application.Url
{
    public interface IWebUrlService
    {
        string WebSiteRootAddressFormat { get; }

        bool SupportsTenancyNameInUrl { get; }

        string GetWebSiteRootAddress(string tenancyName = null);

        List<string> GetRedirectAllowedExternalWebSites();
    }
}
