﻿using System;

namespace Application.Orders.NumberProviders
{
    public class DefaultNumberProvider : INumberProvider
    {
        public static object _lock = new object();
        public static int count = 1;
        public string BuildNumber()
        {
            lock (_lock)
            {
                Random random = new Random();
                return "T" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + random.Next(1000, 9999).ToString();
            }
        }
    }
}
