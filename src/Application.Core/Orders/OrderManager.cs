﻿using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Application.AuctionAttempts;
using Application.AuctionRecords;
using Application.Authorization;
using Application.Bids;
using Application.Comments;
using Application.Notifications;
using Application.Orders.Events;
using Application.Orders.Exceptions;
using Application.Orders.NumberProviders;
using Application.Packages;
using Application.Pays.PayProviders;
using Application.Shops;
using Application.Teams;
using Application.Wallets;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Orders
{
    public class OrderManager:ApplicationDomainServiceBase
    {
        public IRepository<Order> OrderRepository { get; set; }
        public IRepository<Package> PackageRepository { get; set; }
        public IRepository<Team> TeamRepository { get; set; }
        public IRepository<Comment> CommentRepository { get; set; }
        public IRepository<CompensationRule> CompensationRuleRepository { get; set; }
        public INumberProvider NumberProvider { get; set; }
        public IIocResolver IocResolver { get; set; }
        public IRepository<Package> PackageRespository { get; set; }
        public ApplicationAuthorizationHelper ApplicationAuthorizationHelper { get; set; }
        public IAppNotifier AppNotifier { get; set; }
        public ShopManager ShopManager { get; set; }

        public async Task<Order> CreateOrderAsync(int? teamId , Package package, int compensationRuleId, Bid bid, int auctionNumber)
        {
            if (teamId.HasValue)
            {
                Team team = TeamRepository.Get(teamId.Value);
                team.DealCount++;
                TeamRepository.Update(team);
            }
            
            Order order = new Order()
            {
                Title= package.Name,
                UserId =AbpSession.UserId.Value,
                User = ApplicationAuthorizationHelper.CurrentUser,
                Number = NumberProvider.BuildNumber(),
                TeamId = teamId,
                PackageId = package.Id,
                CompensationRuleId = compensationRuleId,
                Money=package.Price,
                BidId = bid.Id,
                AuctionNumber = auctionNumber
            };
            order.Contract = CreateContract(package, bid, compensationRuleId);
            OrderRepository.Insert(order);

            package.OrderCount++;
            PackageRespository.Update(package);
            CurrentUnitOfWork.SaveChanges();

            await AppNotifier.OrderCreatedAsync(order);

            await EventBus.TriggerAsync(new OrderCreatedEventData(order));

            return order;
        }

        public string CreateContract(Package package, Bid bid, int compensationRuleId)
        {
            var shop = ShopManager.GetShop();

            string contract = shop.ContractTemplate;
            if (string.IsNullOrEmpty(contract))
            {
                return "";
            }
            var compensationRule = CompensationRuleRepository.FirstOrDefault(compensationRuleId);
            contract = contract.Replace("{FullName}", bid.FullName);
            contract = contract.Replace("{IDNumber}", bid.IDNumber);
            contract = contract.Replace("{PhoneNumber}", bid.PhoneNumber);
            contract = contract.Replace("{Account}", bid.Account);
            contract = contract.Replace("{ShopCompany}", shop.CompanyName);
            contract = contract.Replace("{ShopAddress}", shop.Address);
            contract = contract.Replace("{ShopTelePhone}", shop.TelePhone);
            contract = contract.Replace("{Price}", package.Price.ToString("f2"));
            contract = contract.Replace("{AttemptCount}", compensationRule.Count.ToString());
            contract = contract.Replace("{CompensationMoney}", compensationRule.Money.ToString());
            contract = contract.Replace("{Sign}", bid.FullName);
            contract = contract.Replace("{DateTime}", DateTime.Now.ToString("yyyy-MM-dd"));
            return contract;
        }

        public async Task<Order> Pass(Order order)
        {
            order.OrderStatus = OrderStatus.Aucting;
            await OrderRepository.UpdateAsync(order);

            await AppNotifier.PassOrderAsync(order);

            await EventBus.TriggerAsync(new OrderPassedEventData(order));

            return order;
        }

        public async Task<Order> Deny(Order order, string reason)
        {
            order.DenyReason = reason;
            order.OrderStatus = OrderStatus.AuditDenyed;
            await OrderRepository.UpdateAsync(order);

            await AppNotifier.DenyOrderAsync(order);

            return order;
        }

        [UnitOfWork]
        public Order ResetRefundNumber(Order order)
        {
            order.RefundNumber = NumberProvider.BuildNumber();
            OrderRepository.Update(order);
            return order;
        }

        [UnitOfWork]
        public async Task<Order> PayCallback(string orderNumber, PayType payType, string transactionId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MustHaveTenant, AbpDataFilters.MayHaveTenant))
            {
                Order order = OrderRepository.GetAll().Where(model => model.Number == orderNumber).FirstOrDefault();
                await PayCallback(order, payType, transactionId);
                return order;
            }
        }

        [UnitOfWork]
        public async Task<Order> Attempt(Order order)
        {
            if (order.GetRemainAuctionCount() <= 0)
            {
                throw new UserFriendlyException(L("NoMoreAuctionChance"));
            }
            order.AttemptCount++;
            await OrderRepository.UpdateAsync(order);
            return order;
        }

        [UnitOfWork]
        public async Task<Order> PayCallback(Order order, PayType payType, string transactionId)
        {
            await PayedAsync(order, payType, transactionId);
            return order;
        }

        [UnitOfWork]
        public async Task PayedAsync(Order order, PayType payType, string transactionId)
        {
            if (order.PaymentStatus == PaymentStatus.Payed)
            {
                throw new OrderPayedException(L("OrderHasPayed"));
            }
            order.PayType = payType;
            order.PaymentStatus = PaymentStatus.Payed;
            order.PaymentDatetime = DateTime.Now;
            order.OutTradeNo = transactionId;
            order.OrderStatus = OrderStatus.WaitingForComment;

            await OrderRepository.UpdateAsync(order);
            await CurrentUnitOfWork.SaveChangesAsync();
            await AppNotifier.OrderPayedAsync(order);
            await EventBus.TriggerAsync(new OrderPayedEventData(order));
        }

        [UnitOfWork]
        public async Task CompensateAsync(Order order)
        {
            order.OrderStatus = OrderStatus.Completed;
            order.CompensationStatus = CompensationStatus.Compensated;

            await OrderRepository.UpdateAsync(order);
            await CurrentUnitOfWork.SaveChangesAsync();

            await AppNotifier.OrderCompensatedAsync(order);
        }

        [UnitOfWork]
        public async Task RefundAsync(Order order, decimal refundFee)
        {
            order.OrderStatus = OrderStatus.Refunded;
            order.RefundStatus = RefundStatus.Refunded;
            OrderRepository.Update(order);

            try
            {
                switch (order.PayType)
                {
                    case PayType.WeChat:
                        IPayProvider payProvider = IocResolver.Resolve<WeixinPayProvider>();
                        await payProvider.Refund(order, refundFee);
                        break;
                }
                await EventBus.TriggerAsync(new OrderRefundEventData(order));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        public async Task CompleteAuction(int orderId, AuctionAttempt auctionAttempt, int attemptCount, string shot)
        {
            var order = await OrderRepository.GetAsync(orderId);
            await CompleteAuction(order, attemptCount, auctionAttempt.AuctionDateTime, shot);
        }

        public async Task LoseAuction(Order order)
        {
            await AppNotifier.LoseAuction(order);

            if (order.GetRemainAuctionCount()<=0)
            {
                await AppNotifier.FailAuction(order);
            }
        }


        public async Task FailAuction(Order order)
        {
            // 等待赔付
            order.OrderStatus = OrderStatus.WaitingForCompensation;
            await OrderRepository.UpdateAsync(order);
            CurrentUnitOfWork.SaveChanges();

            await AppNotifier.FailAuction(order);
        }

        public async Task CompleteAuction(Order order, int attemptCount, DateTime winDateTime, string shot)
        {
            AuctionRecord auctionRecord = new AuctionRecord()
            {
                AttemptCount = attemptCount,
                WinDateTime = winDateTime,
                BidId = order.BidId,
                TeamId = order.TeamId,
                Shot = shot,
                PackageId = order.PackageId,
                UserId = order.UserId,
                Bid = new AuctionRecord.AuctionRecordBid()
                {
                    FullName = order.Bid.FullName,
                    PhoneNumber = order.Bid.PhoneNumber,
                    IDNumber = order.Bid.IDNumber,
                    Account = order.Bid.Account,
                    Password = order.Bid.Password
                }
            };
            order.AuctionRecord = auctionRecord;
            order.OrderStatus = OrderStatus.WaitingForPayment;
            order.AuctionResult = AuctionResult.Success;
            await OrderRepository.UpdateAsync(order);
            CurrentUnitOfWork.SaveChanges();
        }

        public async Task<Comment> Comment(Order order, string content, int rate)
        {
            order.OrderStatus = OrderStatus.Completed;
            var user = ApplicationAuthorizationHelper.CurrentUser;
            Comment comment = new Comment()
            {
                Content = content,
                TeamId = order.TeamId,
                PackageId = order.PackageId,
                OrderId = order.Id,
                Rate = rate,
                Avatar = user.Avatar,
                NickName = user.NickName,
            };
            await CommentRepository.InsertAsync(comment);
            return comment;
        }
    }
}
