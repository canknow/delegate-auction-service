﻿using Abp.Dependency;
using Application.MultiTenancy;

namespace Application.Orders
{
    public static class OrderHelper
    {
        public static string GetOrderDetailUrl(int orderId)
        {
            TenantHelper TenantHelper = IocManager.Instance.Resolve<TenantHelper>();
            return TenantHelper.GetTenantBaseUrl() + "app/mobile/#/order/detail?id=" + orderId;
        }
    }
}
