using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Application.AuctionAttempts;
using Application.AuctionRecords;
using Application.Authorization.Users;
using Application.Bids;
using Application.Comments;
using Application.Distributions;
using Application.Entities;
using Application.Packages;
using Application.Teams;
using Application.Wallets;
using System;
using System.Collections.Generic;

namespace Application.Orders
{
    public enum OrderStatus
    {
        WaitingForAudit,
        Aucting,
        WaitingForPayment,
        WaitingForCompensation,
        WaitingForComment,
        Completed,
        Refunded,
        AuditDenyed,
        Close,
    }

    public enum CompensationStatus
    {
        UnCompensate,
        Compensated
    }

    public enum RefundStatus
    {
        ToRefund,
        Refunding,
        Refunded,
        Failed
    }

    public enum PaymentStatus
    {
        ToPay,
        Payed
    }

    public enum ShipStatus
    {
        UnShip,
        Shipping,
        Received
    }

    public enum AuctionResult
    {
        UnAuction,
        Success,
        Fail
    }

    public class Order : FullAuditedAggregateRoot, IUserIdentifierEntity, IMustHaveTenant
    {
        public long UserId { get; set; }

        public virtual User User { get; set; }

        public int TenantId { get; set; }

        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }

        public int? PackageId { get; set; }

        public virtual Package Package { get; set; }

        public int? CompensationRuleId { get; set; }

        public virtual CompensationRule CompensationRule { get; set; }

        public string Title { get; set; }

        public decimal Money { get; set; }

        public string Number { get; set; }

        public int AttemptCount { get; set; }

        public int GetRemainAuctionCount()
        {
            return AuctionNumber - AttemptCount;
        }

        public AuctionResult AuctionResult { get; set; }

        public CompensationStatus CompensationStatus { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public string GetOrderStatusText()
        {
            return OrderStatus.ToString();
        }

        public PaymentMethod PaymentMethod { get; set; }

        public PaymentStatus PaymentStatus { get; set; }

        public RefundStatus RefundStatus { get; set; }

        public string OutTradeNo { get; set; }

        public string PrepayId { get; set; }

        public PayType PayType { get; set; }

        public DateTime? PaymentDatetime { get; set; }

        public string RefundNumber { get; set; }

        public ShipStatus ShipStatus { get; set; }

        public virtual Bid Bid { get; set; }

        public int BidId { get; set; }

        public string AuditRemark { get; set; }

        public string DenyReason { get; set; }

        public virtual AuctionRecord AuctionRecord { get; set; }

        public virtual ICollection<AuctionAttempt> AuctionAttempts { get; set; }

        public string Contract { get; set; }

        public virtual Comment Comment{get;set;}

        public virtual ICollection<OrderDistribution> OrderDistributions { get; set; }

        public int AuctionNumber { get; set; }

        public string AdminRemark { get; set; }
    }

    public enum PaymentMethod{

    }
}