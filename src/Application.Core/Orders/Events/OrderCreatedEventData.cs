﻿using System;

namespace Application.Orders.Events
{
    [Serializable]
    public class OrderCreatedEventData : OrderEventData
    {
        public OrderCreatedEventData(Order order) :base(order)
        {;
        }
    }
}
