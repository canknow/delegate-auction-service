﻿using System;

namespace Application.Orders.Events
{
    [Serializable]
    public class OrderClosedEventData : OrderEventData
    {
        public OrderClosedEventData(Order order) :base(order)
        {
        }
    }
}
