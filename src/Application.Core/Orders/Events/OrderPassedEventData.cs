﻿using System;

namespace Application.Orders.Events
{
    [Serializable]
    public class OrderPassedEventData : OrderEventData
    {
        public OrderPassedEventData(Order order) :base(order)
        {;
        }
    }
}
