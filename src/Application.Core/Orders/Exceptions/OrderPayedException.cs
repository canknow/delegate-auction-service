﻿using Abp;

namespace Application.Orders.Exceptions
{
    public class OrderPayedException: AbpException
    {
        public OrderPayedException(string message):base(message)
        {

        }
    }
}
