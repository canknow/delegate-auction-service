using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Events.Bus.Handlers;
using Application.BackgroundJob;
using Application.BusinessNotifiers;
using Application.Configuration;
using Application.Distributions;
using Application.Notifications;
using Application.Orders.Events;
using System;
using System.Threading.Tasks;

namespace Application.Orders.EventHandlers
{
    public class OrderCreatedEventHandler : ApplicationDomainServiceBase, IAsyncEventHandler<OrderCreatedEventData>, ISingletonDependency
    {
        public RebateManage RebateManage { get; set; }
        public IBackgroundJobManager BackgroundJobManager { get; set; }
        public BusinessNotiferManager BusinessNotiferManager { get; set; }
        public IAppNotifier AppNotifier { get; set; }

        [UnitOfWork]
        public virtual async Task HandleEventAsync(OrderCreatedEventData eventData)
        {
            if ((DistributionWhen)Enum.Parse(typeof(DistributionWhen), SettingManager.GetSettingValue(AppSettings.Order.DistributionWhen)) == DistributionWhen.Create)
            {
                CurrentUnitOfWork.Completed += (sender, args) =>
                {
                    BackgroundJobManager.Enqueue<OrderPayedRebateJob, OrderPayedJobArgs>(new OrderPayedJobArgs()
                    {
                        TenantId = eventData.Order.TenantId,
                        OrderId = eventData.Order.Id
                    });
                };
            }

            foreach (BusinessNotifier businessNotifier in BusinessNotiferManager.GetBusinessNotifiers())
            {
                await AppNotifier.OrderCreatedForBusinessAsync(eventData.Order, businessNotifier);
            }
        }
    }
}
