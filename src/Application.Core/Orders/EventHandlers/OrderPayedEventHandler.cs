﻿using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Events.Bus.Handlers;
using Application.BackgroundJob;
using Application.Configuration;
using Application.Distributions;
using Application.Orders.Events;
using System;
using System.Threading.Tasks;

namespace Application.Orders.EventHandlers
{
    public class OrderPayedEventHandler : ApplicationDomainServiceBase, IAsyncEventHandler<OrderPayedEventData>, ITransientDependency
    {
        public RebateManage RebateManage { get; set; }
        public IBackgroundJobManager BackgroundJobManager { get; set; }

        [UnitOfWork]
        public virtual async Task HandleEventAsync(OrderPayedEventData eventData)
        {
            if ((DistributionWhen)Enum.Parse(typeof(DistributionWhen), SettingManager.GetSettingValue(AppSettings.Order.DistributionWhen)) == DistributionWhen.Payed)
            {
                CurrentUnitOfWork.Completed += (sender, args) =>
                {
                    BackgroundJobManager.Enqueue<OrderPayedRebateJob, OrderPayedJobArgs>(new OrderPayedJobArgs()
                    {
                        TenantId = eventData.Order.TenantId,
                        OrderId = eventData.Order.Id
                    });
                };
            };
        }
    }
}
