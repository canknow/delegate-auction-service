using System.Threading.Tasks;

namespace Application.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}