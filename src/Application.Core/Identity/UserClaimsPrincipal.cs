﻿using Application.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Application.Identity
{
    public class UserClaimsPrincipal : IUserClaimsPrincipalFactory<User>
    {
        private readonly UserStore _storeService;
        public UserClaimsPrincipal(UserStore storeService)
        {
            _storeService = storeService;
        }
        public async Task<ClaimsPrincipal> CreateAsync(User user)
        {
            var claims = await _storeService.GetClaimsAsync(user);
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);
            ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
            return await Task.FromResult(claimsPrincipal);

        }
    }
}
