﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace Application.Shops
{
    public class Shop : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string TelePhone { get;set; }

        public string Seal { get; set; }

        public string AboutUs { get; set; }

        public string WechatQrcode { get; set; }

        public string ShareTitle { get; set; }

        public string ShareDescription { get; set; }

        public string SharePicture { get; set; }

        public string Logo { get; set; }

        public string CopyrightLogo { get; set; }

        public string AgreeContent { get; set; }

        public bool EnableAgree { get; set; }

        public string ContractTemplate { get; set; }

        public string Notice { get; set; }

        public string UploadBidNotice { get; set; }

        public string OfflinePayNote { get; set; }
    }
}
