﻿using Application.MultiTenancy;
using Abp.Domain.Repositories;
using System;
using System.Linq;

namespace Application.Shops
{
    public class ShopManager : ApplicationDomainServiceBase
    {
        public IRepository<Shop> ShopRepository { get; set; }
        public TenantHelper TenantHelper { get; set; }

        public Shop GetShop()
        {
            CurrentUnitOfWork.SetTenantId(TenantHelper.GetTenant()?.Id);
            Shop shop = ShopRepository.GetAll().FirstOrDefault();
            
            if (shop == null)
            {
                shop = new Shop();
                ShopRepository.Insert(shop);
            }
            return shop;
        }
    }
}
