﻿namespace Application
{
    public class ApplicationConsts
    {
        public const string LocalizationSourceName = "Application";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const int PaymentCacheDurationInMinutes = 30;

        /// <summary>
        /// Default page size for paged requests.
        /// </summary>
        public const int DefaultPageSize = 10;

        /// <summary>
        /// Maximum allowed page size for paged requests.
        /// </summary>
        public const int MaxPageSize = 1000;

        public const int MaxUrlLength = 255;
    }
}
