﻿using System.Collections.Generic;
using System.Linq;
using Abp.Extensions;
using Microsoft.Extensions.Configuration;
using Application.Configuration;
using Abp.Configuration;

namespace Application.Web.Url
{
    public abstract class WebUrlServiceBase
    {
        public const string TenancyNamePlaceHolder = "{TENANCY_NAME}";

        public abstract string WebSiteRootAddressFormatKey { get; }

        public string WebSiteRootAddressFormat { get; set; } = "http://localhost:21021/";

        public bool SupportsTenancyNameInUrl
        {
            get
            {
                var siteRootFormat = WebSiteRootAddressFormat;
                return !siteRootFormat.IsNullOrEmpty() && siteRootFormat.Contains(TenancyNamePlaceHolder);
            }
        }

        readonly IConfigurationRoot _appConfiguration;
        readonly ISettingManager _settingManager;

        public WebUrlServiceBase(IAppConfigurationAccessor configurationAccessor, ISettingManager settingManager)
        {
            _appConfiguration = configurationAccessor.Configuration;
            _settingManager = settingManager;

            WebSiteRootAddressFormat = _appConfiguration[WebSiteRootAddressFormatKey];
            string webSiteRootAddressFormatFromSetting = _settingManager.GetSettingValue(AppSettings.General.WebSiteRootAddress);
            if (!webSiteRootAddressFormatFromSetting.IsNullOrEmpty())
            {
                webSiteRootAddressFormatFromSetting = webSiteRootAddressFormatFromSetting.EnsureEndsWith('/');
                WebSiteRootAddressFormat = webSiteRootAddressFormatFromSetting;
            }
        }

        public string GetWebSiteRootAddress(string tenancyName = null)
        {
            return ReplaceTenancyNameInUrl(WebSiteRootAddressFormat, tenancyName);
        }

        public List<string> GetRedirectAllowedExternalWebSites()
        {
            var values = _appConfiguration["App:RedirectAllowedExternalWebSites"];
            return values?.Split(',').ToList() ?? new List<string>();
        }

        private string ReplaceTenancyNameInUrl(string siteRootFormat, string tenancyName)
        {
            if (!siteRootFormat.Contains(TenancyNamePlaceHolder))
            {
                return siteRootFormat;
            }

            if (siteRootFormat.Contains(TenancyNamePlaceHolder + "."))
            {
                siteRootFormat = siteRootFormat.Replace(TenancyNamePlaceHolder + ".", TenancyNamePlaceHolder);
            }

            if (tenancyName.IsNullOrEmpty())
            {
                return siteRootFormat.Replace(TenancyNamePlaceHolder, "");
            }

            return siteRootFormat.Replace(TenancyNamePlaceHolder, tenancyName.ToLower() + ".");
        }
    }
}