using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;

namespace Application.Controllers
{
    public abstract class ApplicationControllerBase: AbpController
    {
        public IHostingEnvironment hostingEnvironment { get; set; }

        protected ApplicationControllerBase()
        {
            LocalizationSourceName = ApplicationConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected void SetTenantIdCookie(int? tenantId)
        {
            Response.Cookies.Append(
                "Abp.TenantId",
                tenantId?.ToString(),
                new CookieOptions
                {
                    Expires = DateTimeOffset.Now.AddYears(5),
                    Path = "/"
                }
            );
        }

        [HttpGet]
        public ActionResult GetQrcode(string content)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(content, QRCodeGenerator.ECCLevel.Q);
            QRCode qrcode = new QRCode(qrCodeData);

            // qrcode.GetGraphic 方法可参考最下发“补充说明”
            Bitmap qrCodeImage = qrcode.GetGraphic(5, Color.Black, Color.White, null, 15, 6, false);
            MemoryStream memoryStream = new MemoryStream();
            qrCodeImage.Save(memoryStream, ImageFormat.Jpeg);
            return File(memoryStream.ToArray(), "image/Jpeg");
        }

        [HttpGet]
        public ActionResult PictureProxy(string url)
        {
            Uri mUri = new Uri(url);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(mUri);
            request.Method = "GET";
            request.Timeout = 5000;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = null;
            Image image = null;
            try
            {
                stream = response.GetResponseStream();
                image = Image.FromStream(stream);
                MemoryStream memoryStream = new MemoryStream();
                image.Save(memoryStream, ImageFormat.Png);
                return File(memoryStream.ToArray(), response.ContentType);
            }
            finally
            {
                if (image!=null)
                {
                    image.Dispose();
                }
               
                // 释放资源
                if (stream != null)
                    stream.Close();
                if (response != null)
                    response.Close();
            }
        }
    }
}
