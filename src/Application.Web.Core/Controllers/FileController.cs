﻿using System.IO;
using Abp.Auditing;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using Application.Dto;
using Application.Controllers;

namespace Application.Web.Controllers
{
    public class FileController : ApplicationControllerBase
    {
        private readonly IAppFolders _appFolders;
        public PathHelper PathHelper { get; set; }

        public FileController(IAppFolders appFolders)
        {
            _appFolders = appFolders;
        }

        [DisableAuditing]
        public ActionResult DownloadTempFile(FileDto file)
        {
            var filePath = PathHelper.GetAbsolutePath(Path.Combine(_appFolders.TempFileDownloadFolder, file.FileToken));
            if (!System.IO.File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            }

            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            System.IO.File.Delete(filePath);
            return File(fileBytes, file.FileType, file.FileName);
        }
    }
}