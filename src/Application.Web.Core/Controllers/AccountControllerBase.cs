using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Extensions;
using Abp.MultiTenancy;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.Threading;
using Abp.UI;
using Abp.Zero.Configuration;
using Application.Authentication.JwtBearer;
using Application.Authorization;
using Application.Authorization.Models;
using Application.Authorization.Users;
using Application.Configuration;
using Application.Identity;
using Application.MultiTenancy;
using Application.Notifications;
using Application.Security;
using Application.Security.Recaptcha;
using Application.Url;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Application.Controllers
{
    public abstract class AccountControllerBase : ApplicationControllerBase
    {
        public TenantHelper TenantHelper { get; set; }
        public LogInManager LogInManager { get; set; }
        public AbpLoginResultTypeHelper AbpLoginResultTypeHelper { get; set; }
        public ISmsSender SmsSender { get; set; }
        public IEmailSender EmailSender { get; set; }
        public IUserEmailer UserEmailer { get; set; }
        public IAppUrlService AppUrlService { get; set; }
        public IRecaptchaValidator RecaptchaValidator { get; set; }
        public TokenAuthConfiguration Configuration { get; set; }

        public UserManager UserManager { get; set; }
        public TenantManager TenantManager { get; set; }
        public IMultiTenancyConfig MultiTenancyConfig { get; set; }
        public IWebUrlService WebUrlService { get; set; }
        public IAppNotifier AppNotifier { get; set; }
        public SignInManager SignInManager { get; set; }
        public ITenantCache TenantCache { get; set; }
        public UserRegistrationManager UserRegistrationManager { get; set; }
        public IPasswordComplexitySettingStore PasswordComplexitySettingStore { get; set; }
        public IdentityOptions IdentityOptions { get; set; }
        public IPasswordHasher<User> PasswordHasher { get; set; }


        [HttpPost]
        public async Task Logout()
        {
            await SignInManager.SignOutAsync();
        }

        public async Task<LoginOption> GetLoginOptions()
        {
            return new LoginOption
            {
                TenancyName = TenantHelper.GetCurrentTenancyNameOrNull(),
                IsSelfRegistrationEnabled = IsSelfRegistrationEnabled(),
                IsMultiTenancyEnabled = MultiTenancyConfig.IsEnabled,
                AuthenticationSchemes = (await SignInManager.GetExternalAuthenticationSchemesAsync())
                .Where(s => !s.DisplayName.IsNullOrWhiteSpace())
                .ToList()
            };
        }

        public async Task<bool> CheckUsername([FromBody]CheckUserNameInput input)
        {
            if (!string.IsNullOrEmpty(input.TenancyName))
            {
                var tenant = AsyncHelper.RunSync(() => GetActiveTenantAsync(input.TenancyName));
                CurrentUnitOfWork.SetTenantId(tenant.Id);
            }
            User user = await UserManager.FindByNameOrEmailAsync(input.UserName);
            return user != null;
        }

        protected bool UseCaptchaOnRegistration()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                //Host users can not register
                throw new InvalidOperationException();
            }

            return SettingManager.GetSettingValue<bool>(AppSettings.UserManagement.UseCaptchaOnRegistration);
        }

        protected bool IsSelfRegistrationEnabled()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return false; //No registration enabled for host users!
            }

            return SettingManager.GetSettingValue<bool>(AppSettings.UserManagement.AllowSelfRegistration);
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        protected async Task<Tenant> GetActiveTenantAsync(int tenantId)
        {
            var tenant = await TenantManager.FindByIdAsync(tenantId);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("UnknownTenantId{0}", tenantId));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIdIsNotActive{0}", tenantId));
            }

            return tenant;
        }

        protected async Task<Tenant> GetActiveTenantAsync(string tenancyName)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(tenancyName);

            if (tenant == null)
            {
                throw new UserFriendlyException(L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIsNotActive", tenancyName));
            }
            return tenant;
        }

        protected async Task<string> GetTenancyNameOrNullAsync(int? tenantId)
        {
            return tenantId.HasValue ? (await GetActiveTenantAsync(tenantId.Value)).TenancyName : null;
        }

        protected async Task<User> GetUserByChecking(string inputEmailAddress)
        {
            var user = await UserManager.FindByEmailAsync(inputEmailAddress);
            if (user == null)
            {
                throw new UserFriendlyException(L("InvalidEmailAddress"));
            }

            return user;
        }

        // two factor login security code
        public async Task<SendSecurityCodeOutput> SendSecurityCode(SendSecurityCodeInput model)
        {
            var user = await SignInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {

            }

            CheckCurrentTenant(await SignInManager.GetVerifiedTenantIdAsync());

            var code = await UserManager.GenerateTwoFactorTokenAsync(user, model.SelectedProvider);
            var message = L("EmailSecurityCodeBody", code);

            if (model.SelectedProvider == "Email")
            {
                await EmailSender.SendAsync(await UserManager.GetEmailAsync(user), L("EmailSecurityCodeSubject"), message);
            }
            else if (model.SelectedProvider == "Phone")
            {
                await SmsSender.SendAsync(await UserManager.GetPhoneNumberAsync(user), message);
            }

            return new SendSecurityCodeOutput()
            {
                Provider = model.SelectedProvider,
                RememberMe = model.RememberMe
            };
        }

        public async Task VerifySecurityCode(VerifySecurityCodeViewModel model)
        {
            CheckCurrentTenant(await SignInManager.GetVerifiedTenantIdAsync());

            var result = await SignInManager.TwoFactorSignInAsync(
                model.Provider,
                model.Code,
                model.RememberMe,
                await IsRememberBrowserEnabledAsync() && model.RememberBrowser
            );

            if (result.IsLockedOut)
            {
                throw new UserFriendlyException(L("UserLockedOutMessage"));
            }

            throw new UserFriendlyException(L("InvalidSecurityCode"));
        }

        protected Task<bool> IsRememberBrowserEnabledAsync()
        {
            return SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsRememberBrowserEnabled);
        }

        protected void CheckCurrentTenant(int? tenantId)
        {
            if (AbpSession.TenantId != tenantId)
            {
                throw new Exception($"Current tenant is different than given tenant. AbpSession.TenantId: {AbpSession.TenantId}, given tenantId: {tenantId}");
            }
        }

        protected async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            var loginResult = await LogInManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw AbpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
            }
        }

        protected string CreateAccessToken(IEnumerable<Claim> claims, TimeSpan? expiration = null)
        {
            var now = DateTime.UtcNow;

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: Configuration.Issuer,
                audience: Configuration.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(expiration ?? Configuration.Expiration),
                signingCredentials: Configuration.SigningCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }

        protected static List<Claim> CreateJwtClaims(ClaimsIdentity identity)
        {
            var claims = identity.Claims.ToList();
            var nameIdClaim = claims.First(c => c.Type == ClaimTypes.NameIdentifier);

            // Specifically add the jti (random nonce), iat (issued timestamp), and sub (subject/user) claims.
            claims.AddRange(new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, nameIdClaim.Value),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.Now.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            });

            return claims;
        }

        protected string GetEncrpyedAccessToken(string accessToken)
        {
            return SimpleStringCipher.Instance.Encrypt(accessToken, AppConsts.DefaultPassPhrase);
        }
    }
}
