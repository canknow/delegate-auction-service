﻿namespace Application.Authorization.Models
{
    public class CheckUserNameInput
    {
        public string TenancyName { get; set; }

        public string UserName { get; set; }
    }
}
