﻿namespace Application.Web.Host.Areas.Client.Models
{
    public class SendLoginPhoneCaptchaInput
    {
        public string PhoneNumber { get; set; }
    }
}
