﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Authorization.Models
{
    public class VerifyLoginPhoneCaptchaInput
    {
        public string PhoneNumber { get; set; }

        public string Code { get; set; }
    }
}
