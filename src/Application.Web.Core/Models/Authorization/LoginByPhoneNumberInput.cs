﻿namespace Application.Authorization.Models
{
    public class LoginByPhoneNumberInput
    {
        public string TenancyName { get; set; }

        public string PhoneNumber { get; set; }

        public string Captcha { get; set; }
    }
}
