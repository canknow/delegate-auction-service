﻿using Abp.Authorization.Users;
using System.ComponentModel.DataAnnotations;

namespace Application.Authorization.Models
{
    public class SendPasswordResetCodeInput
    {
        [Required]
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}
