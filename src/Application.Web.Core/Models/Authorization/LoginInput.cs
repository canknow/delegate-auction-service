﻿using Abp.Auditing;
using System.ComponentModel.DataAnnotations;

namespace Application.Authorization.Models
{
    public class LoginInput
    {
        public string TenancyName { get; set; }

        [Required]
        public string UserNameOrEmailAddress { get; set; }

        [Required]
        [DisableAuditing]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string SS { get; set; }
    }
}
