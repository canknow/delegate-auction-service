﻿namespace Application.Authorization.Models
{
    public class SendSecurityCodeOutput
    {
        public string Provider { get; set; }

        public bool RememberMe { get; set; }
    }
}
