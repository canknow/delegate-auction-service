﻿using System.ComponentModel.DataAnnotations;

namespace Application.Authorization.Models
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}
