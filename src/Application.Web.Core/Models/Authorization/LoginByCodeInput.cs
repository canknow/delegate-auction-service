﻿namespace Application.Authorization.Models
{
    public class LoginByCodeInput
    {
        public string Code { get; set; }

        public string Provider { get; set; }
    }
}
