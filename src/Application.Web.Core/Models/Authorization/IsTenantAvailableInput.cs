﻿using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;

namespace Application.Authorization.Models
{
    public class IsTenantAvailableInput
    {
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        public string TenancyName { get; set; }
    }
}
