﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Application.Authorization.Models
{
    public class SendSecurityCodeInput
    {
        public List<string> Providers { get; set; }

        [Required]
        public string SelectedProvider { get; set; }

        public bool RememberMe { get; set; }
    }
}