﻿using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;

namespace Application.Authorization.Models
{
    public class LoginOption
    {
        public string TenancyName { get; set; }

        public bool IsMultiTenancyEnabled { get; set; }

        public bool IsSelfRegistrationEnabled { get; set; }

        public List<AuthenticationScheme> AuthenticationSchemes { get; set; }
    }
}
