﻿namespace Application.Authorization.Models
{
    public class LoginResultOutput
    {
        public bool ResetPassword { get; set; }

        public bool RequiresTwoFactor { get; set; }

        public string AccessToken { get; set; }

        public string EncryptedAccessToken { get; set; }

        public int ExpireInSeconds { get; set; }

        public long UserId { get; set; }
    }
}
