﻿namespace Application.Authorization.Models
{
    public class ActivateEmailInput
    {
        public long UserId { get; set; }

        public string ConfirmationCode { get; set; }

        /// <summary>
        /// Encrypted values for {TenantId}, {UserId} and {ConfirmationCode}
        /// </summary>
        public string c { get; set; }
    }
}
