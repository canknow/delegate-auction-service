﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Net.Sms;
using Abp.UI;
using Application.Captchas;
using Application.Security.Recaptcha;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Application.Web.Security.Recaptcha
{
    public class RecaptchaValidator : ApplicationServiceBase, IRecaptchaValidator, ITransientDependency
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ISmsSender SmsSender { get; set; }
        public IRepository<Captcha> CaptchaRepository { get; set; }
        public string CaptchaCodeSessionName = "CaptchaCodeSession";

        public RecaptchaValidator(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task Send(string provider, string providerKey, string code)
        {
            string DefaultFreeSignName = await SettingManager.GetSettingValueAsync(SmsSettingNames.DefaultFreeSignName);
            string DefaultSmsTemplateCode = await SettingManager.GetSettingValueAsync(SmsSettingNames.DefaultSmsTemplateCode);

            string data = JsonConvert.SerializeObject(new
            {
                code
            });
            SmsMessage smsMessage = new SmsMessage(providerKey, DefaultSmsTemplateCode, data, DefaultFreeSignName);
            await SmsSender.SendAsync(smsMessage);
            CaptchaRepository.Insert(new Captcha()
            {
                Provider = provider,
                ProviderKey = providerKey,
                Code = code,
                VerificationState = VerificationState.UnVerified
            });
            _httpContextAccessor.HttpContext.Session.SetString(CaptchaCodeSessionName, code);
        }

        public async Task ReSend(Captcha captcha)
        {
            if (captcha.VerificationState == VerificationState.UnVerified)
            {
                string DefaultFreeSignName = await SettingManager.GetSettingValueAsync(SmsSettingNames.DefaultFreeSignName);
                string DefaultSmsTemplateCode = await SettingManager.GetSettingValueAsync(SmsSettingNames.DefaultSmsTemplateCode);

                string data = JsonConvert.SerializeObject(new
                {
                    captcha.Code
                });
                SmsMessage smsMessage = new SmsMessage(captcha.ProviderKey, DefaultSmsTemplateCode, data, DefaultFreeSignName);
                await SmsSender.SendAsync(smsMessage);
            }
        }

        public async Task ValidateAsync(string provider, string providerKey, string code)
        {
            var isValid = code.Equals(_httpContextAccessor.HttpContext.Session.GetString(CaptchaCodeSessionName), StringComparison.OrdinalIgnoreCase);
            _httpContextAccessor.HttpContext.Session.Remove(CaptchaCodeSessionName);

            if (isValid)
            {
                var entity = await CaptchaRepository.FirstOrDefaultAsync(model => model.Provider == provider && model.ProviderKey == providerKey && model.Code == code);
                if (entity != null)
                {
                    entity.VerificationState = VerificationState.Verified;
                    await CaptchaRepository.UpdateAsync(entity);
                }
                return;
            }
            throw new UserFriendlyException(L("IncorrectCaptchaAnswer"));
        }
    }
}
