﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Client.Groups.Dto;

namespace Application.Client.Groups
{
    public interface IGroupAppService: IApplicationService
    {
        GroupDataGetOutput GetGroupData();

        PagedResultDto<CustomerDto> GetCustomers(CustomerGetAllInput input);
    }
}
