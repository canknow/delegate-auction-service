﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace Application.Client.Qrcodes
{
    public interface IQrcodeAppService:IApplicationService
    {
        Task<string> GetQrcode();

        Task<string> ReCreateQrcode();
    }
}
