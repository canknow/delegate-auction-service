﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Application.Shares;
using Application.SpreadPosters;
using Application.Wechat.Qrcodes;
using System;
using System.Threading.Tasks;

namespace Application.Client.Qrcodes
{
    [AbpAuthorize]
    public class QrcodeAppService:ApplicationAppServiceBase, IQrcodeAppService
    {
        public SpreadPosterManager SpreadPosterManager { get; set; }
        public QrcodeManager QrcodeManager { get; set; }
        private IRepository<Share> ShareRepository;

        public QrcodeAppService(IRepository<Share> shareRepository)
        {
            ShareRepository = shareRepository;
        }

        public async Task<string> GetQrcode()
        {
            try
            {
                Qrcode qrcode = await QrcodeManager.GetQrcodeAsync(AbpSession.ToUserIdentifier());
                return qrcode.Path;
            }
            catch(Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<string> ReCreateQrcode()
        {
            try
            {
                Qrcode qrcode = await QrcodeManager.ReCreateQrcodeAsync(AbpSession.ToUserIdentifier());
                return qrcode.Path;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }
    }
}
