﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Client.Articles.Dto;

namespace Application.Client.Articles
{
    public interface IArticleAppService : ICrudAppService<ArticleDto,int, ArticleGetAllInput>
    {
        ArticleGetOutput GetArticle(EntityDto<int> input);

        ArticleLikeDto AddLike(ArticleLikeInput input);
    }
}