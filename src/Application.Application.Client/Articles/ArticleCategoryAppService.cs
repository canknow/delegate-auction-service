﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.Client.Articles.Dto;
using Application.Articles;

namespace Application.Client.Articles.Front
{
    public class ArticleCategoryAppService : CrudAppService<ArticleCategory, ArticleCategoryDto>, IArticleCategoryAppService
    {
        public ArticleCategoryAppService(IRepository<ArticleCategory> respository) :base(respository)
        {
        }
    }
}
