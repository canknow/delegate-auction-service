﻿using Abp.Application.Services;
using Application.Client.Articles.Dto;

namespace Application.Client.Articles
{
    public interface IArticleCategoryAppService : ICrudAppService<ArticleCategoryDto>
    {
    }
}