﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Articles;
using Application.Client.Articles.Dto;
using System.Linq;

namespace Application.Client.Articles
{
    public class ArticleAppService : CrudAppService<Article, ArticleDto,int, ArticleGetAllInput>, IArticleAppService
    {
        public ArticleManager ArticleManager { get; set; }
        private IRepository<ArticleLike> ArticleLikeRepository;
        public ArticleAppService(IRepository<Article> respository,  IRepository<ArticleLike> articleLikeRepository) :base(respository)
        {
            ArticleLikeRepository = articleLikeRepository;
        }

        protected override IQueryable<Article> CreateFilteredQuery(ArticleGetAllInput input)
        {
            var query = Repository.GetAll().Where(model=>model.Status==ArticleStatus.On)
                .WhereIf(!string.IsNullOrEmpty(input.Title), model => model.Title == input.Title)
                .WhereIf(input.ArticleCategoryId.HasValue, model => model.ArticleCategoryId == input.ArticleCategoryId);
            return query;
        }

        public ArticleGetOutput GetArticle(EntityDto<int> input)
        {
            CheckGetPermission();
            var artile = GetEntityById(input.Id);
            ArticleManager.AddHint(AbpSession.UserId.Value, artile);

            ArticleGetOutput articleGetOutput = new ArticleGetOutput()
            {
                Article= artile.MapTo<ArticleDto>(),
            };
            var articleLike = ArticleLikeRepository.GetAll().Where(model => model.ArticleId == input.Id && model.UserId == AbpSession.UserId.Value).FirstOrDefault();

            if (articleLike != null)
            {
                articleGetOutput.ArticleLike = articleLike.MapTo<ArticleLikeDto>();
            }
            return articleGetOutput;
        }

        public ArticleLikeDto AddLike(ArticleLikeInput input)
        {
            ArticleLike articleLike=ArticleManager.AddLike(AbpSession.UserId.Value,input.Id, input.LikeType);
            return articleLike.MapTo<ArticleLikeDto>();
        }
    }
}
