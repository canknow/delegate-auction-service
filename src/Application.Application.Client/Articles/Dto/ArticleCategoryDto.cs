﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Articles;

namespace Application.Client.Articles.Dto
{
    [AutoMap(typeof(ArticleCategory))]
    public class ArticleCategoryDto: EntityDto
    {
        public string Name { get; set; }

        public string Icon { get; set; }
    }
}
