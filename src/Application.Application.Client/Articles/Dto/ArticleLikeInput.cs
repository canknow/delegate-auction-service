﻿using Abp.Application.Services.Dto;
using Application.Articles;

namespace Application.Client.Articles.Dto
{
    public class ArticleLikeInput : EntityDto
    {
        public LikeType LikeType{get;set;}
    } 
}
