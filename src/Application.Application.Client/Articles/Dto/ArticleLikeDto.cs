﻿using Abp.AutoMapper;
using Application.Articles;
using System;

namespace Application.Client.Articles.Dto
{
    [AutoMapFrom(typeof(ArticleLike))]
    public class ArticleLikeDto
    {
        public DateTime CreationTime { get; set; }
        public LikeType Like { get; set; }
    }
}
