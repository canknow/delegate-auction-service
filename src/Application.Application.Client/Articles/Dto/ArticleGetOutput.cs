﻿namespace Application.Client.Articles.Dto
{
    public class ArticleGetOutput
    {
        public ArticleDto Article { get; set; }
        public ArticleLikeDto ArticleLike { get; set; }
    }
}
