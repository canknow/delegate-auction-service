﻿using Abp.Application.Services.Dto;

namespace Application.Client.Articles.Dto
{
    public class ArticleGetAllInput:PagedAndSortedResultRequestDto
    {
        public int? ArticleCategoryId { get; set; }

        public string Title { get; set; }
    }
}
