﻿using Abp.AutoMapper;
using Application.Shops;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Application.Services.Dto;

namespace Application.Client.Shops.Dto
{
    [AutoMap(typeof(Shop))]
    public class ShopDto:EntityDto
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Seal { get; set; }

        public string WechatQrcode { get; set; }

        public string TelePhone { get;set; }

        public string AboutUs { get; set; }

        public string Logo { get; set; }

        public string CopyrightLogo { get; set; }

        public string ShareTitle { get; set; }

        public string ShareDescription { get; set; }

        public string SharePicture { get; set; }

        [NotMapped]
        public string SubscribeLink { get; set; }

        public string AgreeContent { get; set; }

        public bool EnableAgree { get; set; }

        public string Notice { get; set; }

        public string UploadBidNotice { get; set; }


        public string OfflinePayNote { get; set; }
    }
}
