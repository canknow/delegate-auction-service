﻿using Abp.Application.Services;
using Application.Client.Shops.Dto;

namespace Application.Client.Shops
{
    public interface IShopAppService : ICrudAppService<ShopDto>
    {
     ShopDto GetShop();

     ShopTemplateDto GetShopTemplate();
    }
}
