﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Application.Client.Shops.Dto;
using Application.MultiTenancy;
using Application.Shops;
using Application.ShopTemplates;
using System.Linq;

namespace Application.Client.Shops
{
    public class ShopAppService : CrudAppService<Shop, ShopDto>,
        IShopAppService
    {
        public TenantHelper TenantHelper { get; set; }
        public ShopManager ShopManager {get;set;}
        public IRepository<ShopTemplate> ShopTemplateRespository{get;set;}
        
        public ShopAppService(IRepository<Shop> respository)
            : base(respository)
        {
        }

        public ShopDto GetShop(){
            Shop shop = ShopManager.GetShop();
            return shop.MapTo<ShopDto>();
        }

        public ShopTemplateDto GetShopTemplate()
        {
            Tenant tenant = TenantHelper.GetTenant();
            CurrentUnitOfWork.SetTenantId(tenant.Id);
            ShopTemplate shopTemplate = ShopTemplateRespository.GetAll().FirstOrDefault();
            ShopTemplateDto shopTemplateDto = shopTemplate.MapTo<ShopTemplateDto>();
            return shopTemplateDto;
        }
    }
}
