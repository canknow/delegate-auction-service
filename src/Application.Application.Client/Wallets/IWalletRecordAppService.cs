﻿using Abp.Application.Services;
using Application.Client.Wallets.Dto;

namespace Application.Client.Wallets
{
    public interface IWalletRecordAppService : ICrudAppService<
        WalletRecordDto,
        int,
        WalletRecordGetAllInput>
    {
    }
}
