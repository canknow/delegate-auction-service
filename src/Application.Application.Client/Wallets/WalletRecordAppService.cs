﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Application.Client.Wallets.Dto;
using Application.Wallets;
using System.Linq;

namespace Application.Client.Wallets
{
    [AbpAuthorize]
    public class WalletRecordAppService : CrudAppService<
        WalletRecord,
        WalletRecordDto,
        int,
        WalletRecordGetAllInput
        >, IWalletRecordAppService
    {
        public WalletRecordAppService(IRepository<WalletRecord> repository) :base(repository)
        {

        }

        protected override IQueryable<WalletRecord> CreateFilteredQuery(WalletRecordGetAllInput input)
        {
            return Repository.GetAll().Where(model => model.UserId ==AbpSession.UserId.Value);
        }
    }
}
