﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Application.Client.Wallets.Dto;
using Application.Wallets;
using System;
using System.Threading.Tasks;

namespace Application.Client.Wallets
{
    [AbpAuthorize]
    public class WalletAppService: ApplicationAppServiceBase, IWalletAppService
    {
        private readonly IRepository<Wallet> _walletRepository;
        public WalletManager WalletManager { get; set; }
        public WithdrawManager WithdrawManager { get; set; }

        public WalletAppService(IRepository<Wallet> walletRepository)
        {
            _walletRepository = walletRepository;
        }

        public async Task Withdraw(WithdrawApplyInput input)
        {
            try
            {
                await WithdrawManager.WithdrawAllBalanceAsync(AbpSession.ToUserIdentifier(),input.FinanceAccountId);
            }
            catch(Exception exception)
            {
                throw new UserFriendlyException(exception.Message, exception);
            }
        }

        public WalletInfoOutput GetWalletInfo()
        {
            try
            {
                Wallet wallet = WalletManager.GetWalletOfUser(AbpSession.ToUserIdentifier());
                WalletInfoOutput walletInfoOutput = new WalletInfoOutput()
                {
                    Balance = wallet.Money,
                    Freezing = WalletManager.GetMoneyOfFreezing(AbpSession.UserId.Value),
                    Withdrawed = -WalletManager.GetMoneyOfWithdrawed(AbpSession.UserId.Value),
                    Withdrawing = WalletManager.GetMoneyOfWithdrawing(AbpSession.UserId.Value),
                    TotalIncome = WalletManager.GetMoneyOfRecharge(AbpSession.UserId.Value)
                };
                return walletInfoOutput;
            }
            catch(Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }
    }
}
