﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Client.Wallets.Dto
{
    public class WalletRecordGetAllInput : PagedAndSortedResultRequestDto
    {
        public long UserId { get; set; }
    }
}
