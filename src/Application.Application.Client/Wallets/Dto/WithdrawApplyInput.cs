﻿namespace Application.Client.Wallets.Dto
{
    public class WithdrawApplyInput
    {
        public int? FinanceAccountId { get; set; }
    }
}
