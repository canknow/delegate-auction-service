﻿using Abp.Application.Services;
using Application.Client.Wallets.Dto;
using System.Threading.Tasks;

namespace Application.Client.Wallets
{
    public interface IWalletAppService: IApplicationService
    {
        WalletInfoOutput GetWalletInfo();

        Task Withdraw(WithdrawApplyInput input);
    }
}
