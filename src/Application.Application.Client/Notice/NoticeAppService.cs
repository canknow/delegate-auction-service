﻿using Application.Client.Notices.Dto;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.Notices;
using Abp.Application.Services.Dto;
using System.Linq;

namespace Application.Client.Notices
{
    public class NoticeAppService:CrudAppService<Notice, NoticeDto>, INoticeAppService
    {
        public NoticeAppService(IRepository<Notice> respository) :base(respository)
        {
        }

        protected override IQueryable<Notice> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input).Where(model=>model.Status ==NoticeStatus.On);
        }
    }
}
