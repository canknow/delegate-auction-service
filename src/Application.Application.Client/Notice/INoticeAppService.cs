﻿using Abp.Application.Services;
using Application.Client.Notices.Dto;

namespace Application.Client.Notices
{
    public interface INoticeAppService: ICrudAppService<NoticeDto>
    {
    }
}
