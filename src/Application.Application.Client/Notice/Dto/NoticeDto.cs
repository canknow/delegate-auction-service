﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Notices;
using System.ComponentModel.DataAnnotations;

namespace Application.Client.Notices.Dto
{
    [AutoMap(typeof(Notice))]
    public class NoticeDto : EntityDto
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public NoticeStatus Status { get; set; }
    }
}
