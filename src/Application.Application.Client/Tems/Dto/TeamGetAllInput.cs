﻿using Abp.Application.Services.Dto;

namespace Application.Client.Teams.Dto
{
    public class TeamGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }

        public bool? IsHot { get; set; }
    }
}
