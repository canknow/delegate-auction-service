﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Client.Packages.Dto;
using Application.Teams;
using System.Collections.Generic;

namespace Application.Client.Teams.Dto
{
    [AutoMap(typeof(Team))]
    public class TeamDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string Logo { get; set; }

        public string Level { get; set; }

        public string Introduce { get; set; }

        public string Illustration { get; set; }

        public float Rate { get; set; }

        public int DealCount { get; set; }

        public int HintCount { get; set; }

        public List<PackageDto> Packages { get; set; }

        public int Quota { get; set; }

        public bool IsHot { get; set; }

        public bool IsShow { get; set; }
    }
}
