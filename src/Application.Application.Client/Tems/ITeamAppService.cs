﻿using Abp.Application.Services;
using Application.Client.Teams.Dto;

namespace Application.Client.Teams
{
    public interface ITeamAppService : ICrudAppService<TeamDto, int, TeamGetAllInput>
    {
    }
}