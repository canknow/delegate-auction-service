﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Client.Teams.Dto;
using Application.Teams;
using System.Linq;

namespace Application.Client.Teams
{
    public class TeamAppService : CrudAppService<Team, TeamDto, int, TeamGetAllInput>, ITeamAppService
    {
        public IRepository<TeamHintRecord> TeamHintRecordRespository { get; set; }

        public TeamAppService(IRepository<Team> respository) : base(respository)
        {

        }

        protected override IQueryable<Team> CreateFilteredQuery(TeamGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Name), model => model.Name == input.Name).Where(model=>model.IsShow)
                 .WhereIf(input.IsHot.HasValue, model => model.IsHot == input.IsHot.Value);
        }

        public override TeamDto Get(EntityDto<int> input)
        {
            var team = Repository.Get(input.Id);
            if (AbpSession.UserId.HasValue)
            {
                TeamHintRecord teamHintRecord = TeamHintRecordRespository.GetAll().Where(model => model.TeamId == input.Id && model.UserId == AbpSession.UserId.Value).FirstOrDefault();
                if (teamHintRecord == null)
                {
                    teamHintRecord = new TeamHintRecord()
                    {
                        UserId = AbpSession.UserId.Value,
                        TeamId = input.Id
                    };
                    TeamHintRecordRespository.Insert(teamHintRecord);
                    team.HintCount++;
                    Repository.Update(team);
                }
            }
            return team.MapTo<TeamDto>();
        }
    }
}
