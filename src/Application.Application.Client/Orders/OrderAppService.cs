﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Application.Bids;
using Application.Client.Orders.Dto;
using Application.Client.Pay.Dto;
using Application.Orders;
using Application.Packages;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Client.Orders
{
    public class OrderAppService : CrudAppService<Order, OrderDto, int, OrderGetAllInput>, IOrderAppService
    {
        public BidManage BidManage { get; set; }
        public OrderManager OrderManage { get; set; }
        public IRepository<Package> PackageRespository { get; set; }

        public OrderAppService(IRepository<Order> respository) : base(respository)
        {

        }

        protected override IQueryable<Order> CreateFilteredQuery(OrderGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Number), model => model.Number == input.Number)
                .WhereIf(input.OrderStatus.HasValue, model=>model.OrderStatus == input.OrderStatus)
                .Where(model=>model.UserId==AbpSession.UserId.Value);
        }

        public async Task<OrderDto> CreateOrder(OrderCreateInput input)
        {
            Bid bid = BidManage.UpdateBid(input.Bid.IDNumber, input.Bid.FullName, input.Bid.PhoneNumber, input.Bid.Account, input.Bid.Password, input.Bid.BidPicture, input.Bid.FrontOfIDCard,input.Bid.BackOfIDCard);
            Package package = await PackageRespository.GetAsync(input.PackageId);

            Order order = await OrderManage.CreateOrderAsync(input.TeamId, package, input.CompensationRuleId, bid, input.AuctionNumber);
         
            CurrentUnitOfWork.SaveChanges();
            return order.MapTo<OrderDto>();
        }

        public PayOutput GetPayOutput(PayInput input)
        {
            OrderDto order = Get(new EntityDto()
            {
                Id = input.Id
            });

            if (order.PaymentStatus == PaymentStatus.Payed)
            {
                throw new UserFriendlyException("the order has payed!");
            }
            PayOutput payOutput = new PayOutput()
            {
                Order = order
            };
            return payOutput;
        }

        public async Task<CommentDto> Comment(CommentInput input)
        {
            Order order = Repository.Get(input.OrderId);
            var comment = await OrderManage.Comment(order, input.Content, input.Rate);
            return comment.MapTo<CommentDto>();
        }
    }
}
