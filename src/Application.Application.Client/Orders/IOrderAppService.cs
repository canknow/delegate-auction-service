﻿using Abp.Application.Services;
using Application.Client.Orders.Dto;
using Application.Client.Pay.Dto;
using Application.Orders;
using System.Threading.Tasks;

namespace Application.Client.Orders
{
    public interface IOrderAppService : ICrudAppService<OrderDto, int, OrderGetAllInput>
    {
        Task<OrderDto> CreateOrder(OrderCreateInput input);

        PayOutput GetPayOutput(PayInput input);

        Task<CommentDto> Comment(CommentInput input);
    }
}