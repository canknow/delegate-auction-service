﻿namespace Application.Client.Orders.Dto
{
    public class CommentInput
    {
        public int OrderId { get; set; }

        public string Content { get; set; }

        public int Rate { get; set; }
    }
}
