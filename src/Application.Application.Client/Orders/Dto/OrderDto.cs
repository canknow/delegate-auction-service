﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Client.Bids.Dto;
using Application.Client.Packages.Dto;
using Application.Client.Teams.Dto;
using Application.Orders;
using System.Collections.Generic;

namespace Application.Client.Orders.Dto
{
    [AutoMapFrom(typeof(Order))]
    public class OrderDto : FullAuditedEntityDto
    {
        public int? TeamId { get; set; }

        public TeamDto Team { get; set; }

        public int? PackageId { get; set; }

        public PackageDto Package { get; set; }

        public int? CompensationRuleId { get; set; }

        public CompensationRuleDto CompensationRule { get; set; }

        public string Title{get;set;}

        public decimal Money{get;set;}

        public string Number{get;set;}

        public OrderStatus OrderStatus { get;set;}

        public PaymentStatus PaymentStatus { get; set; }

        public PaymentMethod PaymentMethod{get;set;}

        public int BidId{get;set;}

        public BidDto Bid { get; set; }

        public string AuditRemark { get; set; }

        public string DenyReason { get; set; }

        public string OrderStatusText { get; set; }

        public List<AuctionAttemptDto> AuctionAttempts { get; set; }

        public string Contract { get; set; }

        public CommentDto Comment { get; set; }

        public int AuctionNumber { get; set; }

        public CompensationStatus CompensationStatus { get; set; }

        public AuctionResult AuctionResult { get; set; }

        public int RemainAuctionCount { get; set; }
    }
}
