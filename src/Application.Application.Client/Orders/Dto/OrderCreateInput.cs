﻿namespace Application.Client.Orders.Dto
{
    public class OrderCreateInput
    {
        public BidCreateDto Bid { get; set; }

        public int? TeamId { get; set; }

        public int PackageId { get; set; }

        public int CompensationRuleId { get; set; }

        public int AuctionNumber { get; set; }
    }
}
