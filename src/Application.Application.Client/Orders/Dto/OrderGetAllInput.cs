﻿using Abp.Application.Services.Dto;
using Application.Orders;

namespace Application.Client.Orders.Dto
{
    public class OrderGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Number { get; set; }

        public OrderStatus? OrderStatus { get; set; }
    }
}
