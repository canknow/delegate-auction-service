﻿namespace Application.Client.Orders.Dto
{
    public class BidCreateDto
    {
        public string IDNumber { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string Account { get; set; }

        public string Password { get; set; }

        public string BidPicture { get; set; }

        public string FrontOfIDCard { get; set; }

        public string BackOfIDCard { get; set; }
    }
}
