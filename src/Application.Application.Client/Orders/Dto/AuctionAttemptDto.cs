﻿using Abp.AutoMapper;
using Application.AuctionAttempts;
using System;

namespace Application.Client.Orders.Dto
{
    [AutoMapFrom(typeof(AuctionAttempt))]
    public class AuctionAttemptDto
    {
        public AuctionAttemptResult Result { get; set; }

        public string Shot { get; set; }

        public DateTime AuctionDateTime { get; set; }
    }
}
