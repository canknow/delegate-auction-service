﻿using Abp.AutoMapper;
using Application.Comments;

namespace Application.Client.Orders.Dto
{
    [AutoMapFrom(typeof(Comment))]
    public class CommentDto
    {
        public string Content { get; set; }

        public int Rate { get; set; }
    }
}
