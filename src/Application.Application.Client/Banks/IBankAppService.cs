﻿using Application.Client.Banks.Dto;
using Abp.Application.Services;

namespace Application.Client.Banks.Tenants
{
    public interface IBankAppService: ICrudAppService<BankDto>
    {
    }
}
