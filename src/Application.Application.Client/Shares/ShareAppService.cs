﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Application.Authorization;
using Application.Client.Spread.Shares.Dto;
using Application.Shares;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Client.Spread.Shares
{
    [AbpAuthorize]
    public class ShareAppService :
        CrudAppService<Share, ShareDto,int,PagedAndSortedResultRequestDto,ShareForCreateInput,ShareDto>,
        IShareAppService
    {
        public ShareManager ShareManager { get; set; }
        public ApplicationAuthorizationHelper ApplicationAuthorizationHelper { get; set; }

        public ShareAppService(IRepository<Share> respository) :base(respository)
        {

        }

        public async Task ProcessShareAccessAsync(ProcessShareAccessInput input)
        {
            Share share = Repository.FirstOrDefault(model => model.No == input.ShareNo);
            if (share != null)
            {
               await ShareManager.ProcessShareAccessAsync(input.ShareNo, ApplicationAuthorizationHelper.CurrentUser);
            }
        }

        protected override IQueryable<Share> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return Repository.GetAll().Where(model=>model.CreatorUserId==AbpSession.UserId.Value);
        }

        public PreShareOutput GetPreShare()
        {
            PreShareOutput preShareOutput = new PreShareOutput();
            preShareOutput.No = Application.Shares.Share.CreateNo();
            return preShareOutput;
        }

        public ShareDto Share(ShareForCreateInput shareForCreateInput)
        {
            ShareDto share = Create(shareForCreateInput);
            return share;
        }
    }
}
