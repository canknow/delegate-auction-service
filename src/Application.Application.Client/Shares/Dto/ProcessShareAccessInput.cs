﻿namespace Application.Client.Spread.Shares.Dto
{
    public class ProcessShareAccessInput
    {
        public string ShareNo { get; set; }
    }
}
