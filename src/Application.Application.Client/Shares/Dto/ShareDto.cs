﻿using Application.Shares;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Client.Spread.Shares.Dto
{
    [AutoMap(typeof(Share))]
    public class ShareDto:AuditedEntityDto
    {
        public string No { get; set; }

        public string Title { get; set; }

        public string Link { get; set; }

        public string ImgUrl { get; set; }

        public string Desc { get; set; }

        public int AccessCount { get; set; }
    }
}
