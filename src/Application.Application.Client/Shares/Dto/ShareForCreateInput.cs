﻿using Application.Shares;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Application.Client.Spread.Shares.Dto
{
    [AutoMapTo(typeof(Share))]
    public class ShareForCreateInput
    {
        [Required]
        public string No { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Link { get; set; }

        public string ImgUrl { get; set; }

        public string Desc { get; set; }
    }
}
