﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Client.Spread.Shares.Dto;
using System.Threading.Tasks;

namespace Application.Client.Spread.Shares
{
    public interface IShareAppService : ICrudAppService<ShareDto, int, PagedAndSortedResultRequestDto, ShareForCreateInput, ShareDto>
    {
        Task ProcessShareAccessAsync(ProcessShareAccessInput input);

        PreShareOutput GetPreShare();

        ShareDto Share(ShareForCreateInput shareForCreateInput);
    }
}
