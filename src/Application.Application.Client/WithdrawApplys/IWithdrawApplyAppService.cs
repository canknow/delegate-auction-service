﻿using Abp.Application.Services;
using Application.Client.WithdrawApplys.Dtos;

namespace Application.Client.WithdrawApplys
{
    public interface IWithdrawApplyAppService : ICrudAppService<WithdrawApplyDto,int, WithdrawApplyGetAllInput>
    {
    }
}
