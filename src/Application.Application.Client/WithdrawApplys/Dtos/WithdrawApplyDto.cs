﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Client.Wallets.Dto;
using Application.Wallets;

namespace Application.Client.WithdrawApplys.Dtos
{
    [AutoMap(typeof(WithdrawApply))]
    public class WithdrawApplyDto : FullAuditedEntityDto
    {
        public string SerialNumber { get; set; }

        public decimal Money { get; set; }

        public PayType? PayType { get; set; }

        public WithdrawStatus Status { get; set; }

        public WithdrawMethod WithdrawMethod { get; set; }

        public WalletRecordDto WalletRecord { get; set; }

        public string FailReason { get; set; }

        public string PayTypeText { get; set; }

        public string WithdrawMethodText { get; set; }

        public string StatusText { get; set; }
    }
}
