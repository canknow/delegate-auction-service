﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Application.Client.WithdrawApplys.Dtos;
using Application.FinanceAccounts;
using Application.Wallets;
using System.Linq;

namespace Application.Client.WithdrawApplys
{
    [AbpAuthorize]
    public class WithdrawApplyAppService : CrudAppService<WithdrawApply, WithdrawApplyDto,int, WithdrawApplyGetAllInput>,IWithdrawApplyAppService
    {
        public FinanceAccountManager FinanceAccountManager { get; set; }
        public WithdrawApplyAppService(
            IRepository<WithdrawApply> respository) 
            :base(respository)
        {
        }

        protected override IQueryable<WithdrawApply> CreateFilteredQuery(WithdrawApplyGetAllInput input)
        {
            return Repository.GetAll().Where(model=>model.UserId==AbpSession.UserId.Value);
        }
    }
}
