﻿using Abp.Application.Services;
using Application.Client.QuestionAndAnswers.Dto;

namespace Application.Client.QuestionAndAnswers
{
    public interface IQuestionAndAnswerAppService: ICrudAppService<QuestionAndAnswerDto>
    {
    }
}
