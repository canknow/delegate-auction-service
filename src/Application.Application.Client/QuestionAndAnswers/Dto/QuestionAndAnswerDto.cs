﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.QuestionAndAnswers;
using System.ComponentModel.DataAnnotations;

namespace Application.Client.QuestionAndAnswers.Dto
{
    [AutoMap(typeof(QuestionAndAnswer))]
    public class QuestionAndAnswerDto : EntityDto
    {
        public string Question { get; set; }

        public string Answer { get; set; }
    }
}
