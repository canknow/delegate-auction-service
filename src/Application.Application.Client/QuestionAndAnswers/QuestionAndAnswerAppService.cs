﻿using Application.Client.QuestionAndAnswers.Dto;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.QuestionAndAnswers;

namespace Application.Client.QuestionAndAnswers
{
    public class QuestionAndAnswerAppService:CrudAppService<QuestionAndAnswer, QuestionAndAnswerDto>, IQuestionAndAnswerAppService
    {
        public QuestionAndAnswerAppService(IRepository<QuestionAndAnswer> respository) :base(respository)
        {
        }
    }
}
