﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Application.Client.FinanceAccounts.Dtos;
using Application.FinanceAccounts;
using Application.FinanceAccounts.Entities;
using System.Linq;

namespace Application.Client.FinanceAccounts
{
    [AbpAuthorize]
    public class FinanceAccountAppService : CrudAppService<FinanceAccount, FinanceAccountDto,int, FinanceAccountGetAllInput>,IFinanceAccountAppService
    {
        public FinanceAccountManager FinanceAccountManager { get; set; }
        public FinanceAccountAppService(
            IRepository<FinanceAccount> respository) 
            :base(respository)
        {
        }

        protected override IQueryable<FinanceAccount> CreateFilteredQuery(FinanceAccountGetAllInput input)
        {
            return Repository.GetAll().Where(model=>model.UserId==AbpSession.UserId.Value);
        }

        public FinanceAccountDto GetDefaultFinanceAccount()
        {
            FinanceAccount financeAccount = FinanceAccountManager.GetDefaultFinanceAccount(AbpSession.UserId.Value);
            return financeAccount.MapTo<FinanceAccountDto>();
        }

        public FinanceAccountDto SetAsDefault(EntityDto input)
        {
            FinanceAccount financeAccount = FinanceAccountManager.SetAsDefaultFinanceAccount(input.Id, AbpSession.UserId.Value);
            return financeAccount.MapTo<FinanceAccountDto>();
        }

        public FinanceAccountForCreateOrEditDto GetFinanceAccountForCreateOrEdit(FinanceAccountForCreateOrEditDto input)
        {
            FinanceAccountForCreateOrEditDto financeAccount;

            if (input.Id.HasValue)
            {
                financeAccount = Repository.Get(input.Id.Value).MapTo<FinanceAccountForCreateOrEditDto>();
            }
            else
            {
                financeAccount = new FinanceAccountForCreateOrEditDto();
            }
            return financeAccount;
        }

        public FinanceAccountDto CreateOrEdit(FinanceAccountForCreateOrEditDto input)
        {
            if (input.Id.HasValue)
            {
                CheckUpdatePermission();

                var entity = GetEntityById(input.Id.Value);
                ObjectMapper.Map(input, entity);

                FinanceAccount defaultFinanceAccount = FinanceAccountManager.GetDefaultFinanceAccount(AbpSession.UserId.Value);

                if (defaultFinanceAccount == null)
                {
                    entity.IsDefault = true;
                }
                CurrentUnitOfWork.SaveChanges();
                return MapToEntityDto(entity);
            }
            else
            {
                CheckCreatePermission();
                var entity = input.MapTo<FinanceAccount>();
                entity.UserId = AbpSession.UserId.Value;
                FinanceAccount defaultFinanceAccount = FinanceAccountManager.GetDefaultFinanceAccount(AbpSession.UserId.Value);

                if (defaultFinanceAccount == null)
                {
                    entity.IsDefault = true;
                }
                Repository.Insert(entity);
                CurrentUnitOfWork.SaveChanges();
                return MapToEntityDto(entity);
            }
        }
    }
}
