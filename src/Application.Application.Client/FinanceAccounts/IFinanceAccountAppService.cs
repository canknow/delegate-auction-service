﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Client.FinanceAccounts.Dtos;

namespace Application.Client.FinanceAccounts
{
    public interface IFinanceAccountAppService : ICrudAppService<FinanceAccountDto,int, FinanceAccountGetAllInput>
    {
        FinanceAccountDto SetAsDefault(EntityDto input);
        FinanceAccountDto GetDefaultFinanceAccount();
        FinanceAccountForCreateOrEditDto GetFinanceAccountForCreateOrEdit(FinanceAccountForCreateOrEditDto input);
        FinanceAccountDto CreateOrEdit(FinanceAccountForCreateOrEditDto input);
    }
}
