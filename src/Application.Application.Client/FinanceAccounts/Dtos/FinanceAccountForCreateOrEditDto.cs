﻿using Application.FinanceAccounts.Entities;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Client.FinanceAccounts.Dtos
{
    [AutoMap(typeof(FinanceAccount))]
    public class FinanceAccountForCreateOrEditDto:NullableIdDto
    {
        public string Account { get; set; }
        public bool IsDefault { get; set; }
        public string FullName { get; set; }
        public int? BankId { get; set; }
        public FinanceAccountType Type { get; set; }
        public FinanceAccountForCreateOrEditDto()
        {
            Type = FinanceAccountType.Alipay;
        }
    }
}
