﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Client.Banks.Dto;
using Application.FinanceAccounts.Entities;

namespace Application.Client.FinanceAccounts.Dtos
{
    [AutoMap(typeof(FinanceAccount))]
    public class FinanceAccountDto : EntityDto
    {
        public string Account { get; set; }
        public bool IsDefault { get; set; }
        public string FullName { get; set; }
        public int? BankId { get; set; }
        public BankDto Bank { get; set; }
        public FinanceAccountType Type { get; set; }
    }
}
