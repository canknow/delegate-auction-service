﻿using Abp.UI;
using Application.Configuration;
using Application.MultiTenancy;
using Application.Wechat;
using Senparc.Weixin.MP.Helpers;
using System;
using System.Threading.Tasks;

namespace Application.Client.Wechat
{
    public class WechatAuthorAppService : ApplicationAppServiceBase, IWechatAuthorAppService
    {
        public WechatUserManager WeixinUserManager { get; set; }
        public TenantHelper TenantHelper { get; set; }

        public async Task<JsSdkUiPackage> GetJsTicketParameters(GetJsSdkUiPackageInput getJsSdkUiPackageInput)
        {
            try
            {
                Tenant tenant = TenantHelper.GetTenant();
                string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, tenant.Id);
                string appSecret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, tenant.Id);
                JsSdkUiPackage jssdkUiPackage = JSSDKHelper.GetJsSdkUiPackage(appId, appSecret, getJsSdkUiPackageInput.Url);
                return jssdkUiPackage;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}
