﻿using Abp.Application.Services;
using Senparc.Weixin.MP.Helpers;
using System.Threading.Tasks;

namespace Application.Client.Wechat
{
    public interface IWechatAuthorAppService : IApplicationService
    {
        Task<JsSdkUiPackage> GetJsTicketParameters(GetJsSdkUiPackageInput getJsSdkUiPackageInput);
    }
}
