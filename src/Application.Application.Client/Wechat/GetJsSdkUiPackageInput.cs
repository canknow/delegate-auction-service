﻿namespace Application.Client.Wechat
{
    public class GetJsSdkUiPackageInput
    {
        public string Url { get; set; }
    }
}
