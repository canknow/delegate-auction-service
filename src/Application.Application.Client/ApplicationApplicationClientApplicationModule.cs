﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Application.Client
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(ApplicationCoreModule))]
    public class ApplicationApplicationClientApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationApplicationClientApplicationModule).GetAssembly());
        }
    }
}