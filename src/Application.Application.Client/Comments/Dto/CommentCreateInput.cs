﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Comments;

namespace Application.Client.Comments.Dto
{
    [AutoMapTo(typeof(Comment))]
    public class CommentCreateInput: EntityDto
    {
        public int OrderId { get; set; }


        public string Content { get; set; }

        public int Rate { get; set; }

        public string Avatar { get; set; }

        public string NickName { get; set; }
    }
}
