﻿using Abp.AutoMapper;
using Application.Authorization.Users;

namespace Application.Client.Comments.Dto
{
    [AutoMap(typeof(User))]
    public class UserDto
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string NickName { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string Avatar { get; set; }

        public string PhoneNumber { get; set; }
    }
}
