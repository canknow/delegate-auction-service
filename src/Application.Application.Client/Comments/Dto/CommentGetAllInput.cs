﻿using Abp.Application.Services.Dto;

namespace Application.Client.Comments.Dto
{
    public class CommentGetAllInput : PagedAndSortedResultRequestDto
    {
        public int? TeamId { get; set; }

        public int? PackageId { get; set; }
    }
}
