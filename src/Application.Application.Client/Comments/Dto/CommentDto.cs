﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Comments;

namespace Application.Client.Comments.Dto
{
    [AutoMap(typeof(Comment))]
    public class CommentDto : FullAuditedEntityDto
    {
        public string Content { get; set; }

        public int? TeamId { get; set; }

        public int? PackageId { get; set; }

        public int Rate { get; set; }

        public string TeamName { get; set; }

        public string PackageName { get; set; }

        public UserDto CreatorUser { get; set; }

        public string Avatar { get; set; }

        public string NickName { get; set; }
    }
}
