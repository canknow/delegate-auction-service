﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Client.Comments.Dto;
using Application.Comments;
using System.Linq;

namespace Application.Client.Comments
{
    public class CommentAppService : CrudAppService<Comment, CommentDto, int, CommentGetAllInput, CommentCreateInput>,
        ICommentAppService
    {
        public CommentAppService(IRepository<Comment> respository)
            : base(respository)
        {
        }


        protected override IQueryable<Comment> CreateFilteredQuery(CommentGetAllInput input)
        {
            return Repository.GetAll().WhereIf(input.TeamId.HasValue, model => model.TeamId == input.TeamId.Value)
                .WhereIf(input.PackageId.HasValue, model => model.PackageId == input.PackageId.Value);
        }
    }
}
