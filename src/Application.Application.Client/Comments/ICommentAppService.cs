﻿using Abp.Application.Services;
using Application.Client.Comments.Dto;

namespace Application.Client.Comments
{
    public interface ICommentAppService : ICrudAppService<CommentDto, int, CommentGetAllInput, CommentCreateInput>
    {
    }
}
