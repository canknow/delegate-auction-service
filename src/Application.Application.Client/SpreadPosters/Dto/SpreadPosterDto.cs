﻿namespace Application.Client.SpreadPosters.Dto
{
    public class SpreadPosterDto
    {
        public SpreadPosterTemplateDto SpreadPosterTemplate { get; set; }
        public string Path { get; set; }
    }
}
