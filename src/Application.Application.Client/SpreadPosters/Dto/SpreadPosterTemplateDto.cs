﻿using Application.SpreadPosters.SpreadPosterTemplates;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Client.SpreadPosters.Dto
{
    [AutoMap(typeof(SpreadPosterTemplate))]
    public class SpreadPosterTemplateDto:EntityDto
    {
        public string Name { get; set; }

        public string Template { get; set; }

        public bool IsDefault { get; set; }
    }
}
