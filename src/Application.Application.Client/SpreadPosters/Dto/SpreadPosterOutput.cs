﻿using System.Collections.Generic;

namespace Application.Client.SpreadPosters.Dto
{
    public class SpreadPosterOutput
    {
        public List<SpreadPosterDto> SpreadPosters { get; set; }
    }
}
