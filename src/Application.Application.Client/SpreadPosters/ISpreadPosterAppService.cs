﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Client.SpreadPosters.Dto;
using System.Threading.Tasks;

namespace Application.Client.SpreadPosters
{
    public interface ISpreadPosterAppService:IApplicationService
    {
        Task<QrcodeDto> GetQrcodeAsync();

        Task<SpreadPosterOutput> GetSpreadPosters();

        Task<string> GetDefaultSpreadPoster();

        Task<string> ReCreateDefaultSpreadPoster();

        Task<string> CreateSpreadPoster(EntityDto input);
    }
}
