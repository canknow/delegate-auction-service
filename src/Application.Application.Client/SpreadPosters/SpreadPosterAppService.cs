using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Application.Client.SpreadPosters.Dto;
using Application.Spread;
using Application.SpreadPosters;
using Application.SpreadPosters.SpreadPosterTemplates;
using Application.Wechat.Qrcodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Client.SpreadPosters
{
    [AbpAuthorize]
    public class SpreadPosterAppService: ApplicationAppServiceBase, ISpreadPosterAppService
    {
        private IRepository<SpreadPosterTemplate> SpreadPosterTemplateRespository;
        public SpreadPosterManager SpreadPosterManager { get; set; }
        public SpreadManager SpreadManager { get; set; }
        public QrcodeManager QrcodeManager { get; set; }

        public SpreadPosterAppService(IRepository<SpreadPosterTemplate> spreadPosterTemplateRespository)
        {
            SpreadPosterTemplateRespository = spreadPosterTemplateRespository;
        }


        public async Task<QrcodeDto> GetQrcodeAsync()
        {
            try
            {
                UserIdentifier userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
                Qrcode qrcode = await QrcodeManager.GetQrcodeAsync(userIdentifier);
                return qrcode.MapTo<QrcodeDto>();
            }
            catch(Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<SpreadPosterOutput> GetSpreadPosters()
        {
            try
            {
                await SpreadManager.CanGetSpreadPoster(AbpSession.ToUserIdentifier());
            }
            catch(Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
            List<SpreadPosterTemplate> spreadPosterTemplates = SpreadPosterTemplateRespository.GetAll().Where(model=>model.Status).ToList();
            List<SpreadPosterDto> spreadPosterDtos = new List<SpreadPosterDto>();
            SpreadPosterOutput spreadPosterOutput = new SpreadPosterOutput();

            foreach (SpreadPosterTemplate spreadPosterTemplate in spreadPosterTemplates)
            {
                SpreadPosterDto spreadPosterDto = new SpreadPosterDto();
                spreadPosterDto.SpreadPosterTemplate = spreadPosterTemplate.MapTo<SpreadPosterTemplateDto>();
                spreadPosterDto.Path =await SpreadPosterManager.GetSpreadPosterAsync(spreadPosterTemplate.Id, AbpSession.ToUserIdentifier());
                spreadPosterDtos.Add(spreadPosterDto);
            }
            spreadPosterOutput.SpreadPosters = spreadPosterDtos;
            return spreadPosterOutput;
        }

        public async Task<string> GetDefaultSpreadPoster()
        {
            try
            {
                await SpreadManager.CanGetSpreadPoster(AbpSession.ToUserIdentifier());
                UserIdentifier userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
                string path = await SpreadPosterManager.GetDefaultSpreadPosterAsync(userIdentifier);
                return path;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<string> ReCreateDefaultSpreadPoster()
        {
            try
            {
                UserIdentifier userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
                return await SpreadPosterManager.ReCreateDefaultSpreadPosterAsync(userIdentifier);
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<string> CreateSpreadPoster(EntityDto input)
        {
            try
            {
                UserIdentifier userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.UserId.Value);
                return await SpreadPosterManager.CreateSpreadPosterAsync(input.Id, userIdentifier);
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }
    }
}
