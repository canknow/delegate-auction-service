﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Application.Bids;
using Application.Client.Bids.Dto;
using System.Linq;

namespace Application.Client.Bids
{
    public class BidAppService : CrudAppService<Bid, BidDto, int, BidGetAllInput>, IBidAppService
    {
        public BidAppService(IRepository<Bid> respository) : base(respository)
        {

        }

        protected override IQueryable<Bid> CreateFilteredQuery(BidGetAllInput input)
        {
            return Repository.GetAll();
        }

        public BidDto GetMyBid()
        {
            var bid = Repository.FirstOrDefault(model => model.UserId == AbpSession.UserId.Value);
            if (bid==null)
            {
                return null;
            }
            return bid.MapTo<BidDto>();
        }
    }
}
