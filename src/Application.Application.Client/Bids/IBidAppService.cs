﻿using Abp.Application.Services;
using Application.Client.Bids.Dto;

namespace Application.Client.Bids
{
    public interface IBidAppService : ICrudAppService<BidDto, int, BidGetAllInput>
    {
        BidDto GetMyBid();
    }
}