﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Authorization.Users;

namespace Application.Client.Session.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserLoginInfoDto : EntityDto<long>
    {
        public int? TenantId { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string NickName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string Avatar { get; set; }

        public string Cover { get; set; }
    }
}
