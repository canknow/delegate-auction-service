﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.MultiTenancy;

namespace Application.Client.Session.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }

        public string Domain { get; set; }
    }
}