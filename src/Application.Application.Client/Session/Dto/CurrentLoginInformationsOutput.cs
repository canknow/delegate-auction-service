﻿namespace Application.Client.Session.Dto
{
    public class CurrentLoginInformationsOutput
    {
        public UserLoginInfoDto User { get; set; }
    }
}