﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Application.Authorization;
using Application.Authorization.Users;
using Application.Client.Session.Dto;
using Application.MultiTenancy;
using System.Threading.Tasks;

namespace Application.Client.Sessions
{
    [AbpAuthorize]
    public class SessionAppService : ApplicationAppServiceBase, ISessionAppService
    {
        public ApplicationAuthorizationHelper ApplicationAuthorizationHelper { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        public TenantHelper TenantHelper { get; set; }

        public async Task<CurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            User user = null;
            var output = new CurrentLoginInformationsOutput
            {
            };

            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                user = await UserRepository.GetAsync(AbpSession.UserId.Value);
                output.User = ApplicationAuthorizationHelper.CurrentUser.MapTo<UserLoginInfoDto>();
            }
            return output;
        }
    }
}
