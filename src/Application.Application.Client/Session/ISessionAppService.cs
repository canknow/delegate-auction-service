﻿using Abp.Application.Services;
using Application.Client.Session.Dto;
using System.Threading.Tasks;

namespace Application.Client.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<CurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
