﻿using Application.Client.Orders.Dto;

namespace Application.Client.Pay.Dto
{
    public class PayOutput
    {
        public OrderDto Order { get; set; }
    }
}
