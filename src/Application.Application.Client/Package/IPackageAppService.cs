﻿using Abp.Application.Services;
using Application.Client.Packages.Dto;

namespace Application.Client.Packages
{
    public interface IPackageAppService : ICrudAppService<PackageDto, int, PackageGetAllInput>
    {
    }
}