﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Client.Packages.Dto;
using Application.Packages;
using System.Linq;

namespace Application.Client.Packages
{
    public class PackageAppService : CrudAppService<Package, PackageDto, int, PackageGetAllInput>, IPackageAppService
    {
        public PackageAppService(IRepository<Package> respository) : base(respository)
        {

        }

        protected override IQueryable<Package> CreateFilteredQuery(PackageGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Name), model => model.Name == input.Name)
                .WhereIf(input.TeamId.HasValue, model => model.TeamId == input.TeamId.Value)
                .WhereIf(input.IsHot.HasValue, model=>model.IsHot ==input.IsHot.Value);
        }
    }
}
