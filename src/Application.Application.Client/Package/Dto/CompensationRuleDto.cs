﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Packages;

namespace Application.Client.Packages.Dto
{
    [AutoMap(typeof(CompensationRule))]
    public class CompensationRuleDto: EntityDto
    {
        public int Count { get; set; }

        public int Money { get; set; }
    }
}
