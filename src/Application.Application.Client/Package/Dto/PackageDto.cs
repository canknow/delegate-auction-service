﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Packages;
using System.Collections.Generic;

namespace Application.Client.Packages.Dto
{
    [AutoMap(typeof(Package))]
    public class PackageDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string Cover { get; set; }

        public PackageType PackageType { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public float Score { get; set; }

        public int HintCount { get; set; }

        public int OrderCount { get; set; }

        public List<CompensationRuleDto> CompensationRules { get; set; }

        public List<DistributionDto> Distributions { get; set; }


        public bool IsHot { get; set; }
    }
}
