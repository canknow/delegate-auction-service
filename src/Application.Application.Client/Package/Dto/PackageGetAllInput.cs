﻿using Abp.Application.Services.Dto;

namespace Application.Client.Packages.Dto
{
    public class PackageGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }

        public bool? IsHot { get; set; }

        public int? TeamId { get; set; }
    }
}
