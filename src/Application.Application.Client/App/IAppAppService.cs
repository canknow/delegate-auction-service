﻿using Abp.Application.Services;
using Application.Client.App.Dto;
using System.Threading.Tasks;

namespace Application.Client.App
{
    public interface IAppAppService : IApplicationService
    {
        Task<AppInfo> GetAppInfo();
    }
}
