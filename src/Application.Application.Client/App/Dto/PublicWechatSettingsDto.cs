﻿using Abp.AutoMapper;
using Application.Wechat.PublicWechats;
using System.ComponentModel.DataAnnotations;

namespace Application.Client.App.Dto
{
    [AutoMap(typeof(General))]
    public class PublicWechatSettingsDto
    {
        public string AppId { get; set; }

        [MaxLength(128)]
        public string SubscribeLink { get; set; }
    }
}
