﻿namespace Application.Client.App.Dto
{
    public class AppInfo
    {
        public PublicWechatSettingsDto PublicWechatSettings { get; set; }
    }
}
