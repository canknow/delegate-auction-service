﻿using Abp.Domain.Repositories;
using Application.Authorization;
using Application.Authorization.Users;
using Application.Client.App.Dto;
using Application.Configuration;
using Application.MultiTenancy;
using System.Threading.Tasks;

namespace Application.Client.App
{
    public class AppAppService : ApplicationAppServiceBase, IAppAppService
    {
        public ApplicationAuthorizationHelper ApplicationAuthorizationHelper { get; set; }
        public IRepository<User, long> UserRepository { get; set; }
        public TenantHelper TenantHelper { get; set; }

        public async Task<AppInfo> GetAppInfo()
        {
            PublicWechatSettingsDto publicWechatSettings = new PublicWechatSettingsDto();
            publicWechatSettings.AppId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, AbpSession.TenantId.Value);
            publicWechatSettings.SubscribeLink = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.SubscribeLink, AbpSession.TenantId.Value);
            var output = new AppInfo
            {
                PublicWechatSettings = publicWechatSettings
            };
            return output;
        }
    }
}
