﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.AuctionRecords;
using Application.Client.Packages.Dto;
using Application.Client.Teams.Dto;
using System;
using static Application.AuctionRecords.AuctionRecord;

namespace Application.Client.AuctionRecords.Dto
{
    [AutoMap(typeof(AuctionRecord))]
    public class AuctionRecordDto : FullAuditedEntityDto
    {
        public AuctionRecordBid Bid { get; set; }

        public string Shot { get; set; }

        public UserDto User { get; set; }

        public int AttemptCount { get; set; }

        public DateTime? WinDateTime { get; set; }

        public int? PackageId { get; set; }

        public PackageDto Package { get; set; }

        public int? TeamId { get; set; }

        public TeamDto Team { get; set; }
    }
}
