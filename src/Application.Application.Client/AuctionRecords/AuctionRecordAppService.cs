﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.AuctionRecords;
using Application.Client.AuctionRecords.Dto;
using System.Linq;

namespace Application.Client.AuctionRecords
{
    public class AuctionRecordAppService : CrudAppService<AuctionRecord, AuctionRecordDto, int, AuctionRecordGetAllInput>, IAuctionRecordAppService
    {
        public AuctionRecordAppService(IRepository<AuctionRecord> respository) : base(respository)
        {

        }

        protected override IQueryable<AuctionRecord> CreateFilteredQuery(AuctionRecordGetAllInput input)
        {
            return Repository.GetAll();
        }
    }
}
