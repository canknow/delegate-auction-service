﻿using Abp.Application.Services;
using Application.Client.AuctionRecords.Dto;

namespace Application.Client.AuctionRecords
{
    public interface IAuctionRecordAppService : ICrudAppService<AuctionRecordDto, int, AuctionRecordGetAllInput>
    {
    }
}