﻿namespace Application.Client.Authorization.Users.Profile.Dto
{
    public class UpdateNickNameInput
    {
        public string NickName { get; set; }
    }
}
