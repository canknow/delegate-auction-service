﻿using Abp.Authorization.Users;
using System.ComponentModel.DataAnnotations;

namespace Application.Client.Authorization.Users.Profile.Dto
{
    public class UpdateFullNameInput
    {
        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }
    }
}
