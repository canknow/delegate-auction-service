﻿using System.ComponentModel.DataAnnotations;

namespace Application.Client.Authorization.Users.Profile.Dto
{
    public class UpdateCoverInput
    {
        [Required]
        public string Cover { get; set; }
    }
}
