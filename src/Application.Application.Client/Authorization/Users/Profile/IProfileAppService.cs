﻿using Abp.Application.Services;
using Application.Authorization.Users.Profile.Dto;
using Application.Client.Authorization.Users.Profile.Dto;
using System.Threading.Tasks;

namespace Application.Client.Authorization.Users.Profile
{
    public interface IProfileAppService : IApplicationService
    {
        Task<CurrentUserProfileEditDto> GetCurrentUserProfileForEdit();

        Task UpdateCurrentUserProfile(CurrentUserProfileEditDto input);
        
        Task ChangePassword(ChangePasswordInput input);

        Task UpdateProfilePicture(UpdateProfilePictureInput input);

        Task UpdateNickName(UpdateNickNameInput input);

        Task UpdateFullName(UpdateFullNameInput input);

        Task UpdateCover(UpdateCoverInput input);

        Task UpdateAvatar(UpdateAvatarInput input);

        Task<GetPasswordComplexitySettingOutput> GetPasswordComplexitySetting();

        Task SendVerificationSms();

        Task VerifySmsCode(VerifySmsCodeInputDto input);
    }
}
