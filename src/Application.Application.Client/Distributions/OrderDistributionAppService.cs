﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Application.Client.Distributions.Dto;
using Application.Distributions;
using System.Linq;

namespace Application.Client.OrderDistributions
{
    [AbpAuthorize]
    public class OrderDistributionAppService : CrudAppService<OrderDistribution, OrderDistributionDto>, IOrderDistributionAppService
    {
        public OrderDistributionAppService(IRepository<OrderDistribution> respository) :base(respository)
        {

        }

        protected override IQueryable<OrderDistribution> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return Repository.GetAll().Where(model => model.UserId == AbpSession.UserId.Value && model.Order.IsDeleted == false);
        }
    }
}
