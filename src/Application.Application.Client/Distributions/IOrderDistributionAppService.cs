﻿using Abp.Application.Services;
using Application.Client.Distributions.Dto;

namespace Application.Client.OrderDistributions
{
    public interface IOrderDistributionAppService : ICrudAppService<OrderDistributionDto>
    {
    }
}
