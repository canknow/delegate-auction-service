﻿using Abp.AutoMapper;
using Application.Distributions;

namespace Application.Client.Distributions.Dto
{
    [AutoMap(typeof(Distribution))]
    public class DistributionDto
    {
        public int ProductId { get; set; }

        public BuyWhen BuyWhen { get; set; }

        public int Level { get; set; }

        public decimal Money { get; set; }

        public float Ratio { get; set; }
    }
}