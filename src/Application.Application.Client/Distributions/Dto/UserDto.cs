﻿using Abp.AutoMapper;
using Application.Authorization.Users;
using System;

namespace Application.Client.Distributions.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto
    {
        public string Number { get; set; }

        public string NickName { get; set; }

        public string Avatar { get; set; }

        public UserSource Source { get; set; }

        public DateTime CreationTime { get; set; }
    }
}