﻿using Abp.AutoMapper;
using Application.Orders;

namespace Application.Client.Distributions.Dto
{
    [AutoMapFrom(typeof(Order))]
    public class OrderDistributionOrderDto
    {
        public string Title { get; set; }

        public decimal Money { get; set; }

        public UserDto User { get; set; }
    }
}
