﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Application.Configuration;
using Application.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Application.Jobs
{
    [DependsOn(
         typeof(ApplicationEntityFrameworkCoreModule))]
    public class ApplicaitonJobsModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ApplicaitonJobsModule(ApplicationEntityFrameworkCoreModule applicationEntityFrameworkCoreModule)
        {
            applicationEntityFrameworkCoreModule.SkipDbSeed = true;
            _appConfiguration = AppConfigurations.Get(typeof(ApplicaitonJobsModule).GetAssembly().GetDirectoryPathOrNull());
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                ApplicationConsts.ConnectionStringName
            );
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicaitonJobsModule).GetAssembly());
        }
    }
}
