﻿using Abp;
using Castle.Facilities.Logging;
using System;

namespace Application.Jobs
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bootstrapper = AbpBootstrapper.Create<ApplicaitonJobsModule>())
            {
                bootstrapper.Initialize();

                Console.WriteLine("Press enter to exit...");
                Console.ReadLine();
            }
        }
    }
}
