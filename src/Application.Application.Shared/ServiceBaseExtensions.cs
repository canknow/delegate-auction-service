﻿using Abp;
using Abp.UI;

namespace Application
{
    public static class AbpServiceBaseExtensions
    {
        public static void ThrowUserFriendlyDomainException (this AbpServiceBase serviceBase, string detail)
        {
            throw new UserFriendlyException("DomainException", detail);
        }
    }
}
