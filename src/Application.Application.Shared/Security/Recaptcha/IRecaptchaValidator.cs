using System.Threading.Tasks;
using Application.Captchas;

namespace Application.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task Send(string provider, string providerKey, string code);

        Task ReSend(Captcha captcha);

        Task ValidateAsync(string provider, string providerKey, string code);
    }
}