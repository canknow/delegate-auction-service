using System.Threading.Tasks;
using Application.Captchas;

namespace Application.Security.Recaptcha
{
    public class NullRecaptchaValidator : IRecaptchaValidator
    {
        public static NullRecaptchaValidator Instance { get; } = new NullRecaptchaValidator();

        public Task ReSend(Captcha captcha)
        {
            throw new System.NotImplementedException();
        }

        public Task Send(string provider, string providerKey, string code)
        {
            return Task.CompletedTask;
        }

        public Task ValidateAsync(string provider, string providerKey, string code)
        {
            return Task.CompletedTask;
        }
    }
}