﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Application
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(ApplicationCoreModule))]
    public class ApplicationApplicationSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationApplicationSharedModule).GetAssembly());
        }
    }
}