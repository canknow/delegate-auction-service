﻿using Abp.Notifications;
using Application.Dto;

namespace Application.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}