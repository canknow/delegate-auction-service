namespace Application.Configuration.Dto
{
    public class HostBillingSettingsEditDto
    {
        public string LegalName { get; set; }

        public string Address { get; set; }
    }
}