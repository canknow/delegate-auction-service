using System.ComponentModel.DataAnnotations;

namespace Application.Authorization.Users.Profile.Dto
{
    public class UpdateAvatarInput
    {
        [Required]
        [MaxLength(400)]
        public string FilePath { get; set; }
    }
}