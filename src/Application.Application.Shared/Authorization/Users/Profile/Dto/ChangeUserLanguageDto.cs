﻿using System.ComponentModel.DataAnnotations;

namespace Application.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
