﻿using Abp.AutoMapper;
using Application.Authorization.Users;
using Application.Authorization.Users.Dtos;

namespace Application.Public.Authorization.Users.Dto
{
    [AutoMap(typeof(User))]

    public class UserProfileDto : UserBaseDto
    {
    }
}