﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;

namespace Application.Authorization.Users.Dtos
{
    public class UserBaseDto : EntityDto<long>, IHasCreationTime
    {
        public int? TenantId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string NickName { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string Avatar { get; set; }

        public string Cover { get; set; }

        public string PhoneNumber { get; set; }

        public Guid? ProfilePictureId { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public DateTime CreationTime { get; set; }

        public string Say { get; set; }
    }
}
