﻿namespace Application.Dto
{
    public class IdInput: IdInput<int>
    {
    }

    public class IdInput<TEntityId>
    {
        public TEntityId Id { get; set; }
    }
}
