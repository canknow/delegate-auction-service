﻿using Abp.EntityFrameworkCore;
using Application.Packages;
using Microsoft.EntityFrameworkCore;

namespace Application.EntityFrameworkCore.Repositories
{
    public class PackageRepository: ApplicationRepositoryBase<Package>, IPackageRepository
    {
        public PackageRepository(IDbContextProvider<ApplicationDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public DbContext GetContext()
        {
            return Context;
        }
    }
}
