using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Application.EntityFrameworkCore
{
    public static class ApplicationDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ApplicationDbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseLazyLoadingProxies().UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ApplicationDbContext> builder, DbConnection connection)
        {
            //builder.UseSqlServer(connection);
            builder.UseLazyLoadingProxies().UseMySql(connection);
        }
    }
}
