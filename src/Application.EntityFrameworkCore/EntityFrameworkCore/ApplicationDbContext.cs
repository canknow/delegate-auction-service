﻿using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Application.Articles;
using Application.Authorization.Roles;
using Application.Authorization.Users;
using Application.Editions;
using Application.Files;
using Application.MultiTenancy;
using Application.MultiTenancy.Accounting;
using Application.MultiTenancy.Payments;
using Application.Packages;
using Application.Storage;
using Application.Teams;
using Microsoft.EntityFrameworkCore;
using Application.Orders;
using Application.Bids;
using Application.Captchas;
using Application.AuctionRecords;
using Application.ShopTemplates;
using Application.Shares;
using Application.Shops;
using Application.Wechat;
using Application.Wechat.PublicWechats;
using Application.Wechat.Qrcodes;
using Application.SpreadPosters.SpreadPosterTemplates;
using Application.Wechat.AutoReplys;
using Application.FinanceAccounts.Entities;
using Application.Banks;
using Application.Wallets;
using Application.Distributions;
using Application.PictureTemplates;
using Application.QuestionAndAnswers;
using Application.Comments;
using Application.AuctionAttempts;
using Application.Notices;
using Application.BusinessNotifiers;

namespace Application.EntityFrameworkCore
{
    public class ApplicationDbContext : AbpZeroDbContext<Tenant, Role, User, ApplicationDbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<AbpFileInfo> AbpFileInfos { get; set; }

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<Captcha> Captchas { get; set; }

        public virtual DbSet<BusinessNotifier> BusinessNotifiers { get; set; }

        public virtual DbSet<Article> Articles { get; set; }

        public virtual DbSet<ArticleLike> ArticleLikes { get; set; }

        public virtual DbSet<ArticleHint> ArticleHints { get; set; }

        public virtual DbSet<ArticleCategory> ArticleCategorys { get; set; }

        public virtual DbSet<Team> Teams { get; set; }

        public virtual DbSet<TeamHintRecord> TeamHintRecords { get; set; }

        public virtual DbSet<Package> Packages { get; set; }

        public virtual DbSet<CompensationRule> CompensationRules { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<AuctionRecord> AuctionRecords { get; set; }

        public virtual DbSet<Bid> Bids { get; set; }

        public virtual DbSet<ShopTemplate> ShopTemplates { get; set; }

        public virtual DbSet<Share> Shares { get; set; }

        public virtual DbSet<ShareAccess> ShareAccesses { get; set; }

        public virtual DbSet<PublicWechat> PublicWechats { get; set; }

        public virtual DbSet<Qrcode> Qrcodes { get; set; }

        public virtual DbSet<SpreadPosterTemplate> SpreadPosterTemplates { get; set; }

        public virtual DbSet<TemplateParameter> TemplateParameters { get; set; }

        public virtual DbSet<AutoReply> AutoReplys { get; set; }

        public virtual DbSet<FinanceAccount> FinanceAccounts { get; set; }

        public virtual DbSet<Bank> Banks { get; set; }

        public virtual DbSet<Wallet> Wallets { get; set; }

        public virtual DbSet<WalletRecord> WalletRecords { get; set; }

        public virtual DbSet<WithdrawApply> WithdrawApplys { get; set; }

        public virtual DbSet<Distribution> Distributions { get; set; }

        public virtual DbSet<OrderDistribution> OrderDistributions { get; set; }

        public virtual DbSet<PictureTemplate> PictureTemplates { get; set; }

        public virtual DbSet<AutoReplyArticle> AutoReplyArticles { get; set; }

        public virtual DbSet<QuestionAndAnswer> QuestionAndAnswers { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }

        public virtual DbSet<Shop> Shops { get; set; }

        public virtual DbSet<AuctionAttempt> AuctionAttempts { get; set; }

        public virtual DbSet<Notice> Notices { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { e.PaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}