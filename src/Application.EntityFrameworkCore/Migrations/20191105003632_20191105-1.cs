﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201911051 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Avatar",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NickName",
                table: "Comments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Avatar",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "NickName",
                table: "Comments");
        }
    }
}
