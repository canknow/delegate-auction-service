﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201911041 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WalletRecords_WithdrawApplys_WithdrawApplyId",
                table: "WalletRecords");

            migrationBuilder.AlterColumn<int>(
                name: "WithdrawApplyId",
                table: "WalletRecords",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_WalletRecords_WithdrawApplys_WithdrawApplyId",
                table: "WalletRecords",
                column: "WithdrawApplyId",
                principalTable: "WithdrawApplys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WalletRecords_WithdrawApplys_WithdrawApplyId",
                table: "WalletRecords");

            migrationBuilder.AlterColumn<int>(
                name: "WithdrawApplyId",
                table: "WalletRecords",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WalletRecords_WithdrawApplys_WithdrawApplyId",
                table: "WalletRecords",
                column: "WithdrawApplyId",
                principalTable: "WithdrawApplys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
