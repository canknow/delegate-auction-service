﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201911024 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bid_Account",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "Bid_FullName",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "Bid_IDNumber",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "Bid_Password",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "Bid_PhoneNumber",
                table: "AuctionAttempts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Bid_Account",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bid_FullName",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bid_IDNumber",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bid_Password",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bid_PhoneNumber",
                table: "AuctionAttempts",
                nullable: true);
        }
    }
}
