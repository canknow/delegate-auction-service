﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201911022 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuctionAttemptBid_Account",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "AuctionAttemptBid_FullName",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "AuctionAttemptBid_IDNumber",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "AuctionAttemptBid_Password",
                table: "AuctionAttempts");

            migrationBuilder.DropColumn(
                name: "AuctionAttemptBid_PhoneNumber",
                table: "AuctionAttempts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AuctionAttemptBid_Account",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuctionAttemptBid_FullName",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuctionAttemptBid_IDNumber",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuctionAttemptBid_Password",
                table: "AuctionAttempts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuctionAttemptBid_PhoneNumber",
                table: "AuctionAttempts",
                nullable: true);
        }
    }
}
