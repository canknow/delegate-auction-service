﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201912024 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Seal",
                table: "Shops",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WechatQrcode",
                table: "Shops",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Seal",
                table: "Shops");

            migrationBuilder.DropColumn(
                name: "WechatQrcode",
                table: "Shops");
        }
    }
}
