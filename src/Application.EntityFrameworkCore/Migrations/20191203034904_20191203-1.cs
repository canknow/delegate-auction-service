﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201912031 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompensationStatus",
                table: "Orders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompensationStatus",
                table: "Orders");
        }
    }
}
