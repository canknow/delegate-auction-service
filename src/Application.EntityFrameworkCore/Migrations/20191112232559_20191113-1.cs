﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Application.Migrations
{
    public partial class _201911131 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderCustomInfo_FullName",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderCustomInfo_PhoneNumber",
                table: "Orders");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Shops",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                table: "Shops");

            migrationBuilder.AddColumn<string>(
                name: "OrderCustomInfo_FullName",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrderCustomInfo_PhoneNumber",
                table: "Orders",
                nullable: true);
        }
    }
}
