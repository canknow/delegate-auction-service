﻿namespace Infrastructure.Security.OAuth
{
    public static class ApplicationClaimTypes
    {
        public const string NickName = "urn:application:nickname";
        public const string City = "urn:application:city";
        public const string HeadImgUrl = "urn:application:headimgurl";
        public const string OpenId = "urn:application:openid";
        public const string Privilege = "urn:application:privilege";
        public const string Province = "urn:application:province";
    }
}
