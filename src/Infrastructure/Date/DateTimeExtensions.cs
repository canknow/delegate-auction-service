﻿using System;

namespace Infrastructure.Date
{
    public static class DateTimeExtensions
    {
        public static string ToLocalString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToLocalString(this DateTime? dateTime)
        {
            if(dateTime.HasValue)
            {
                return dateTime.Value.ToLocalString();
            }
            return "";
        }
    }
}
