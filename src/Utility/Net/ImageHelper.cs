﻿using SixLabors.ImageSharp;
using System.IO;
using Utility.IO;

namespace Utility.Net
{
    public static class ImageHelper
    {
        public static void GetAndSaveImage(string url, string path)
        {
            Stream responseStream = CommonFileHelper.LoadNetFile(url);
            var imgSrc = Image.Load(responseStream);
            imgSrc.Save(path);
        }
    }
}
