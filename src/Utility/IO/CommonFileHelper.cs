﻿using System.IO;
using System.Net.Http;

namespace Utility.IO
{
    public class CommonFileHelper
    {
        public static Stream LoadNetFile(string url)
        {
            if (url.Contains("//"))
            {
                var httpClient = new HttpClient();
                var t = httpClient.GetByteArrayAsync(url);
                t.Wait();
                Stream responseStream = new MemoryStream(t.Result);
                return responseStream;
            }
            return null;
        }
    }
}
