﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.IO;

namespace Utility.IO
{
    public static class ImageSharpExtension
    {
        public static Image<Rgba32> Load(string url)
        {
            Stream stream = ImageHelper.LoadImageStreamFromNet(url);
            return Image.Load(stream);
        }
    }
}
