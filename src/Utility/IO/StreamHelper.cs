﻿using System.IO;
using System.Text;

namespace Utility.IO
{
    public static class StreamHelper
    {
        public static MemoryStream StringToMemberStream (string input)
        {
            byte[] array = Encoding.ASCII.GetBytes(input);
            MemoryStream memoryStream = new MemoryStream(array);
            return memoryStream;
        }

        public static string StreamToString (Stream stream)
        {
            StreamReader streamReader = new StreamReader(stream);
            return streamReader.ReadToEnd();
        }

        /// 将 Stream 转成 byte[]
        public static byte[] StreamToBytes(Stream stream, int length = 1024)
        {
            byte[] bytes = new byte[length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        }

        public static string MemoryStreamToString (MemoryStream memoryStream)
        {
            byte[] b = memoryStream.ToArray();
            return Encoding.UTF8.GetString(b, 0, b.Length);
        }

        /// 将 byte[] 转成 Stream
        public static Stream BytesToStream(byte[] bytes)
        {
            Stream stream = new MemoryStream(bytes);
            return stream;
        }

        public static MemoryStream StreamToMemoryStream(Stream stream, int bufferLength = 10485760)
        {
            var memoryStream = new MemoryStream();
            byte[] buffer = new byte[bufferLength];
            int actual = stream.Read(buffer, 0, bufferLength);
            if (actual > 0)
            {
                memoryStream.Write(buffer, 0, actual);
            }
            memoryStream.Position = 0;
            return memoryStream;
        }

        //将 Stream 写入文件
        public static void StreamToFile(Stream stream, string fileName)
        {
            // 把 Stream 转换成 byte[]
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            // 把 byte[] 写入文件
            FileStream fileStream = new FileStream(fileName, FileMode.Create);
            BinaryWriter binaryWriter = new BinaryWriter(fileStream);
            binaryWriter.Write(bytes);
            binaryWriter.Close();
            fileStream.Close();
        }

        //从文件读取 Stream
        public static Stream FileToStream(string fileName)
        {
            // 打开文件
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            // 读取文件的 byte[]
            byte[] bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, bytes.Length);
            fileStream.Close();
            // 把 byte[] 转换成 Stream
            Stream stream = new MemoryStream(bytes);
            return stream;
        }
    }
}
