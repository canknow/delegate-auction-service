﻿using System.IO;

namespace Utility.IO
{
    public class ImageHelper
    {
        public static Stream LoadImageStreamFromNet(string url)
        {
            System.Net.WebRequest webRequest = System.Net.WebRequest.Create(url);
            System.Net.WebResponse webResponse = webRequest.GetResponse();
            System.IO.Stream stream = webResponse.GetResponseStream();
            return stream;
        }
    }
}
