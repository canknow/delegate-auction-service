﻿using System;
using System.Runtime.InteropServices;

namespace Utility.Systems
{
    public class SystemInfoHelper
    {
        public static bool IsLinux => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        public static bool IsMono => Type.GetType("Mono.Runtime") != null; // https://stackoverflow.com/a/721194

        public static bool IsWindows => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        public static bool Is64BitProcess => IntPtr.Size == 8;
    }
}
