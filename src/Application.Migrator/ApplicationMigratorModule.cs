using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Application.Configuration;
using Application.EntityFrameworkCore;
using Application.Migrator.DependencyInjection;
using Castle.MicroKernel.Registration;
using Microsoft.Extensions.Configuration;

namespace Application.Migrator
{
    [DependsOn(typeof(ApplicationEntityFrameworkCoreModule))]
    public class ApplicationMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ApplicationMigratorModule(ApplicationEntityFrameworkCoreModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(ApplicationMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                ApplicationConsts.ConnectionStringName
            );
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
