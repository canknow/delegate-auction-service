﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Application.IdentityServer
{
    [DependsOn(typeof(ApplicationWebCoreModule))]
    public class ApplicationIdentityServerModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationIdentityServerModule).GetAssembly());
        }
    }
}
