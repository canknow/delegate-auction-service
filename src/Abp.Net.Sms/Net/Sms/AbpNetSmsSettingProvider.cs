﻿using Abp.Configuration;
using System.Collections.Generic;
using System.Configuration;

namespace Abp.Net.Sms
{
    public class AbpNetSmsSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
                {
                // Sms config
                new SettingDefinition(SmsSettingNames.ServiceUrl,
                    ConfigurationManager.AppSettings[SmsSettingNames.ServiceUrl] ?? ""),
                new SettingDefinition(SmsSettingNames.AppKey,
                    ConfigurationManager.AppSettings[SmsSettingNames.AppKey] ?? ""),
                new SettingDefinition(SmsSettingNames.AppSecret,
                    ConfigurationManager.AppSettings[SmsSettingNames.AppSecret] ?? ""),
                new SettingDefinition(SmsSettingNames.DefaultFreeSignName,
                    ConfigurationManager.AppSettings[SmsSettingNames.DefaultFreeSignName] ?? ""),
                new SettingDefinition(SmsSettingNames.DefaultSmsTemplateCode,
                    ConfigurationManager.AppSettings[SmsSettingNames.DefaultSmsTemplateCode] ?? ""),
        };
        }
    }
}
