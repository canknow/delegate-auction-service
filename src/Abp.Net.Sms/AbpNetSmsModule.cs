﻿using Abp.Modules;
using System.Reflection;

namespace Abp.Net.Sms
{
    public class AbpNetSmsModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }

        public override void PreInitialize()
        {
            Configuration.Settings.Providers.Add<AbpNetSmsSettingProvider>();
        }
    }
}
