﻿using Application.Security;

namespace Application.Public.MultiTenancy.Dto
{
    public class TenantRegistrationOptions
    {
        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }

        public bool UseCaptcha { get; set; }
    }
}
