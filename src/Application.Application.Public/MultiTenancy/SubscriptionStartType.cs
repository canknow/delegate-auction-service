﻿namespace Application.Public.MultiTenancy
{
    public enum SubscriptionStartType
    {
        Free = 1,
        Trial = 2,
        Paid = 3
    }
}
