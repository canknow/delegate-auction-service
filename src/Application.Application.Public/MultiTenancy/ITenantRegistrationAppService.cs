﻿using Abp.Application.Services;
using Application.Editions.Dto;
using Application.MultiTenancy.Dto;
using Application.Public.MultiTenancy.Dto;
using System.Threading.Tasks;

namespace Application.Public.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<TenantRegistrationOptions> GetTenantRegistrationOptions();

        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}
