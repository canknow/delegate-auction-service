﻿using Abp.Application.Services;
using Abp.Web.Models.AbpUserConfiguration;
using System.Threading.Tasks;

namespace Application.Public.UserConfiguration
{
    public interface IUserConfigurationAppService: IApplicationService
    {
        Task<AbpUserConfigurationDto> GetAll();

        AbpMultiTenancyConfigDto GetUserMultiTenancyConfig();

        AbpUserSessionConfigDto GetUserSessionConfig();

        Task<AbpUserFeatureConfigDto> GetUserFeaturesConfig();

        Task<AbpUserAuthConfigDto> GetUserAuthConfig();

        Task<AbpUserSettingConfigDto> GetUserSettingConfig();
    }
}
