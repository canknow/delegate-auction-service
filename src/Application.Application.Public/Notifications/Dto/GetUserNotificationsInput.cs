﻿using Abp.Notifications;
using Application.Dto;

namespace Application.Public.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}