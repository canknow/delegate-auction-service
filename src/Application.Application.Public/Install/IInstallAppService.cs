﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Application.Install.Dto;

namespace Application.Install
{
    public interface IInstallAppService : IApplicationService
    {
        void Restart();

        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}