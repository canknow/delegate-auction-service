﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Common.Dto;
using Application.Dto;
using Application.Editions.Dto;
using Application.Public.Authorization.Users.Dto;

namespace Application.Common
{
    public interface ICommonLookupAppService : IApplicationService
    {
        Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false);

        Task<PagedResultDto<UserProfileDto>> FindUsers(FindUsersInput input);

        Task<UserProfileDto> GetUser(IdInput input);

        GetDefaultEditionNameOutput GetDefaultEditionName();
    }
}