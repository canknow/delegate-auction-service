﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Application.Public
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(ApplicationCoreModule))]
    public class ApplicationApplicationPublicApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationApplicationPublicApplicationModule).GetAssembly());
        }
    }
}