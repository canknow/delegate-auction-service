﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Application.MultiTenancy.Accounting.Dto;

namespace Application.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
