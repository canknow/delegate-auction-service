using System.Threading.Tasks;
using Abp.Application.Services;
using Application.Editions.Dto;
using Application.MultiTenancy.Dto;

namespace Application.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}