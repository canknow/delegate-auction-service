﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace Application.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task UpgradeTenantToEquivalentEdition(int upgradeEditionId);
    }
}
