﻿using Application.BusinessNotifiers.Admin.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using System.Threading.Tasks;

namespace Application.BusinessNotifiers.Admin
{
    public class BusinessNotifierAppService : CrudAppService<BusinessNotifier, BusinessNotifierDto>, IBusinessNotifierAppService
    {
        public BusinessNotifierAppService(IRepository<BusinessNotifier> respository) :base(respository)
        {
        }

        public async Task<BusinessNotifierCreateOrEditDto> GetBusinessNotifierForEdit(NullableIdDto input)
        {
            BusinessNotifierCreateOrEditDto businessNotifierCreateOrEditDto;

            if (input.Id.HasValue) //Editing existing edition?
            {
                BusinessNotifier businessNotifier = await Repository.GetAsync(input.Id.Value);
                businessNotifierCreateOrEditDto = businessNotifier.MapTo<BusinessNotifierCreateOrEditDto>();
            }
            else
            {
                businessNotifierCreateOrEditDto = new BusinessNotifierCreateOrEditDto();
            }

            return businessNotifierCreateOrEditDto;
        }

        public BusinessNotifierCreateOrEditDto CreateOrUpdateBusinessNotifier(BusinessNotifierCreateOrEditDto input)
        {
            if (!input.Id.HasValue)
            {
                CheckCreatePermission();

                var entity = input.MapTo<BusinessNotifier>();

                Repository.Insert(entity);
                CurrentUnitOfWork.SaveChanges();

                return ObjectMapper.Map<BusinessNotifierCreateOrEditDto>(entity);
            }
            else
            {
                CheckUpdatePermission();

                var entity = GetEntityById(input.Id.Value);
                ObjectMapper.Map(input, entity);
                CurrentUnitOfWork.SaveChanges();

                return ObjectMapper.Map<BusinessNotifierCreateOrEditDto>(entity);
            }
        }
    }
}
