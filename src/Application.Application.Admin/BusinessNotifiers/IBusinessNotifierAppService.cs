﻿using Application.BusinessNotifiers.Admin.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System.Threading.Tasks;

namespace Application.BusinessNotifiers.Admin
{
    public interface IBusinessNotifierAppService : ICrudAppService<BusinessNotifierDto>
    {
        Task<BusinessNotifierCreateOrEditDto> GetBusinessNotifierForEdit(NullableIdDto input);

        BusinessNotifierCreateOrEditDto CreateOrUpdateBusinessNotifier(BusinessNotifierCreateOrEditDto input);
    }
}