﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.BusinessNotifiers.Admin.Dto
{
    [AutoMap(typeof(BusinessNotifier))]
    public class BusinessNotifierDto:FullAuditedEntityDto
    {
        public long UserId { get; set; }

        public bool Status { get; set; }

        public UserDto User { get; set; }
    }
}
