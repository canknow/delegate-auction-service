﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Authorization.Users;

namespace Application.BusinessNotifiers.Admin.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        public string Number { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string NickName { get; set; }
    }
}
