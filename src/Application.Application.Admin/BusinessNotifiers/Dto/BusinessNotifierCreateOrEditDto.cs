﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.BusinessNotifiers.Admin.Dto
{
    [AutoMap(typeof(BusinessNotifier))]
    public class BusinessNotifierCreateOrEditDto:NullableIdDto
    {
        public bool Status { get; set; }

        public long UserId { get; set; }
    }
}
