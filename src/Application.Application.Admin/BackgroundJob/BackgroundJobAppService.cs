﻿using Application.Authorization;
using Application.BackgroundJob.Dto;
using Abp.Application.Services;
using Abp.Authorization;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;

namespace Application.BackgroundJob
{
    [AbpAuthorize(AppPermissions.Administration.Host.BackgroundJobs)]
    public class BackgroundJobAppService: CrudAppService<BackgroundJobInfo, BackgroundJobListDto, long>,IBackgroundJobAppService
    {
        private readonly IRepository<BackgroundJobInfo, long> _backgroundJobRepository;

        public BackgroundJobAppService(IRepository<BackgroundJobInfo, long> backgroundJobRepository):base(backgroundJobRepository)
        {
            _backgroundJobRepository = backgroundJobRepository;
        }
    }
}
