﻿using Abp.Application.Services;
using Application.BackgroundJob.Dto;

namespace Application.BackgroundJob
{
    public interface IBackgroundJobAppService: ICrudAppService<BackgroundJobListDto, long>
    {
    }
}
