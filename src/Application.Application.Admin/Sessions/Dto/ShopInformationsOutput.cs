﻿namespace Application.Sessions.Dto
{
    public class ShopInformationsOutput
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Logo { get; set; }

        public string CopyrightLogo { get; set; }
    }
}
