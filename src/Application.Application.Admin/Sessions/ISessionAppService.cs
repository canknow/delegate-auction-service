﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Application.Sessions.Dto;

namespace Application.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
