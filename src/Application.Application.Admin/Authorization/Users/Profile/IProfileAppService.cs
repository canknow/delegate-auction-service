﻿using Abp.Application.Services;
using Application.Authorization.Users.Profile.Dto;
using System.Threading.Tasks;

namespace Application.Authorization.Users.Profile
{
    public interface IProfileAppService : IApplicationService
    {
        Task<CurrentUserProfileEditDto> GetCurrentUserProfileForEdit();

        Task UpdateCurrentUserProfile(CurrentUserProfileEditDto input);
        
        Task ChangePassword(ChangePasswordInput input);

        Task UpdateProfilePicture(UpdateProfilePictureInput input);

        Task UpdateAvatar(UpdateAvatarInput input);

        Task<GetPasswordComplexitySettingOutput> GetPasswordComplexitySetting();

        Task SendVerificationSms();

        Task VerifySmsCode(VerifySmsCodeInputDto input);
    }
}
