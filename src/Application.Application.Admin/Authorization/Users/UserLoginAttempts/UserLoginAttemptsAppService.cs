﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Application.UserLoginAttempts.Dto;
using System.Linq;

namespace Application.UserLoginAttempts
{
    public class UserLoginAttemptsAppService : CrudAppService<UserLoginAttempt, UserLoginAttemptDto, long>, IUserLoginAttemptsAppService
    {
        public UserLoginAttemptsAppService(IRepository<UserLoginAttempt, long> userLoginAttemptRepository)
            :base(userLoginAttemptRepository)
        {
        }

        protected override IQueryable<UserLoginAttempt> CreateFilteredQuery(PagedAndSortedResultRequestDto input)
        {
            return Repository.GetAll().Where(model=>model.UserId==AbpSession.UserId.Value);
        }
    }
}
