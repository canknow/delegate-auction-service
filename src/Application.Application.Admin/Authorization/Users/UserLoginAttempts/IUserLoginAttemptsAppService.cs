﻿using Application.UserLoginAttempts.Dto;
using Abp.Application.Services;

namespace Application.UserLoginAttempts
{
    public interface IUserLoginAttemptsAppService : ICrudAppService<UserLoginAttemptDto, long>
    {
    }
}
