﻿using System.Collections.Generic;
using Application.Authorization.Permissions.Dto;

namespace Application.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}