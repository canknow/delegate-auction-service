﻿using Application.Authorization.Users;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;

namespace Application.Authorization.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserListItemDto : EntityDto<long>
    {
        public string Number { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string NickName { get; set; }

        public List<UseRoleDto> Roles { get; set; }

        public int TotalScore { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string Avatar { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationTime { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }

        [AutoMapFrom(typeof(UserRole))]
        public class UseRoleDto
        {
            public int RoleId { get; set; }

            public string RoleName { get; set; }
        }
    }
}
