﻿using Abp.Authorization.Users;
using Abp.AutoMapper;

namespace Application.Authorization.Users.Dto
{
    [AutoMap(typeof(UserRole))]
    public class UserListRoleDto
    {
        public int RoleId { get; set; }

        public string RoleName { get; set; }
    }
}