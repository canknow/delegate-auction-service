using System.Collections.Generic;
using Application.Authorization.Users.Dto;
using Application.Dto;

namespace Application.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}