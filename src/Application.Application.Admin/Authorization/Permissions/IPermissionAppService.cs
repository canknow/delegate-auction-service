﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Authorization.Permissions.Dto;

namespace Application.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
