﻿using Abp.Application.Services;
using Application.Dto;
using Application.Logging.Dto;

namespace Application.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();

        void DeleteWebLogs();
    }
}
