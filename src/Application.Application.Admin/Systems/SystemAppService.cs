﻿using Abp.Authorization;
using Application.Authorization;
using Application.MultiTenancy;

namespace Application.Systems
{
    [AbpAuthorize(AppPermissions.Administration.Host.Value)]
    public class SystemAppService : ApplicationAppServiceBase, ISystemAppService
    {
        public TenantHelper TenantHelper { get; set; }
    }
}