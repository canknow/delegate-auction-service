﻿using Abp.Application.Services;
using Application.Tenants.Dashboard.Dto;

namespace Application.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();

        GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input);

        GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input);

        GetRegionalStatsOutput GetRegionalStats(GetRegionalStatsInput input);

        GetGeneralStatsOutput GetGeneralStats(GetGeneralStatsInput input);

        DashboardOutput GetDashboardActivity();

        UserActivityOutput GetUserActivity();

        OrderActivityOutput GetOrderActivity();
    }
}
