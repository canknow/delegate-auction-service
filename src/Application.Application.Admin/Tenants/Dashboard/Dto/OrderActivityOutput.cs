﻿using System.Collections.Generic;

namespace Application.Tenants.Dashboard.Dto
{
    public class OrderActivityOutput
    {
        public List<CommonDateCount> NewPayedOrders { get; set; }

        public List<CommonDateCount> TotalPayedOrders { get; set; }
    }
}