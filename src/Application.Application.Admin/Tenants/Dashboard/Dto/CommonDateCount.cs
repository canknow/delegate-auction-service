﻿namespace Application.Tenants.Dashboard.Dto
{
    public class CommonDateCount
    {
        public string Date { get; set; }
        public int Count { get; set; }
    }
}
