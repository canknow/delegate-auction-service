﻿using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.UI;
using Application.Authorization.Users;
using Application.Tenants.Dashboard.Dto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Tenants.Dashboard
{
    [DisableAuditing]
    public class TenantDashboardAppService : ApplicationAppServiceBase, ITenantDashboardAppService
    {
        public IRepository<User, long> UserRepository { get; set; }

        public GetMemberActivityOutput GetMemberActivity()
        {
            return new GetMemberActivityOutput
            (
                DashboardRandomDataGenerator.GenerateMemberActivities()
            );
        }

        public GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input)
        {
            var output = new GetDashboardDataOutput
            {
                TotalProfit = DashboardRandomDataGenerator.GetRandomInt(5000, 9000),
                NewFeedbacks = DashboardRandomDataGenerator.GetRandomInt(1000, 5000),
                NewOrders = DashboardRandomDataGenerator.GetRandomInt(100, 900),
                NewUsers = DashboardRandomDataGenerator.GetRandomInt(50, 500),
                SalesSummary = DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod),
                Expenses = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Growth = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Revenue = DashboardRandomDataGenerator.GetRandomInt(1000, 9000),
                TotalSales = DashboardRandomDataGenerator.GetRandomInt(10000, 90000),
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                DailySales = DashboardRandomDataGenerator.GetRandomArray(30, 10, 50),
                ProfitShares = DashboardRandomDataGenerator.GetRandomPercentageArray(3)
            };

            return output;
        }

        public GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input)
        {
            return new GetSalesSummaryOutput(DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod));
        }

        public GetRegionalStatsOutput GetRegionalStats(GetRegionalStatsInput input)
        {
            return new GetRegionalStatsOutput(DashboardRandomDataGenerator.GenerateRegionalStat());
        }

        public GetGeneralStatsOutput GetGeneralStats(GetGeneralStatsInput input)
        {
            return new GetGeneralStatsOutput
            {
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100)
            };
        }

        public UserActivityOutput GetUserActivity()
        {
            var userActivityOutput = new UserActivityOutput()
            {
                NewUsers = new List<CommonDateCount>(),
                TotalUsers = new List<CommonDateCount>()
            };
            DateTime yestoday = DateTime.Now.AddDays(-1);
            for (int i = 0; i <= 30; i++)
            {
                DateTime dateTime = DateTime.Now.AddDays(i - 30);
                DateTime endDateTime = dateTime.AddDays(1);

                int userCount = UserRepository.GetAll().Where(model => model.CreationTime >= dateTime &&
                model.CreationTime <= endDateTime).Count();
                int userTotalCount = UserRepository.GetAll().Where(model =>
                model.CreationTime <= endDateTime).Count();
                userActivityOutput.NewUsers.Add(new CommonDateCount()
                {
                    Date = dateTime.ToString("yyyy-MM-dd"),
                    Count = userCount
                });
                userActivityOutput.TotalUsers.Add(new CommonDateCount()
                {
                    Date = dateTime.ToString("yyyy-MM-dd"),
                    Count = userTotalCount
                });
            }
            return userActivityOutput;
        }

        public OrderActivityOutput GetOrderActivity()
        {
            var orderActivityOutput = new OrderActivityOutput()
            {
                NewPayedOrders = new List<CommonDateCount>(),
                TotalPayedOrders = new List<CommonDateCount>()
            };
            DateTime yestoday = DateTime.Now.AddDays(-1);
            for (int i = 0; i <= 30; i++)
            {
                DateTime dateTime = DateTime.Now.AddDays(i - 30);
                DateTime endDateTime = dateTime.AddDays(1);
            }
            return orderActivityOutput;
        }

        public DashboardOutput GetDashboardActivity()
        {
            try
            {
                DashboardOutput dashboardOutput = new DashboardOutput();
                DateTime yestoday = DateTime.Now.AddDays(-1);
                return dashboardOutput;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}