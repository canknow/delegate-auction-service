﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Files.Dto
{
    [AutoMap(typeof(AbpFileInfo))]
    public class FileInfoDto:AuditedEntityDto
    {
        public string Name { get; set; }

        public string Path { get; set; }
    }
}
