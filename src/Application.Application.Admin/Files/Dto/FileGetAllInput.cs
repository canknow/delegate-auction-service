﻿using Abp.Application.Services.Dto;

namespace Application.Files.Dto
{
    public class FileGetAllInput:PagedAndSortedResultRequestDto
    {
        public string FileType { get; set; }
    }
}
