﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace Application.Files.Dto
{
    public class FileDeleteInput
    {
        public string Ids { get; set; }
    }
}
