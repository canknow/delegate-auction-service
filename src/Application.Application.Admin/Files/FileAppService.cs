﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using Application.Files.Dto;
using System;
using System.IO;
using System.Linq;
using Application.Web;

namespace Application.Files
{
    public class FileAppService: CrudAppService<AbpFileInfo,FileInfoDto,int, FileGetAllInput>, IFileAppService
    {
        public PathHelper PathHelper { get; set; }

        public FileAppService(
            IRepository<AbpFileInfo> repository)
            :base(repository)
        {

        }

        protected override IQueryable<AbpFileInfo> CreateFilteredQuery(FileGetAllInput input)
        {
            return Repository.GetAll();
        }

        public override void Delete(EntityDto<int> input)
        {
            try
            {
                AbpFileInfo fileInfo = Repository.Get(input.Id);
                string filePath = PathHelper.GetAbsolutePath(fileInfo.Path);
                File.Delete(filePath);
                base.Delete(input);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public void DeleteFiles(FileDeleteInput input)
        {
            try
            {
                string[] ids= input.Ids.Split(',');
                foreach(var item in ids)
                {
                    int id = Convert.ToInt32(item);
                    AbpFileInfo fileInfo = Repository.Get(id);
                    string filePath = PathHelper.GetAbsolutePath(fileInfo.Path);
                    File.Delete(filePath);
                    base.Delete(new EntityDto<int>(id));
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}
