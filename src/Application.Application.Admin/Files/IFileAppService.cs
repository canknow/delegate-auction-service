﻿using Abp.Application.Services;
using Application.Files.Dto;

namespace Application.Files
{
    public interface IFileAppService: ICrudAppService<FileInfoDto, int, FileGetAllInput>
    {
        void DeleteFiles(FileDeleteInput input);
    }
}
