﻿using Abp.Configuration;

namespace Application.Timing.Dto
{
    public class GetTimezoneComboboxItemsInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }

        public string SelectedTimezoneId { get; set; }
    }
}
