﻿namespace Application.Configuration.Tenants.Dto
{
    public class OrderSettingEditDto
    {
        public string DecreaseStockWhen { get; set; }

        public int OverTimeForDelete { get; set; }

        public bool ShouldHasParentForBuy { get; set; }

        public string DistributionWhen { get; set; }

        public int AutoReceiveLimit { get; set; }

        public bool RebateGuarantee { get; set; }
    }
}