using Application.Configuration.Host.Dto;
using System.ComponentModel.DataAnnotations;

namespace Application.Configuration.Tenants.Dto
{
    public class TenantSettingsEditDto
    {
        [Required]
        public TenantUserManagementSettingsEditDto UserManagement { get; set; }

        public LdapSettingsEditDto Ldap { get; set; }

        [Required]
        public SecuritySettingsEditDto Security { get; set; }

        public TenantBillingSettingsEditDto Billing { get; set; }

        public SpreadSettingsEditDto Spread { get; set; }

        public WalletSettingsEditDto Wallet { get; set; }

        public OrderSettingEditDto Order { get; set; }
    }
}