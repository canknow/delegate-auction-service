﻿namespace Application.Configuration.Tenants.Dto
{
    public class SpreadSettingsEditDto
    {
        public int MaxManualBindCount { get; set; }

        public decimal UpgradeOrderMoney { get; set; }

        public int NewCustomerScoreAward { get; set; }

        public bool MustBeSpreaderForSpread { get; set; }
    }
}
