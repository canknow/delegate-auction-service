﻿namespace Application.Configuration.Tenants.Dto
{
    public class WalletSettingsEditDto
    {
        public bool NeedFinanceAccountForWithdraw { get; set; }

        public string DefaultWithdrawMethod { get; set; }

        public bool AutoWithdraw { get; set; }

        public bool AutoPayForWithdraw { get; set; }
    }
}
