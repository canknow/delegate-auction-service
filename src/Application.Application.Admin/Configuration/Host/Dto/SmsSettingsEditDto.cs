﻿namespace Application.Configuration.Host.Dto
{
    public class SmsSettingsEditDto
    {
        public string ServiceUrl { get; set; }

        public string AppKey { get; set; }

        public string AppSecret { get; set; }

        public string DefaultFreeSignName { get; set; }

        public string DefaultSmsTemplateCode { get; set; }
    }
}
