﻿namespace Application.Configuration.Host.Dto
{
    public class GeneralSettingsEditDto
    {
        public string Timezone { get; set; }

        /// <summary>
        /// This value is only used for comparing user's timezone to default timezone
        /// </summary>
        public string TimezoneForComparison { get; set; }

        public string WebSiteRootAddress { get; set; }

        public string AppName { get; set; }

        public string AppLogo { get; set; }

        public string AppTitleLogo { get; set; }

        public bool WebSiteStatus { get; set; }
    }
}