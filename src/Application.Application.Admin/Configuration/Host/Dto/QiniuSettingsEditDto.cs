﻿namespace Application.Configuration.Host.Dto
{
    public class QiniuSettingsEditDto
    {
        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string Bucket { get; set; }

        public bool Status { get; set; }

        public string Domain { get; set; }
    }
}
