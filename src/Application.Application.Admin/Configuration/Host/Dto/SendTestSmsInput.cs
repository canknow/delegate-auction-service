﻿namespace Application.Configuration.Host.Dto
{
    public class SendTestSmsInput
    {
        public string PhoneNumber { get; set; }
    }
}
