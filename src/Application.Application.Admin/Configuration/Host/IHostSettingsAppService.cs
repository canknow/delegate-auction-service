﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Application.Configuration.Host.Dto;

namespace Application.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);

        Task SendTestSms(SendTestSmsInput input);
    }
}
