﻿using Microsoft.AspNetCore.Http;
using System.Text;

namespace Application.Web.Host.Extension
{
    public static class HttpRequestExtensions
    {
        public static string GetAbsoluteUri(this HttpRequest request)
        {
            return new StringBuilder()
                .Append(request.Scheme)
                .Append(System.Uri.SchemeDelimiter)
                .Append(request.Host)
                .Append(!request.Host.Port.HasValue ? "" : ":" + request.Host.Port.Value)
                .Append(request.PathBase)
                .Append(request.Path)
                .Append(request.QueryString)
                .ToString();
        }

        public static string GetAbsoluteRootUri(this HttpRequest request)
        {

            return new StringBuilder()
                .Append(request.Scheme)
                .Append(System.Uri.SchemeDelimiter)
                .Append(request.Host)
                .Append(!request.Host.Port.HasValue ? "" : ":" + request.Host.Port.Value)
                .Append(request.Path)
                .ToString();
        }
    }
}
