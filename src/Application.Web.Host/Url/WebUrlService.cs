using Abp.Configuration;
using Abp.Dependency;
using Application.Configuration;
using Application.Url;
using Application.Web.Url;

namespace Application.Web.Host.Url
{
    public class WebUrlService : WebUrlServiceBase, IWebUrlService, ITransientDependency
    {
        public WebUrlService(
            IAppConfigurationAccessor configurationAccessor, ISettingManager settingManager) :
            base(configurationAccessor, settingManager)
        {
        }

        public override string WebSiteRootAddressFormatKey => "App:WebSiteRootAddressFormat";
    }
}