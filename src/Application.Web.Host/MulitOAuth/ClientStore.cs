﻿using Microsoft.AspNetCore.Authentication.MultiOAuth.Stores;
using System;
using Abp.Configuration;
using Abp.Dependency;
using Application.Configuration;

namespace Application.Web.Host.MulitOAuth
{
    public class ClientStore : IClientStore
    {
        public ClientStoreModel FindBySubjectId(string provider, string subjectId)
        {
            ISettingManager settingManager = IocManager.Instance.Resolve<ISettingManager>();
            int tenantId = Convert.ToInt32(subjectId);

            return new ClientStoreModel()
            {
                Provider = provider,
                ClientId = settingManager.GetSettingValueForTenant(WechatSettings.General.AppId, tenantId),
                ClientSecret = settingManager.GetSettingValueForTenant(WechatSettings.General.Secret, tenantId),
                SubjectId = subjectId
            };
        }
    }
}
