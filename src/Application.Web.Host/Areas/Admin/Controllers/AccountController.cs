﻿using Abp.Extensions;
using Abp.UI;
using Application.Authorization.Models;
using Application.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Application.Web.Host.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AccountController : AccountControllerBase
    {

        [HttpPost]
        public async Task<LoginResultOutput> Login([FromBody] LoginInput input)
        {
            var loginResult = await GetLoginResultAsync(
                input.UserNameOrEmailAddress,
                input.Password,
                input.TenancyName
            );

            if (loginResult.User.ShouldChangePasswordOnNextLogin)
            {
                return new LoginResultOutput
                {
                    ResetPassword = true
                };
            }

            var signInResult = await SignInManager.SignInOrTwoFactorAsync(loginResult, input.RememberMe);
            if (signInResult.RequiresTwoFactor)
            {
                return new LoginResultOutput
                {
                    RequiresTwoFactor = true
                };
            }

            var accessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity));

            return new LoginResultOutput
            {
                AccessToken = accessToken,
                EncryptedAccessToken = GetEncrpyedAccessToken(accessToken),
                ExpireInSeconds = (int)Configuration.Expiration.TotalSeconds,
                UserId = loginResult.User.Id
            };
        }

        public async Task SendPasswordResetCode([FromBody]SendPasswordResetCodeInput input)
        {
            var user = await GetUserByChecking(input.EmailAddress);
            user.SetNewPasswordResetCode();
            await UserEmailer.SendPasswordResetLinkAsync(
                user,
                AppUrlService.CreatePasswordResetUrlFormat(AbpSession.TenantId)
                );
        }

        public async Task<ResetPasswordOutput> ResetPassword([FromBody]ResetPasswordInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.UserId);
            if (user == null || user.PasswordResetCode.IsNullOrEmpty() || user.PasswordResetCode != input.ResetCode)
            {
                throw new UserFriendlyException(L("InvalidPasswordResetCode"), L("InvalidPasswordResetCode_Detail"));
            }

            user.Password = PasswordHasher.HashPassword(user, input.Password);
            user.PasswordResetCode = null;
            user.IsEmailConfirmed = true;
            user.ShouldChangePasswordOnNextLogin = false;

            await UserManager.UpdateAsync(user);

            return new ResetPasswordOutput
            {
                CanLogin = user.IsActive,
                UserName = user.UserName
            };
        }

        public async Task SendEmailActivationLink([FromBody]SendEmailActivationLinkInput input)
        {
            var user = await GetUserByChecking(input.EmailAddress);
            user.SetNewEmailConfirmationCode();
            await UserEmailer.SendEmailActivationLinkAsync(
                user,
                AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId)
            );
        }

        public async Task ActivateEmail([FromBody]ActivateEmailInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.UserId);
            if (user == null || user.EmailConfirmationCode.IsNullOrEmpty() || user.EmailConfirmationCode != input.ConfirmationCode)
            {
                throw new UserFriendlyException(L("InvalidEmailConfirmationCode"), L("InvalidEmailConfirmationCode_Detail"));
            }

            user.IsEmailConfirmed = true;
            user.EmailConfirmationCode = null;

            await UserManager.UpdateAsync(user);
        }
    }
}
