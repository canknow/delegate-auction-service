﻿using Application.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Application.Web.Host.Areas.Admin.Controllers
{
    [Area("Admin")]
    public abstract class AdminApplicationControllerBase : ApplicationControllerBase
    {
    }
}
