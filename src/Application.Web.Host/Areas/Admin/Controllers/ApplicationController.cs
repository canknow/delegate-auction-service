﻿using Application.MultiTenancy;

namespace Application.Web.Host.Areas.Admin.Controllers
{
    public class ApplicationController : AdminApplicationControllerBase
    {
        public TenantHelper TenantHelper { get; set; }
    }
}
