﻿using Abp.Web.Models;
using Application.Files;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace Application.Web.Host.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PublicWechatController : AdminApplicationControllerBase
    {
        public IAppFolders AppFolders { get; set; }
        public FileManager FileManager { get; set; }

        public PublicWechatController()
        {
        }

        public ActionResult UploadMPVerifyFile(IFormFile file)
        {
            var fileName = Path.GetFileName(file.FileName);
            var relativePath = AppFolders.MpFolder + fileName;
            var filePath = hostingEnvironment.WebRootPath + @relativePath;

            try
            {
                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                AbpFileInfo fileInfo = new AbpFileInfo() { Name = fileName, Path = relativePath, TenantId = AbpSession.TenantId };
                fileInfo = FileManager.Create(fileInfo);
                return Json(fileInfo);
            }
            catch (Exception exception)
            {
                return Json(new ErrorInfo(exception.Message));
            }
        }
    }
}
