﻿using Abp;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Localization;
using Abp.Localization.Sources;
using Application.Authorization.Users;
using Application.Configuration;
using Application.MultiTenancy;
using Application.Spread;
using Application.SpreadPosters;
using Application.Wechat;
using Application.Wechat.AutoReplys;
using Application.Wechat.Qrcodes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Senparc.NeuChar;
using Senparc.NeuChar.Entities;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.Media;
using Senparc.Weixin.MP.AdvancedAPIs.User;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.MessageHandlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Web.Host.Areas.Client.Weixin.MessageHandles
{
    public class CustomMessageHandler : MessageHandler<CustomMessageContext>
    {
        protected User _user;
        protected Tenant _tenant;
        protected HttpContext _context;
        protected UserManager _userManager;
        protected WechatUserManager _wechatUserManager;
        protected AutoReplyManager _autoReplyManager;
        protected WechatCommonManager _wechatCommonManager;
        protected ISettingManager _settingManager;
        protected CustomerServiceMessageHelper _customerServiceMessageHelper;
        protected string _accessToken;
        protected ILocalizationManager LocalizationManager { get; set; }
        protected string LocalizationSourceName { get; set; }
        protected readonly IHostingEnvironment _hostingEnvironment;

        protected ILocalizationSource LocalizationSource
        {
            get
            {
                if (LocalizationSourceName == null)
                {
                    throw new AbpException("Must set LocalizationSourceName before, in order to get LocalizationSource");
                }

                if (_localizationSource == null || _localizationSource.Name != LocalizationSourceName)
                {
                    _localizationSource = LocalizationManager.GetSource(LocalizationSourceName);
                }

                return _localizationSource;
            }
        }

        private ILocalizationSource _localizationSource;

        protected virtual string L(string name, params object[] args)
        {
            return LocalizationSource.GetString(name, args);
        }

        /// <summary>  
        /// 模板消息集合（Key：checkCode，Value：OpenId）  
        /// </summary>  
        public static Dictionary<string, string> TemplateMessageCollection = new Dictionary<string, string>();

        public CustomMessageHandler(Stream inputStream, PostModel postModel, int maxRecordCount, Tenant tenant, IHostingEnvironment hostingEnvironment)
            : base(inputStream, postModel, maxRecordCount)
        {
            LocalizationManager = IocManager.Instance.Resolve<ILocalizationManager>();
            LocalizationSourceName = ApplicationConsts.LocalizationSourceName;
            _userManager = IocManager.Instance.Resolve<UserManager>();
            _wechatUserManager = IocManager.Instance.Resolve<WechatUserManager>();
            _autoReplyManager = IocManager.Instance.Resolve<AutoReplyManager>();
            _wechatCommonManager = IocManager.Instance.Resolve<WechatCommonManager>();
            _settingManager = IocManager.Instance.Resolve<ISettingManager>();
            _customerServiceMessageHelper = IocManager.Instance.Resolve<CustomerServiceMessageHelper>();
            _hostingEnvironment = hostingEnvironment;
            _tenant = tenant;

            //这里设置仅用于测试，实际开发可以在外部更全局的地方设置，
            //比如MessageHandler<MessageContext>.GlobalGlobalMessageContext.ExpireMinutes = 3。
            GlobalMessageContext.ExpireMinutes = 3;
        }

        public override IResponseMessageBase DefaultResponseMessage(IRequestMessageBase requestMessage)
        {
            var responseMessage = ResponseMessageBase.CreateFromRequestMessage<ResponseMessageText>(requestMessage);
            responseMessage.Content = "DefaultMessage";
            return responseMessage;
        }

        public SuccessResponseMessage SuccessResponseMessage(IRequestMessageBase requestMessage)
        {
            var responseMessage = new SuccessResponseMessage();
            responseMessage.ReturnText = "success";
            return responseMessage;
        }

        public override async Task OnExecutingAsync(CancellationToken cancellationToken)
        {
            _user = await _wechatUserManager.GetUserFromOpenIdAsync(RequestMessage.FromUserName);
            await base.OnExecutingAsync(cancellationToken);
        }

        private async Task SendSpreadPoster(string openid)
        {
            if (_user == null)
            {
                return;
            }
            _accessToken = await _wechatCommonManager.GetAccessTokenAsync(_tenant.Id);

            try
            {
                _customerServiceMessageHelper.SendText(_accessToken, RequestMessage.FromUserName, L("CreatingSpreadPoster"));

                SpreadManager spreadManager = IocManager.Instance.Resolve<SpreadManager>();
                SpreadPosterManager spreadPosterManager = IocManager.Instance.Resolve<SpreadPosterManager>();
                await spreadManager.CanGetSpreadPoster(_user.ToUserIdentifier());
                string path = await spreadPosterManager.GetDefaultSpreadPosterAsync(_user.ToUserIdentifier());
                string serverPath = _hostingEnvironment.WebRootPath + path;
                UploadTemporaryMediaResult uploadTemporaryMediaResult = await MediaApi.UploadTemporaryMediaAsync(
                    _accessToken,
                    UploadMediaFileType.image,
                    serverPath);
                _customerServiceMessageHelper.SendImage(_accessToken, openid, uploadTemporaryMediaResult.media_id);
            }
            catch (Exception exception)
            {
                _customerServiceMessageHelper.SendText(_accessToken, openid, exception.Message);
            }
        }

        public override async Task<IResponseMessageBase> OnEvent_ClickRequestAsync(RequestMessageEvent_Click requestMessage)
        {
            if (requestMessage.EventKey == "spreadPoster")
            {
                await SendSpreadPoster(requestMessage.FromUserName);
            }
            else if (requestMessage.EventKey == "customerService")
            {
                return CreateResponseMessage<ResponseMessageTransfer_Customer_Service>();
            }
            await SendAutoReplyMessages(RequestType.Event_ClickRequest, requestMessage.EventKey);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnEvent_ViewRequestAsync(RequestMessageEvent_View requestMessage)
        {
            await SendAutoReplyMessages(RequestType.Event_ViewRequest, requestMessage.EventKey);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnTextRequestAsync(RequestMessageText requestMessage)
        {
            string triggerWord = _settingManager.GetSettingValueForTenant(WechatSettings.CustomerService.TriggerWord, _tenant.Id);

            if (!string.IsNullOrEmpty(triggerWord) && requestMessage.Content == triggerWord)
            {
                return CreateResponseMessage<ResponseMessageTransfer_Customer_Service>();
            }
            await SendAutoReplyMessages(RequestType.TextRequest, requestMessage.Content);
            return SuccessResponseMessage(requestMessage);
        }

        private async Task SendAutoReplyMessages(RequestType requestType, string key = null)
        {
            List<AutoReply> autoReplys = _autoReplyManager.GetAutoReplysOfRequestMsgType(requestType, key);

            if (autoReplys != null)
            {
                _accessToken = await _wechatCommonManager.GetAccessTokenAsync(_tenant.Id);

                foreach (AutoReply autoReply in autoReplys)
                {
                    switch (autoReply.MsgType)
                    {
                        case ResponseMsgType.Text:
                            string content = autoReply.Content;
                            if (_user != null)
                            {

                                content = content.Replace("{userNickName}", _user.NickName);
                                content = content.Replace("{userNumber}", _user.Number);
                            }
                            int userCount = _userManager.GetUserCount();
                            content = content.Replace("{userCount}", userCount.ToString());

                            _customerServiceMessageHelper.SendText(_accessToken, RequestMessage.FromUserName, content);
                            break;
                        case ResponseMsgType.Image:
                            _customerServiceMessageHelper.SendImage(_accessToken, RequestMessage.FromUserName, autoReply.MediaId);
                            break;
                        case ResponseMsgType.Music:
                            _customerServiceMessageHelper.SendVoice(_accessToken, RequestMessage.FromUserName, autoReply.MediaId);
                            break;
                        case ResponseMsgType.Video:
                            break;
                        case ResponseMsgType.MultipleNews:
                            _customerServiceMessageHelper.SendNews(_accessToken, RequestMessage.FromUserName, autoReply.Articles.ToList());
                            break;
                        case ResponseMsgType.News:
                            _customerServiceMessageHelper.SendMpNews(_accessToken, RequestMessage.FromUserName, autoReply.MediaId);
                            break;
                    }
                }
            }
        }

        public override async Task<IResponseMessageBase> OnEvent_ScanRequestAsync(RequestMessageEvent_Scan requestMessage)
        {
            _accessToken = await _wechatCommonManager.GetAccessTokenAsync(_tenant.Id);
            UserInfoJson userInfo = UserApi.Info(_accessToken, RequestMessage.FromUserName);

            if (_user == null)
            {
                _user = await _wechatUserManager.CreateUserWhenSubscribeAsync(_tenant.Id, userInfo);
            }
            else
            {
                await _wechatUserManager.UpdateUserAsync(_tenant.Id, userInfo);
            }
            if (!String.IsNullOrEmpty(requestMessage.EventKey))
            {
                int sceneId = int.Parse(requestMessage.EventKey);
                Qrcode qrcode = _wechatUserManager.ProcessSceneId(sceneId, _user);
                if (qrcode != null && qrcode.UserId == _user.Id)
                {
                    _customerServiceMessageHelper.SendText(_accessToken, requestMessage.FromUserName, "二维码验证成功");
                }
            }
            await SendAutoReplyMessages(RequestType.Event_ScanRequest);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnEvent_SubscribeRequestAsync(RequestMessageEvent_Subscribe requestMessage)
        {
            _accessToken = await _wechatCommonManager.GetAccessTokenAsync(_tenant.Id);
            UserInfoJson userInfo = UserApi.Info(_accessToken, RequestMessage.FromUserName);

            if (_user == null)
            {
                _user = await _wechatUserManager.CreateUserWhenSubscribeAsync(_tenant.Id, userInfo);
            }
            else
            {
                await _wechatUserManager.UpdateUserAsync(_tenant.Id, userInfo);
            }
            if (requestMessage.EventKey.Contains("qrscene_"))
            {
                int sceneId = int.Parse(requestMessage.EventKey.Substring(8));
                _wechatUserManager.ProcessSceneId(sceneId, _user);
            }
            await SendAutoReplyMessages(RequestType.Event_SubscribeRequest);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnImageRequestAsync(RequestMessageImage requestMessage)
        {
            await SendAutoReplyMessages(RequestType.ImageRequest);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnVoiceRequestAsync(RequestMessageVoice requestMessage)
        {
            await SendAutoReplyMessages(RequestType.VoiceRequest);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnVideoRequestAsync(RequestMessageVideo requestMessage)
        {
            await SendAutoReplyMessages(RequestType.VideoRequest);
            return SuccessResponseMessage(requestMessage);
        }

        public override async Task<IResponseMessageBase> OnLinkRequestAsync(RequestMessageLink requestMessage)
        {
            await SendAutoReplyMessages(RequestType.LinkRequest);
            return SuccessResponseMessage(requestMessage);
        }
    }
}
