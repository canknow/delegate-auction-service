﻿using Abp.Configuration;
using Application.Configuration;
using Application.Controllers;
using Application.MultiTenancy;
using Application.Web.Host.Areas.Client.Weixin.MessageHandles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Senparc.CO2NET.HttpUtility;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.MvcExtension;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Web.Host.Areas.Client.Controllers
{
    [AllowAnonymous]
    [Area("Client")]
    public class WeixinController : ApplicationControllerBase
    {
        public TenantHelper TenantHelper { get; set; }
        private readonly IHostingEnvironment _hostingEnvironment;

        public WeixinController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [ActionName("Index")]
        public Task<ActionResult> Get(string signature, string timestamp, string nonce, string echostr)
        {
            return Task.Factory.StartNew(() =>
            {
                Tenant tenant = TenantHelper.GetTenant();
                CurrentUnitOfWork.SetTenantId(tenant.Id);
                string EncodingAESKey = SettingManager.GetSettingValueForTenant(WechatSettings.General.EncodingAESKey, tenant.Id);
                string AppId = SettingManager.GetSettingValueForTenant(WechatSettings.General.AppId, tenant.Id);
                string Token = SettingManager.GetSettingValueForTenant(WechatSettings.General.Token, tenant.Id);

                if (CheckSignature.Check(signature, timestamp, nonce, Token))
                {
                    return echostr; //返回随机字符串则表示验证通过
                }
                else
                {
                    return "failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, Token) + "。" +
                        "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。";
                }
            }).ContinueWith<ActionResult>(task => Content(task.Result));
        }


        /// <summary>
        /// 最简化的处理流程
        /// </summary>
        [HttpPost]
        [ActionName("Index")]
        public async Task<ActionResult> Post(PostModel postModel)
        {
            Tenant tenant = TenantHelper.GetTenant();
            CurrentUnitOfWork.SetTenantId(tenant.Id);
            string EncodingAESKey = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.EncodingAESKey, tenant.Id);
            string AppId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, tenant.Id);
            string Token = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Token, tenant.Id);

            postModel.Token = Token;
            postModel.EncodingAESKey = EncodingAESKey;
            postModel.AppId = AppId;

            //v4.2.2之后的版本，可以设置每个人上下文消息储存的最大数量，防止内存占用过多，如果该参数小于等于0，则不限制
            var maxRecordCount = 10;

            if (!CheckSignature.Check(postModel.Signature, postModel.Timestamp, postModel.Nonce, postModel.Token))
            {
                return new WeixinResult("参数错误！");
            }

            //执行微信处理过程
            var messageHandler = new CustomMessageHandler(Request.GetRequestMemoryStream(), postModel, maxRecordCount, tenant, _hostingEnvironment);
            messageHandler.OmitRepeatedMessage = true;//启用消息去重功能
            CancellationToken cancellationToken = new CancellationToken();
            await messageHandler.ExecuteAsync(cancellationToken);
            return new FixWeixinBugWeixinResult(messageHandler);
        }
    }
}
