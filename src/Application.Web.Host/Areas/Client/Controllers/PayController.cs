﻿using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.UI;
using Application.Client.Pay.Dto;
using Application.Controllers;
using Application.Orders;
using Application.Orders.Exceptions;
using Application.Wallets;
using Application.Wechat;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.TenPay.V3;
using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Application.Web.Host.Areas.Mobile.Controllers
{
    [Area("Client")]
    public class PayController : ApplicationControllerBase
    {
        protected Application.Client.Orders.IOrderAppService _orderAppService;
        protected IRepository<Order> _orderRepository;
        public OrderManager OrderManager { get; set; }
        public WechatCommonManager WeixinCommonManager { get; set; }
        public WechatUserManager WeixinUserManager { get; set; }
        public ServerContextHelper ServerContextHelper { get; set; }

        public PathHelper PathHelper { get; set; }

        public async Task<TenPayV3Info> GetTenPayV3Info()
        {
            TenPayV3Info tenPayV3Info =await WeixinCommonManager.GetTenPayV3InfoAsync(
                AbpSession.TenantId.Value, 
                Url.Action("PayNotify", "Pay", null, Request.Scheme));
            return tenPayV3Info;
        }

        public PayController(
             Application.Client.Orders.IOrderAppService orderAppService,
            IRepository<Order> orderRepository)
        {
            _orderAppService = orderAppService;
            _orderRepository = orderRepository;
        }

        public ActionResult Index(PayInput input)
        {
            PayOutput PayOutput = _orderAppService.GetPayOutput(input);
            return View(PayOutput);
        }

        public async Task<JsonResult> GetPaySign([FromBody]PayInput input)
        {
            Order order = _orderRepository.Get(input.Id);

            TenPayV3Info tenPayV3Info = await GetTenPayV3Info();
            string timeStamp = TenPayV3Util.GetTimestamp();
            string nonceStr = TenPayV3Util.GetNoncestr();
            string body = order.Title;//商品或支付单简要描述
            string out_trade_no = order.Number;//商户系统内部的订单号，32个字符内，可包含字母，其他说明见商户订单号
            int totalFee = (int)(order.Money*100);//订单总金额，只能是整数。
            string spbill_create_ip = ServerContextHelper.GetServerIp();//APP和网页支付提交用户端IP，Native支付填调用微信支付API的机器IP
            string openid = WeixinUserManager.GetOpenid(order.User.ToUserIdentifier());//trade_type=JSAPI,此参数必传，用户在商户appid下的唯一标识。必传，这里需要将去获取openid赋值上去

            TenPayV3UnifiedorderRequestData xmlDataInfo = new TenPayV3UnifiedorderRequestData(
                tenPayV3Info.AppId,
                tenPayV3Info.MchId, 
                body,
                out_trade_no,
                totalFee,
                spbill_create_ip,
                tenPayV3Info.TenPayV3Notify,
                Senparc.Weixin.TenPay.TenPayV3Type.JSAPI,
                openid,
                tenPayV3Info.Key, 
                nonceStr);

            UnifiedorderResult unifiedorderResult = TenPayV3.Unifiedorder(xmlDataInfo);
            if (unifiedorderResult.return_code== "FAIL")
            {
                throw new UserFriendlyException(unifiedorderResult.return_msg);
            }
            string prepay_id = unifiedorderResult.prepay_id;

            timeStamp = TenPayV3Util.GetTimestamp();
            nonceStr = TenPayV3Util.GetNoncestr();

            RequestHandler paysignReqHandler = new RequestHandler(null);
            paysignReqHandler.Init();

            //设置支付参数
            paysignReqHandler.SetParameter("appId", tenPayV3Info.AppId);
            paysignReqHandler.SetParameter("timeStamp", timeStamp);
            paysignReqHandler.SetParameter("nonceStr", nonceStr);
            string package = string.Format("prepay_id={0}", prepay_id);
            paysignReqHandler.SetParameter("package", package);

            string paySign = TenPayV3.GetJsPaySign(tenPayV3Info.AppId, timeStamp, nonceStr, package, tenPayV3Info.Key);
            paysignReqHandler.SetParameter("paySign", paySign);
            return Json(paysignReqHandler.GetAllParameters());
        }

        [AllowAnonymous]
        [Audited]
        public async Task<ContentResult> PayNotify()
        {
            try
            {
                TenPayV3Info tenPayV3Info = await GetTenPayV3Info();
                ResponseHandler responseHandler = new ResponseHandler(HttpContext);

                string return_code = responseHandler.GetParameter("return_code");
                string return_msg = responseHandler.GetParameter("return_msg");

                string res = null;
                responseHandler.SetKey(tenPayV3Info.Key);

                //验证请求是否从微信发过来（安全）
                if (return_code.ToUpper() == "SUCCESS")
                {
                    string appId = responseHandler.GetParameter("appId");
                    string mch_id = responseHandler.GetParameter("mch_id");
                    string openid = responseHandler.GetParameter("openid");
                    string out_trade_no = responseHandler.GetParameter("out_trade_no");
                    string transactionId = responseHandler.GetParameter("transaction_id");

                    //支付结果
                    string trade_state = responseHandler.GetParameter("trade_state");

                    try
                    {
                        await OrderManager.PayCallback(out_trade_no, PayType.WeChat, transactionId);
                    }
                    catch(OrderPayedException e)
                    {

                    }
                    catch(Exception e)
                    {
                        throw e;
                    }
                    res = "success";
                }
                else
                {
                    res = "wrong";

                    //错误的订单处理
                }

                string xml = string.Format(
                    @"<xml><return_code><![CDATA[{0}]]></return_code><return_msg><![CDATA[{1}]]></return_msg></xml>",
                    return_code,
                    return_msg);
                return Content(xml, "text/xml");
            }
            catch (Exception e)
            {
                WeixinTrace.WeixinExceptionLog(new WeixinException(e.Message, e));
                throw e;
            }
        }

        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            if (errors == SslPolicyErrors.None)
                return true;

            return false;
        }
    }
}