﻿using Abp.Authorization;
using Abp.Authorization.Users;
using Application.Authorization;
using Application.Authorization.Models;
using Application.Authorization.Users;
using Application.Captchas;
using Application.Configuration;
using Application.Controllers;
using Application.MultiTenancy;
using Application.Web.Host.Areas.Client.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Web.Host.Areas.Client.Controllers
{
    [Area("Client")]
    public class AccountController : AccountControllerBase
    {
        public ApplicationAuthorizationHelper ApplicationAuthorizationHelper { get; set; }

        [HttpPost]
        public async Task<LoginResultOutput> LoginByPhoneNumber([FromBody] LoginByPhoneNumberInput input)
        {
            await VerifyLoginPhoneCaptcha(new VerifyLoginPhoneCaptchaInput { PhoneNumber = input.PhoneNumber, Code = input.Captcha });

            var loginResult = await GetLoginResultAsync(input.PhoneNumber, input.TenancyName);
            var accessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity));

            return new LoginResultOutput
            {
                AccessToken = accessToken,
                EncryptedAccessToken = GetEncrpyedAccessToken(accessToken),
                ExpireInSeconds = (int)Configuration.Expiration.TotalSeconds,
                UserId = loginResult.User.Id
            };
        }

        [AbpAuthorize]
        [HttpPost]
        public async Task BindPhoneNumber([FromBody] LoginByPhoneNumberInput input)
        {
            await VerifyLoginPhoneCaptcha(new VerifyLoginPhoneCaptchaInput { PhoneNumber = input.PhoneNumber, Code = input.Captcha });
            User user = ApplicationAuthorizationHelper.CurrentUser;
            user.PhoneNumber = input.PhoneNumber;
            UserManager.UserRepository.Update(user);
        }

        [HttpPost]
        public async Task SendLoginPhoneCaptcha([FromBody] SendLoginPhoneCaptchaInput input)
        {
            var code = CaptchaHelper.CreateCode();
            await RecaptchaValidator.Send("Phone", input.PhoneNumber, code);
        }

        [HttpPost]
        public async Task VerifyLoginPhoneCaptcha(VerifyLoginPhoneCaptchaInput input)
        {
            await RecaptchaValidator.ValidateAsync("Phone", input.PhoneNumber, input.Code);
        }

        [HttpPost]
        public async Task<LoginResultOutput> LoginByCode([FromBody]LoginByCodeInput input)
        {
            Tenant tenant = TenantHelper.GetTenant();
            string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, tenant.Id);
            string appSecret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, tenant.Id);
            OAuthAccessTokenResult oAuthAccessTokenResult = OAuthApi.GetAccessToken(appId, appSecret, input.Code);
            OAuthUserInfo oAuthUserInfo = OAuthApi.GetUserInfo(oAuthAccessTokenResult.access_token, oAuthAccessTokenResult.openid);
            UserLoginInfo userLoginInfo = new UserLoginInfo("Weixin", oAuthAccessTokenResult.openid, "Weixin");
            AbpLoginResult<Tenant, User> loginResult = await LogInManager.LoginAsync(userLoginInfo, tenant.TenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    var accessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity));
                    return new LoginResultOutput
                    {
                        AccessToken = accessToken,
                        EncryptedAccessToken = GetEncrpyedAccessToken(accessToken),
                        ExpireInSeconds = (int)Configuration.Expiration.TotalSeconds,
                        UserId = loginResult.User.Id
                    };
                case AbpLoginResultType.UnknownExternalLogin:
                    RegisterInput model = new RegisterInput
                    {
                        UserName = oAuthUserInfo.openid,
                        EmailAddress = UserManager.CreateDefaultUserEmail(oAuthUserInfo.openid),
                        NickName = oAuthUserInfo.nickname,
                        Name = oAuthUserInfo.nickname,
                        Avatar = oAuthUserInfo.headimgurl,
                        Surname = oAuthUserInfo.nickname,
                        UserType = UserType.User,
                        UserSource = UserSource.ExternalLogin,
                    };
                    model.Password = Authorization.Users.User.DefaultPassword;
                    var user = await UserRegistrationManager.RegisterAsync(
                        model.Name,
                        model.Surname,
                        model.Avatar,
                        model.EmailAddress,
                        model.PhoneNumber,
                        model.UserName,
                        model.Password,
                        false
                        );
                    user.UserType = model.UserType;
                    user.NickName = model.NickName;
                    user.Logins = new List<UserLogin>
                    {
                        new UserLogin
                        {
                            LoginProvider = userLoginInfo.LoginProvider,
                            ProviderKey = userLoginInfo.ProviderKey,
                            TenantId = user.TenantId
                        }
                    };
                    await UnitOfWorkManager.Current.SaveChangesAsync();
                    loginResult = await LogInManager.LoginAsync(userLoginInfo, tenant.TenancyName);
                    accessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity));
                    return new LoginResultOutput
                    {
                        AccessToken = accessToken,
                        EncryptedAccessToken = GetEncrpyedAccessToken(accessToken),
                        ExpireInSeconds = (int)Configuration.Expiration.TotalSeconds,
                        UserId = loginResult.User.Id
                    };
                default:
                    throw AbpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                      loginResult.Result,
                      userLoginInfo.ProviderKey,
                      tenant.TenancyName
                  );
            }
        }

        private async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string phoneNumber, string tenancyName)
        {
            var loginResult = await LogInManager.LoginByPhoneNumberAsync(phoneNumber, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw AbpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, phoneNumber, tenancyName);
            }
        }
    }
}
