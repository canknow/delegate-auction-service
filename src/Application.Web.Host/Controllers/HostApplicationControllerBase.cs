﻿using Application.Controllers;

namespace Application.Web.Host.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class HostApplicationControllerBase : ApplicationControllerBase
    {
    }
}
