﻿using Abp.Configuration;
using Abp.UI;
using Abp.Web.Models;
using Application.Files;
using Application.IO.CloudStore;
using Application.IO.CloudStore.Qiniu;
using Application.MultiTenancy;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Application.Web.Host.Controllers
{
    public class ApplicationController : HostApplicationControllerBase
    {
        public TenantHelper TenantHelper { get; set; }
        public FileManager FileManager { get; set; }
        public IAppFolders AppFolders { get; set; }
        public ICloudStoreService CloudStoreService { get; set; }
        public PathHelper PathHelper { get; set; }

        public async Task<String> GetCloudStoreToken()
        {
            return await CloudStoreService.GetToken();
        }

        public async Task<ActionResult> Upload(FileUploadInput input)
        {
            if (input.MaxFileSize.HasValue && input.File.Length > input.MaxFileSize.Value)
            {
                throw new UserFriendlyException(L("FileWarnSizeLimit"));
            }
            var fileName = Path.GetFileName(input.File.FileName);
            fileName = Guid.NewGuid().ToString() + Path.GetExtension(input.File.FileName);
            var relativePath = AppFolders.UploadFolder + fileName;
            var filePath = PathHelper.GetAbsolutePath(relativePath);
            string abpFileInfoFilePath = relativePath;
            bool enableQiniu = false;
            Stream stream = input.File.OpenReadStream();
            bool isSavedToFile = false;
            
            if (input.File.ContentType.Contains("image"))
            {
                Image<Rgba32> image = Image.Load(stream);
                int width = image.Width;
                int height = image.Height;

                if (input.ShouldBeRatio.HasValue && ((image.Width / image.Height) != input.ShouldBeRatio.Value))
                {
                    throw new UserFriendlyException(L("PictureRatioBe", input.ShouldBeRatio.Value));
                }
                if (input.Ratio.HasValue)
                {
                    if (image.Width / image.Height >= input.Ratio.Value)
                    {
                        width = height * input.Ratio.Value;
                    }
                    else
                    {
                        height = image.Width / input.Ratio.Value;
                    }

                    image.Mutate(x => x.Resize(width, height).Grayscale());
                }
                image.Save(filePath);
                isSavedToFile = true;
            }

            if (AbpSession.TenantId.HasValue)
            {
                enableQiniu = SettingManager.GetSettingValue<bool>(QiniuSettings.Status) && input.UseQiniu;
            }

            if (enableQiniu)
            {
                if (isSavedToFile)
                {
                    string qiniuFileUrl = await CloudStoreService.Upload(filePath, fileName);
                    abpFileInfoFilePath = qiniuFileUrl;
                }
                else
                {
                    string qiniuFileUrl = await CloudStoreService.Upload(stream, fileName);
                    abpFileInfoFilePath = qiniuFileUrl;
                }
            }
            else if (!isSavedToFile)
            {
                using(FileStream fileStream = System.IO.File.Create(filePath))
                {
                    await stream.CopyToAsync(fileStream);
                    fileStream.Flush();
                    isSavedToFile = true;
                }
            }

            try
            {
                AbpFileInfo fileInfo = new AbpFileInfo()
                {
                    Name = fileName,
                    Path = abpFileInfoFilePath,
                    TenantId = AbpSession.TenantId
                };
                fileInfo = FileManager.Create(fileInfo);
                return Json(fileInfo);
            }
            catch (Exception exception)
            {
                return Json(new ErrorInfo(exception.Message));
            }
        }
    }
}
