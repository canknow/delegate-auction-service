﻿using Abp.AspNetCore.Configuration;
using Abp.AspNetCore.MultiTenancy;
using Abp.BackgroundJobs;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Threading.BackgroundWorkers;
using Application.Client;
using Application.Configuration;
using Application.EntityFrameworkCore;
using Application.Public;
using Application.Web.Host.MultiTenancy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Application.Web.Host.Startup
{
    [DependsOn(typeof(ApplicationWebCoreModule),
        typeof(ApplicationApplicationAdminModule),
        typeof(ApplicationApplicationAdminApplicationModule),
        typeof(ApplicationApplicationClientApplicationModule),
        typeof(ApplicationApplicationPublicApplicationModule))]
    public class ApplicationWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ApplicationWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.BackgroundJobs.UseHangfire();

            Configuration.MultiTenancy.Resolvers.Remove(typeof(DomainTenantResolveContributor));
            Configuration.MultiTenancy.Resolvers.Add(typeof(MyDomainTenantResolveContributor));
            Configuration.Modules.AbpWebCommon().MultiTenancy.DomainFormat = _appConfiguration["App:WebSiteRootAddressFormat"] ?? "http://localhost:62114/";

            Configuration.Modules.AbpAspNetCore()
              .CreateControllersForAppServices(
              typeof(ApplicationApplicationPublicApplicationModule).GetAssembly(), "public");


            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                typeof(ApplicationApplicationAdminModule).GetAssembly(), "admin");

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                typeof(ApplicationApplicationAdminApplicationModule).GetAssembly(), "admin");

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                typeof(ApplicationApplicationClientApplicationModule).GetAssembly(), "client");
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!IocManager.Resolve<IMultiTenancyConfig>().IsEnabled)
            {
                return;
            }

            using (var scope = IocManager.CreateScope())
            {
                if (!scope.Resolve<DatabaseCheckHelper>().Exist(_appConfiguration["ConnectionStrings:Default"]))
                {
                    return;
                }
            }

            BackgroundJobManager backgroundJobManager = IocManager.Resolve<BackgroundJobManager>();

            var workManager = IocManager.Resolve<IBackgroundWorkerManager>();
        }
    }
}
