﻿using Application.Authorization.Users;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Application.Groups.Dto
{
    [AutoMapFrom(typeof(User))]
    public class CustomerDto : EntityDto<long>
    {
        public string NickName { get; set; }

        public string Number { get; set; }

        public string UserName { get; set; }

        public string Avatar { get; set; }

        public UserSource Source { get; set; }

        public bool IsSpreader { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
