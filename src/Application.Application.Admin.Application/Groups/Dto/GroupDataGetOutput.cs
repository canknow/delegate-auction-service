﻿namespace Application.Groups.Dto
{
    public class GroupDataGetOutput
    {
        public decimal MonthSales { get; set; }

        public decimal TotalIncome { get; set; }

        public int MonthOrder { get; set; }
    }
}
