﻿using Abp.Application.Services.Dto;

namespace Application.Groups.Dto
{
    public class CustomerGetAllInput : PagedAndSortedResultRequestDto
    {
        public int? Depth { get; set; } = 1;

        public bool ShouldBePotential { get; set; } = false;

        public string Filter { get; set; }
    }
}
