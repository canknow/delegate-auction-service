﻿using Application.Groups.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;

namespace Application.Groups
{
    public interface IGroupAppService: IApplicationService
    {
        GroupDataGetOutput GetGroupData();

        PagedResultDto<CustomerDto> GetCustomers(CustomerGetAllInput input);
    }
}
