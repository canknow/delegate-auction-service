﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Authorization.Users;
using Application.Distributions;
using Application.Groups.Dto;
using Application.Wallets;
using System.Linq;

namespace Application.Groups
{
    [AbpAuthorize]
    public class GroupAppService :
        CrudAppServiceBase<User, CustomerDto, long, CustomerGetAllInput, EntityDto<long>, EntityDto<long>>,
        IGroupAppService
    {
        public GroupManager GroupManager { get; set; }
        public IRepository<WalletRecord> WalletRecordRepository { get; set; }
        public IRepository<OrderDistribution> OrderDistributionRepository { get; set; }

        public GroupAppService(IRepository<User, long> repository) : base(repository)
        {

        }

        public GroupDataGetOutput GetGroupData()
        {
            GroupDataGetOutput groupDataGetOutput = new GroupDataGetOutput()
            {
               
            };
            groupDataGetOutput.TotalIncome = WalletRecordRepository.GetAll().Where(model => model.UserId == AbpSession.UserId.Value
             && model.Type == WalletRecordType.Recharge).Sum(model => (decimal?)model.Money).GetValueOrDefault();
            groupDataGetOutput.MonthOrder = OrderDistributionRepository.GetAll().Where(model => model.UserId == AbpSession.UserId.Value).Count();
            groupDataGetOutput.MonthSales= OrderDistributionRepository.GetAll().Where(model => model.UserId == AbpSession.UserId.Value).Sum(model=>(decimal?)model.Money).GetValueOrDefault();
            return groupDataGetOutput;
        }

        public PagedResultDto<CustomerDto> GetCustomers(CustomerGetAllInput input)
        {
            var query = Repository.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.Filter), model => model.NickName.Contains(input.Filter))
                .WhereIf(input.Depth == 1, model => model.ParentUserId == AbpSession.UserId.Value)
                .WhereIf(input.ShouldBePotential, model => model.IsSpreader == false)
                .WhereIf(input.Depth == 2, model => model.ParentUserId.HasValue && model.ParentUser.ParentUserId == AbpSession.UserId.Value)
                .WhereIf(input.Depth == 3, model => model.ParentUserId.HasValue && model.ParentUser.ParentUserId.HasValue && model.ParentUser.ParentUser.ParentUserId == AbpSession.UserId.Value);
            var totalCount = query.Count();
            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = query.ToList();

            return new PagedResultDto<CustomerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
    }
}
