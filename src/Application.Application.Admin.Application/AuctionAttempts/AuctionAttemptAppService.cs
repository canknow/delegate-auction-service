﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Application.AuctionAttempts.Dto;
using Application.Orders;
using System.Linq;
using System.Threading.Tasks;

namespace Application.AuctionAttempts
{
    public class AuctionAttemptAppService : CrudAppService<AuctionAttempt, AuctionAttemptDto, int, AuctionAttemptGetAllInput>, IAuctionAttemptAppService
    {
        public OrderManager OrderManager { get; set; }
        public IRepository<Order> OrderRepository { get; set; }

        public AuctionAttemptAppService(IRepository<AuctionAttempt> respository) : base(respository)
        {

        }

        protected override IQueryable<AuctionAttempt> CreateFilteredQuery(AuctionAttemptGetAllInput input)
        {
            return Repository.GetAll().Where(model=>model.OrderId==input.OrderId);
        }

        public async Task<AuctionAttemptDto> CreateAsync(AuctionAttemptDto input)
        {
            var order = OrderRepository.Get(input.OrderId);
            await OrderManager.Attempt(order);
            var entity = input.MapTo<AuctionAttempt>();
            Repository.Insert(entity);
            CurrentUnitOfWork.SaveChanges();

            if (input.Result==AuctionAttemptResult.Success)
            {
                var attemptCount = Repository.GetAll().Where(model => model.OrderId == input.OrderId).Count();
                await OrderManager.CompleteAuction(input.OrderId, entity, attemptCount, input.Shot);
            }
            else if(input.Result == AuctionAttemptResult.Fail)
            {
                await OrderManager.FailAuction(order);
            }
            else if (input.Result == AuctionAttemptResult.Lose)
            {
                await OrderManager.LoseAuction(order);
            }
            return entity.MapTo<AuctionAttemptDto>();
        }
    }
}
