﻿using Abp.Application.Services;
using Application.AuctionAttempts.Dto;
using System.Threading.Tasks;

namespace Application.AuctionAttempts
{
    public interface IAuctionAttemptAppService : ICrudAppService<AuctionAttemptDto, int, AuctionAttemptGetAllInput>
    {
        Task<AuctionAttemptDto> CreateAsync(AuctionAttemptDto input);
    }
}