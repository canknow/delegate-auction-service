﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Application.AuctionAttempts.Dto
{
    [AutoMap(typeof(AuctionAttempt))]
    public class AuctionAttemptDto : FullAuditedEntityDto
    {
        public int OrderId { get; set; }

        public AuctionAttemptResult Result { get; set; }

        public DateTime AuctionDateTime { get; set; }

        public string Shot { get; set; }

        public int? BidId { get; set; }
    }
}
