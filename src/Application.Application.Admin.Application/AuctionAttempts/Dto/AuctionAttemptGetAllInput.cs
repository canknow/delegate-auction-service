﻿using Abp.Application.Services.Dto;

namespace Application.AuctionAttempts.Dto
{
    public class AuctionAttemptGetAllInput : PagedAndSortedResultRequestDto
    {
        public int OrderId { get; set; }
    }
}
