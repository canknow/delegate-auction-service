﻿using Application.PictureTemplates.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;

namespace Application.PictureTemplates
{
    public interface IPictureTemplateAppService : ICrudAppService<PictureTemplateDto>
    {
        ListResultDto<PictureTemplateDto> GetAllWithoutPage();

        CreateOrEditPictureTemplateDto GetPictureTemplateForCreateOrEdit(NullableIdDto input);

        CreateOrEditPictureTemplateDto CreateOrEdit(CreateOrEditPictureTemplateDto input);
    }
}
