﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace Application.PictureTemplates.Dto
{
    [AutoMap(typeof(PictureTemplate))]
    public class CreateOrEditPictureTemplateDto : NullableIdDto
    {
        public string Name { get; set; }

        public string Template { get; set; }

        public string DemoText { get; set; }

        public string DemoPicture { get; set; }

        public PictureTemplateType Type { get; set; }

        public List<TemplateParameterDto> Parameters { get; set; }

        public bool IsDefault { get; set; }

        public bool Status { get; set; }
    }
}
