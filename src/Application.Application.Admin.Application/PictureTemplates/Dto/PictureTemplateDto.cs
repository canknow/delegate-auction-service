﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Application.PictureTemplates.Dto
{
    [AutoMap(typeof(PictureTemplate))]
    public class PictureTemplateDto : AuditedEntityDto
    {
        [Required]
        public string Name { get; set; }

        public PictureTemplateType Type { get; set; }

        [Required]
        public string Template { get; set; }

        public List<TemplateParameterDto> Parameters { get; set; }
    }
}
