﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace Application.Packages.Dto
{
    [AutoMapTo(typeof(PackageUpdateMapDto), typeof(Package))]
    public class PackageUpdateDto:EntityDto
    {
        public string Name { get; set; }

        public int? TeamId { get; set; }

        public string Cover { get; set; }

        public PackageType PackageType { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public float Score { get; set; }

        public int HintCount { get; set; }

        public int OrderCount { get; set; }

        public List<CompensationRuleDto> CompensationRules { get; set; }

        public List<DistributionDto> Distributions { get; set; }

        public bool IsHot { get; set; }
    }

    [AutoMapTo(typeof(Package))]
    public class PackageUpdateMapDto : EntityDto
    {
        public string Name { get; set; }

        public int? TeamId { get; set; }

        public string Cover { get; set; }

        public PackageType PackageType { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public float Score { get; set; }

        public int HintCount { get; set; }

        public int OrderCount { get; set; }


        public bool IsHot { get; set; }
    }
}
