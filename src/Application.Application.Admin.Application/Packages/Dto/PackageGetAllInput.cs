﻿using Abp.Application.Services.Dto;

namespace Application.Packages.Dto
{
    public class PackageGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
    }
}
