﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Teams;

namespace Application.Packages.Dto
{
    [AutoMapFrom(typeof(Team))]
    public class TeamDto:EntityDto
    {
        public string Name { get; set; }

        public string Logo { get; set; }

        public string Level { get; set; }

        public string Introduce { get; set; }

        public string Illustration { get; set; }

        public float Rate { get; set; }

        public int DealCount { get; set; }

        public int HintCount { get; set; }

        public int Quota { get; set; }
    }
}
