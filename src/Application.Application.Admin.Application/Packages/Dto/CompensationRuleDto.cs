﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Packages.Dto
{
    [AutoMap(typeof(CompensationRule))]
    public class CompensationRuleDto: NullableIdDto
    {
        public int Count { get; set; }

        public int Money { get; set; }
    }
}
