﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Distributions;

namespace Application.Packages.Dto
{
    [AutoMap(typeof(Distribution))]
    public class DistributionDto: NullableIdDto
    {
        public int PackageId { get; set; }

        public BuyWhen BuyWhen { get; set; }

        public int Level { get; set; }

        public decimal Money { get; set; }

        public float Ratio { get; set; }
    }
}
