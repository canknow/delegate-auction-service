﻿using Abp.Application.Services;
using Application.Packages.Dto;

namespace Application.Packages
{
    public interface IPackageAppService : ICrudAppService<PackageDto, int, PackageGetAllInput, PackageUpdateDto>
    {
    }
}