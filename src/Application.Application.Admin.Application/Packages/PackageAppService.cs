﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using Application.Distributions;
using Application.Packages.Dto;
using System.Linq;

namespace Application.Packages
{
    public class PackageAppService : CrudAppService<Package, PackageDto, int, PackageGetAllInput, PackageUpdateDto>, IPackageAppService
    {
        public PackageAppService(IPackageRepository respository) : base(respository)
        {

        }

        protected override IQueryable<Package> CreateFilteredQuery(PackageGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Name), model => model.Name == input.Name);
        }

        public override PackageDto Update(PackageUpdateDto input)
        {
            CheckUpdatePermission();

            var entity = Repository.GetAllIncluding().FirstOrDefault(c => c.Id == input.Id);

            PackageUpdateMapDto packageUpdateMapDto = input.MapTo<PackageUpdateMapDto>();
            ObjectMapper.Map(packageUpdateMapDto, entity);

            UpdateDistributions(entity, input);
            UpdateCompensationRules(entity, input);

            return entity.MapTo<PackageDto>();
        }

        public void UpdateDistributions(Package entity, PackageUpdateDto input)
        {
            foreach (var distributionDto in input.Distributions)
            {
                var distribution = entity.Distributions.FirstOrDefault(model => model.Id == distributionDto.Id);
                if (distribution != null)
                {
                    ObjectMapper.Map(distributionDto, distribution);
                }
                else
                {
                    entity.Distributions.Add(distributionDto.MapTo<Distribution>());
                }
            }
            for (var i = 0; i < entity.Distributions.Count; i++)
            {
                var distribution = entity.Distributions.ElementAt(i);
                if (distribution.Id > 0)
                {
                    var distributionDto = input.Distributions.FirstOrDefault(model => model.Id.HasValue && model.Id == distribution.Id);
                    if (distributionDto == null)
                    {
                        entity.Distributions.Remove(distribution);
                    }
                }
            }
        }

        public void UpdateCompensationRules(Package entity, PackageUpdateDto input)
        {
            foreach (var compensationRuleDto in input.CompensationRules)
            {
                var compensationRule = entity.CompensationRules.FirstOrDefault(model => model.Id == compensationRuleDto.Id);
                if (compensationRule != null)
                {
                    ObjectMapper.Map(compensationRuleDto, compensationRule);
                }
                else
                {
                    entity.CompensationRules.Add(compensationRuleDto.MapTo<CompensationRule>());
                }
            }
            for (var i = 0; i < entity.CompensationRules.Count; i++)
            {
                var compensationRule = entity.CompensationRules.ElementAt(i);
                if (compensationRule.Id > 0)
                {
                    var compensationRuleDto = input.CompensationRules.FirstOrDefault(model => model.Id.HasValue && model.Id == compensationRule.Id);
                    if (compensationRuleDto == null)
                    {
                        entity.CompensationRules.Remove(compensationRule);
                    }
                }
            }
        }
    }
}
