﻿using Application.Configuration;
using Senparc.Weixin.MP.Containers;
using System.Threading.Tasks;

namespace Application
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class WechatAppServiceBase : ApplicationAppServiceBase
    {
        protected async Task<string> GetAccessTokenAsync()
        {
            string appId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, AbpSession.TenantId.Value);
            string appSecret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, AbpSession.TenantId.Value);
            string accessToken = AccessTokenContainer.TryGetAccessToken(appId, appSecret);
            return accessToken;
        }
    }
}
