﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Dto;
using Application.Wechat.AutoReplys.Dto;

namespace Application.Wechat.AutoReplys
{
    public interface IAutoReplyAppService : ICrudAppService<AutoReplyDto>
    {
        AutoReplyForCreateOrEditInput GetAutoReplyForCreateOrEdit(NullableIdDto input);

        void CreateOrEdit(AutoReplyForCreateOrEditInput input);

        void RemoveArticle(IdInput input);
    }
}
