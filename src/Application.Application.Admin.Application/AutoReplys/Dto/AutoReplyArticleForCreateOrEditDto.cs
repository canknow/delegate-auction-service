﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Wechat.AutoReplys.Dto
{
    [AutoMap(typeof(AutoReplyArticle))]
    public class AutoReplyArticleForCreateOrEditDto : NullableIdDto
    {
        public int? AutoReplyId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string PicUrl { get; set; }

        public string Url { get; set; }
    }
}
