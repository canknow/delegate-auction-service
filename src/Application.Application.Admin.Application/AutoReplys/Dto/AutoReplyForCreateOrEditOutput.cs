﻿using System.Collections.Generic;

namespace Application.Wechat.AutoReplys.Dto
{
    public class AutoReplyForCreateOrEditOutput
    {
        public AutoReplyForCreateOrEditDto AutoReply { get; set; }
        public ICollection<AutoReplyArticleForCreateOrEditDto> Articles { get; set; }
    }
}
