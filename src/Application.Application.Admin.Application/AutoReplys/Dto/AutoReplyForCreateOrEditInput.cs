﻿using System.Collections.Generic;

namespace Application.Wechat.AutoReplys.Dto
{
    public class AutoReplyForCreateOrEditInput
    {
        public  AutoReplyForCreateOrEditDto AutoReply { get; set; }

        public List<AutoReplyArticleForCreateOrEditDto> Articles { get; set; }
    }
}
