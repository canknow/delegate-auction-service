﻿using Abp.Application.Services;
using Application.Dto;
using Application.Orders.Dto;
using System.Threading.Tasks;

namespace Application.Orders
{
    public interface IOrderAppService : ICrudAppService<OrderDto, int, OrderGetAllInput>
    {
        Task Pass(OrderPassInput input);

        Task Deny(OrderDenyInput input);

        Task Pay(OrderPayInput input);

        Task Remark(RemarkInput input);

        Task Compensate(IdInput input);
    }
}