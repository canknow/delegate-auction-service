﻿using Abp.Application.Services.Dto;
using System;

namespace Application.Orders.Dto
{
    public class CompleteAuctionInput : EntityDto
    {
        public DateTime WinDateTime { get; set; }

        public int AttemptCount { get; set; }

        public string Shot { get; set; }
    }
}
