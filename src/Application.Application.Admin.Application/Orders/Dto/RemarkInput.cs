﻿using Abp.Application.Services.Dto;

namespace Application.Orders.Dto
{
    public class RemarkInput:EntityDto
    {
        public string Remark { get; set; }
    }
}
