﻿using Abp.Application.Services.Dto;

namespace Application.Orders.Dto
{
    public class OrderPassInput : EntityDto
    {
        public string Remark { get; set; }
    }
}
