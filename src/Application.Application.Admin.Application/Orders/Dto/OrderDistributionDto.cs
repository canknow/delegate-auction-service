﻿using Application.Distributions;
using Application.Groups.Dto;
using Abp.AutoMapper;

namespace Application.Orders.Dto
{
    [AutoMapFrom(typeof(OrderDistribution))]
    public class OrderDistributionDto
    {
        public decimal Money { get; set; }

        public CustomerDto User { get; set; }

        public DistributionDto Distribution { get; set; }
    }
}
