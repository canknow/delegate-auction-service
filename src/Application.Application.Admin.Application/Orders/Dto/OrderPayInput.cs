﻿using Abp.Application.Services.Dto;
using Application.Wallets;

namespace Application.Orders.Dto
{
    public class OrderPayInput: EntityDto
    {
        public PayType PayType { get; set; }

        public string OutTradeNo { get; set; }
    }
}
