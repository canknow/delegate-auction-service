﻿using Abp.AutoMapper;
using Application.Packages;

namespace Application.Orders.Dto
{
    [AutoMap(typeof(CompensationRule))]
    public class CompensationRuleDto
    {
        public int Count { get; set; }

        public int Money { get; set; }
    }
}
