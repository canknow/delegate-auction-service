﻿using Application.Distributions;
using Abp.AutoMapper;

namespace Application.Orders.Dto
{
    [AutoMapFrom(typeof(Distribution))]
    public class DistributionDto
    {
        public int Level { get; set; }

        public decimal Money { get; set; }

        public float Ratio { get; set; }
    }
}
