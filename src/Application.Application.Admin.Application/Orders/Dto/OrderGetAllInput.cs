﻿using Abp.Application.Services.Dto;

namespace Application.Orders.Dto
{
    public class OrderGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Number { get; set; }

        public int? AttemptCount { get; set; }

        public AuctionResult? AuctionResult { get; set; }

        public int? RemainAuctionCount { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string BidAccount { get; set; }
    }
}
