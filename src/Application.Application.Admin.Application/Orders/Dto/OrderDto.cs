﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Bids.Dto;
using Application.Packages.Dto;
using System.Collections.Generic;

namespace Application.Orders.Dto
{
    [AutoMap(typeof(Order))]
    public class OrderDto : FullAuditedEntityDto
    {
        public int? TeamId{get;set;}

        public string Title{get;set;}

        public decimal Money{get;set;}

        public string Number{get;set;}

        public OrderStatus OrderStatus { get;set;}

        public PaymentMethod PaymentMethod{get;set;}

        public CompensationStatus CompensationStatus { get; set; }

        public int BidId{get;set;}

        public string AuditRemark { get; set; }

        public string DenyReason { get; set; }

        public string OrderStatusText { get; set; }

        public PackageDto Package { get; set; }

        public TeamDto Team { get; set; }

        public BidDto Bid { get; set; }

        public AuctionRecordDto AuctionRecord { get; set; }

        public string Contract { get; set; }

        public List<OrderDistributionDto> OrderDistributions { get; set; }

        public int AuctionNumber { get; set; }

        public CompensationRuleDto CompensationRule { get; set; }

        public string AdminRemark { get; set; }

        public int AttemptCount { get; set; }

        public AuctionResult AuctionResult { get; set; }

        public int RemainAuctionCount { get; set; }
    }
}
