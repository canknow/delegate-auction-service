﻿using Abp.AutoMapper;
using Application.AuctionRecords;
using System;

namespace Application.Orders.Dto
{
    [AutoMap(typeof(AuctionRecord))]
    public class AuctionRecordDto
    {
        public string Shot { get; set; }

        public int AttemptCount { get; set; }

        public DateTime? WinDateTime { get; set; }
    }
}
