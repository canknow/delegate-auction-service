﻿using Abp.Application.Services.Dto;

namespace Application.Orders.Dto
{
    public class OrderDenyInput:EntityDto
    {
        public string Reason { get; set; }
    }
}
