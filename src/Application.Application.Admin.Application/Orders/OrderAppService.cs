﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.AuctionRecords;
using Application.Dto;
using Application.Orders.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Orders
{
    public class OrderAppService : CrudAppService<Order, OrderDto, int, OrderGetAllInput>, IOrderAppService
    {
        public OrderManager OrderManager { get; set; }
        public IRepository<AuctionRecord> AuctionRecordRespository { get; set; }
        public OrderAppService(IRepository<Order> respository) : base(respository)
        {

        }

        protected override IQueryable<Order> CreateFilteredQuery(OrderGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Number), model => model.Number == input.Number)
                .WhereIf(input.RemainAuctionCount.HasValue, model=>model.GetRemainAuctionCount() == input.RemainAuctionCount.Value)
                .WhereIf(!string.IsNullOrEmpty(input.FullName), model=>model.Bid.FullName.Contains(input.FullName))
                .WhereIf(!string.IsNullOrEmpty(input.PhoneNumber), model => model.Bid.PhoneNumber.Contains(input.PhoneNumber));
        }

        public async Task Pass(OrderPassInput input)
        {
            Order order = await Repository.GetAsync(input.Id);
            await OrderManager.Pass(order);
        }

        public async Task Remark(RemarkInput input)
        {
            Order order = await Repository.GetAsync(input.Id);
            order.AdminRemark = input.Remark;
            await Repository.UpdateAsync(order);
        }

        public async Task Deny(OrderDenyInput input)
        {
            Order order = await Repository.GetAsync(input.Id);
            await OrderManager.Deny(order, input.Reason );
        }

        public async Task Pay(OrderPayInput input)
        {
            Order order = await Repository.GetAsync(input.Id);
            await OrderManager.PayedAsync(order, input.PayType, input.OutTradeNo);
        }

        public async Task Compensate(IdInput input)
        {
            Order order = await Repository.GetAsync(input.Id);
            await OrderManager.CompensateAsync(order);
        }
    }
}
