﻿using Application.ShopTemplates.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;
using System.Threading.Tasks;
using Application.Dto;

namespace Application.ShopTemplates
{
    public interface IShopTemplateAppService : ICrudAppService<ShopTemplateDto>
    {
        Task<ShopTemplateDto> GetShopTemplateOfShopAsync();

        Task<ShopTemplateCreateOrEditDto> GetShopTemplateForCreateOrEdit(NullableIdDto input);

        ShopTemplateCreateOrEditDto CreateOrUpdateShopTemplate(ShopTemplateCreateOrEditDto input);

        void SetAsDefault(IdInput input);
    }
}
