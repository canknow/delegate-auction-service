﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.ShopTemplates.Dto
{
    [AutoMap(typeof(ShopTemplate))]
    public class ShopTemplateCreateOrEditDto : NullableIdDto
    {
        public bool IsDefault { get; set; }

        public string Name { get; set; }

        public string Template { get; set; }
    }
}
