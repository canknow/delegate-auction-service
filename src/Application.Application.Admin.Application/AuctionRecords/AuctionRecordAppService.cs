﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.AuctionRecords.Dto;
using System.Linq;

namespace Application.AuctionRecords
{
    public class AuctionRecordAppService : CrudAppService<AuctionRecord, AuctionRecordDto, int, AuctionRecordGetAllInput, AuctionRecordDto, AuctionRecordUpdateDto>, IAuctionRecordAppService
    {
        public AuctionRecordAppService(IRepository<AuctionRecord> respository) : base(respository)
        {

        }

        protected override IQueryable<AuctionRecord> CreateFilteredQuery(AuctionRecordGetAllInput input)
        {
            return Repository.GetAll();
        }
    }
}
