﻿namespace Application.AuctionRecords.Dto
{
    public class UserDto
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string NickName { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string Avatar { get; set; }

        public string PhoneNumber { get; set; }
    }
}
