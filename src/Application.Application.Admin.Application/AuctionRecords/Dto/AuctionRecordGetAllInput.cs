﻿using Abp.Application.Services.Dto;

namespace Application.AuctionRecords.Dto
{
    public class AuctionRecordGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
    }
}
