﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Application.AuctionRecords.Dto
{
    [AutoMapTo(typeof(AuctionRecord))]
    public class AuctionRecordUpdateDto : EntityDto
    {
        public string Shot { get; set; }

        public DateTime? WinDateTime { get; set; }
    }
}
