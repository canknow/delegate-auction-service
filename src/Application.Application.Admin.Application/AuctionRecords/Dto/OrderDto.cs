﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Orders;

namespace Application.AuctionRecords.Dto
{
    [AutoMapFrom(typeof(Order))]
    public class OrderDto: EntityDto
    {
        public string Title { get; set; }

        public decimal Money { get; set; }

        public string Number { get; set; }
    }
}
