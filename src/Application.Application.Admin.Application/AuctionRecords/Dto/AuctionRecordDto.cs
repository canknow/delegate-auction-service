﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Packages.Dto;
using System;
using static Application.AuctionRecords.AuctionRecord;

namespace Application.AuctionRecords.Dto
{
    [AutoMap(typeof(AuctionRecord))]
    public class AuctionRecordDto : FullAuditedEntityDto
    {
        public AuctionRecordBid Bid { get; set; }

        public string Shot { get; set; }

        public int AttemptCount { get; set; }

        public DateTime? WinDateTime { get; set; }

        public int? PackageId { get; set; }

        public int OrderId { get; set; }

        public OrderDto Order { get; set; }

        public PackageDto Package { get; set; }

        public int? TeamId { get; set; }

        public TeamDto Team { get; set; }

        public UserDto User { get; set; }
    }
}
