﻿using Abp.Application.Services;
using Application.AuctionRecords.Dto;

namespace Application.AuctionRecords
{
    public interface IAuctionRecordAppService : ICrudAppService<AuctionRecordDto, int, AuctionRecordGetAllInput, AuctionRecordDto, AuctionRecordUpdateDto>
    {
    }
}