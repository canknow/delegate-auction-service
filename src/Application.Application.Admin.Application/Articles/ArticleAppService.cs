﻿using Application.Articles.Dto;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Linq;

namespace Application.Articles
{
    public class ArticleAppService : CrudAppService<Article, ArticleDto, int, ArticleGetAllInput>, IArticleAppService
    {
        public ArticleAppService(IRepository<Article> respository) : base(respository)
        {

        }

        protected override IQueryable<Article> CreateFilteredQuery(ArticleGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Title), model => model.Title == input.Title)
                .WhereIf(input.ArticleCategoryId.HasValue, model => model.ArticleCategoryId == input.ArticleCategoryId);
        }
    }
}
