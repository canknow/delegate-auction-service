﻿using Application.Articles.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;

namespace Application.Articles
{
    public interface IArticleCategoryAppService : ICrudAppService<ArticleCategoryDto>
    {
        ArticleCategoryCreateOrEditDto GetArticleCategoryForCreateOrEdit(NullableIdDto input);

        ArticleCategoryCreateOrEditDto CreateOrEdit(ArticleCategoryCreateOrEditDto input);
    }
}