﻿using Application.Authorization;
using Application.Features;
using Application.Articles;
using Application.Articles.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Features;
using Abp.Application.Services;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;

namespace Application.Articles
{
    public class ArticleCategoryAppService : CrudAppService<ArticleCategory, ArticleCategoryDto>, IArticleCategoryAppService
    {
        public ArticleCategoryAppService(IRepository<ArticleCategory> respository) : base(respository)
        {

        }

        public ArticleCategoryCreateOrEditDto GetArticleCategoryForCreateOrEdit(NullableIdDto input)
        {
            ArticleCategoryCreateOrEditDto articleCategory = new ArticleCategoryCreateOrEditDto();

            if (input.Id.HasValue)
            {
                articleCategory = Repository.Get(input.Id.Value).MapTo<ArticleCategoryCreateOrEditDto>();
            }
            return articleCategory;
        }

        public ArticleCategoryCreateOrEditDto CreateOrEdit(ArticleCategoryCreateOrEditDto input)
        {
            if (input.Id.HasValue)
            {
                CheckUpdatePermission();

                var entity = GetEntityById(input.Id.Value);
                ObjectMapper.Map(input, entity);
                CurrentUnitOfWork.SaveChanges();

                return entity.MapTo<ArticleCategoryCreateOrEditDto>();
            }
            else
            {
                CheckCreatePermission();

                var entity = input.MapTo<ArticleCategory>();

                Repository.Insert(entity);
                CurrentUnitOfWork.SaveChanges();

                return entity.MapTo<ArticleCategoryCreateOrEditDto>();
            }
        }
    }
}
