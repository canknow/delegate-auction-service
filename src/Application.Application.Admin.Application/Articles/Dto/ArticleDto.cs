﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Application.Articles.Dto
{
    [AutoMap(typeof(Article))]
    public class ArticleDto : FullAuditedEntityDto
    {
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }

        public string SubTitle { get; set; }

        [Required]
        [MaxLength(ApplicationConsts.MaxUrlLength)]
        public string Thumbnail { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public int? ArticleCategoryId { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [MaxLength(20)]
        public string Author { get; set; }

        public int Hint { get; set; }

        public int Like { get; set; }

        public int DisLike { get; set; }

        public int CommentCount { get; set; }

        [MaxLength(20)]
        public string KeyWord { get; set; }

        public bool IsRedirectExternal { get; set; }

        [MaxLength(ApplicationConsts.MaxUrlLength)]
        public string ExternalLink { get; set; }
        public ArticleStatus Status { get; set; }
    }
}
