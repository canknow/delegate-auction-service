﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Articles.Dto
{
    [AutoMap(typeof(ArticleCategory))]
    public class ArticleCategoryDto:EntityDto
    {
        public string Name { get; set; }

        public string Icon { get; set; }
    }
}
