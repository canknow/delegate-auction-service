﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Articles.Dto
{
    [AutoMap(typeof(ArticleCategory))]
    public class ArticleCategoryCreateOrEditDto: NullableIdDto
    {
        public string Name { get; set; }

        public string Icon { get; set; }
    }
}
