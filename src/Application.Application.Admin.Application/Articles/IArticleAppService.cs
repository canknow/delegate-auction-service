﻿using Application.Articles.Dto;
using Abp.Application.Services;

namespace Application.Articles
{
    public interface IArticleAppService : ICrudAppService<ArticleDto, int, ArticleGetAllInput>
    {
    }
}