﻿using Application.Admin.Banks.Dto;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.Banks;

namespace Application.Admin.Banks
{
    public class BankAppService:CrudAppService<Bank, BankDto>, IBankAppService
    {
        public BankAppService(IRepository<Bank> respository) :base(respository)
        {
        }
    }
}
