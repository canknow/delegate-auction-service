﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Banks;
using System.ComponentModel.DataAnnotations;

namespace Application.Admin.Banks.Dto
{
    [AutoMap(typeof(Bank))]
    public class BankDto : EntityDto
    {
        [Required]
        public string Name { get; set; }

        public string Icon { get; set; }
    }
}
