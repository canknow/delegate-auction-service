﻿using Abp.Application.Services;
using Application.Admin.Banks.Dto;

namespace Application.Admin.Banks
{
    public interface IBankAppService: ICrudAppService<BankDto>
    {
    }
}
