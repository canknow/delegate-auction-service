﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.UI;
using Application.Web;
using Application.Wechat.TemplateMessages;
using Application.Wechats.PublicWechats.Dto;
using Senparc.Weixin.Entities;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.Media;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using Senparc.Weixin.MP.CommonAPIs;
using Senparc.Weixin.MP.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Application.Wechat.PublicWechats
{
    [AbpAuthorize("Pages.Administration.Tenant.PublicWechat")]
    public class PublicWechatAppService : WechatAppServiceBase, IPublicWechatAppService
    {
        public WechatCommonManager WechatCommonManager { get; set; }
        public TemplateMessageManager TemplateMessageManager { get; set; }
        public PublicWechatManager PublicWechatManager { get; set; }
        public PathHelper PathHelper { get; set; }

        public async Task<string> RefreshAccessToken()
        {
            try
            {
                string accessToken = await WechatCommonManager.RefreshAccessToken();
                return accessToken;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public PublicWechatDto GetPublicWechat()
        {
            return PublicWechatManager.GetPublicWechatByTenantId(AbpSession.TenantId.Value).MapTo<PublicWechatDto>();
        }

        public async Task UpdatePublicWechat(PublicWechatDto input)
        {
            PublicWechat publicWechat = PublicWechatManager.GetPublicWechatByTenantId(AbpSession.TenantId.Value);
            ObjectMapper.Map(input, publicWechat);
            CurrentUnitOfWork.SaveChanges();
        }

        public async Task<List<GetPrivateTemplate_TemplateItem>> GetTemplateMessageTemplates()
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                GetPrivateTemplateJsonResult result = await TemplateApi.GetPrivateTemplateAsync(accessToken);
                return result.template_list;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task InstallTemplateMessageTempalte()
        {
            try
            {
                await TemplateMessageManager.Install(AbpSession.TenantId.Value);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<GetMenuResult> GetMenuAsync()
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                GetMenuResult result = CommonApi.GetMenu(accessToken);
                return result;
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task CreateMenuAsync(ConditionalButtonGroupCreateInput input)
        {
            string accessToken = await GetAccessTokenAsync();

            //重新整理按钮信息
            WxJsonResult result = null;
            try
            {
                if (input.matchrule != null)
                {
                    result = CommonApi.CreateMenuConditional(accessToken, input);
                }
                else
                {
                    result = CommonApi.CreateMenu(accessToken, input);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task DeleteMenu()
        {
            string accessToken = await GetAccessTokenAsync();
            var result = CommonApi.DeleteMenu(accessToken);
        }

        public async Task<PagedResultDto<MediaList_News_Item>> GetMediaNews(PagedResultRequestDto input)
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                MediaList_NewsResult mediaList_NewsResult = MediaApi.GetNewsMediaList(accessToken, input.SkipCount, input.MaxResultCount);
                GetMediaCountResultJson getMediaCountResultJson = await MediaApi.GetMediaCountAsync(accessToken);

                int totalCount = getMediaCountResultJson.news_count;
                return new PagedResultDto<MediaList_News_Item>(totalCount, mediaList_NewsResult.item);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<PagedResultDto<MediaList_Others_Item>> GetMediaImages(PagedResultRequestDto input)
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                MediaList_OthersResult mediaList_OthersResult = MediaApi.GetOthersMediaList(accessToken, UploadMediaFileType.image, input.SkipCount, input.MaxResultCount);
                GetMediaCountResultJson getMediaCountResultJson = await MediaApi.GetMediaCountAsync(accessToken);

                int totalCount = getMediaCountResultJson.image_count;
                return new PagedResultDto<MediaList_Others_Item>(totalCount, mediaList_OthersResult.item);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<PagedResultDto<MediaList_Others_Item>> GetMediaOthers(GetMediaOthersInput input)
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                MediaList_OthersResult mediaList_OthersResult = MediaApi.GetOthersMediaList(accessToken, input.Type, input.SkipCount, input.MaxResultCount);
                GetMediaCountResultJson getMediaCountResultJson = await MediaApi.GetMediaCountAsync(accessToken);

                int totalCount = getMediaCountResultJson.image_count;
                switch (input.Type)
                {
                    case UploadMediaFileType.image:
                        totalCount = getMediaCountResultJson.image_count;
                        break;
                    case UploadMediaFileType.video:
                        totalCount = getMediaCountResultJson.video_count;
                        break;
                    case UploadMediaFileType.voice:
                        totalCount = getMediaCountResultJson.voice_count;
                        break;
                    case UploadMediaFileType.news:
                        totalCount = getMediaCountResultJson.news_count;
                        break;
                }
                return new PagedResultDto<MediaList_Others_Item>(totalCount, mediaList_OthersResult.item);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<String> GetPictureById(GetMediaInput input)
        {
            MemoryStream stream = new MemoryStream();
            string accessToken = await GetAccessTokenAsync();
            MediaApi.GetForeverMedia(accessToken, input.MediaId, stream);
            return Utility.IO.StreamHelper.MemoryStreamToString(stream);
        }

        public async Task UploadMediaImage(UploadInput input)
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                string path = PathHelper.GetAbsolutePath(input.Path);
                UploadForeverMediaResult uploadForeverMediaResult = await MediaApi.UploadForeverMediaAsync(accessToken, path);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<UploadImgResult> UploadImgAsync(UploadInput input)
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                string path = PathHelper.GetAbsolutePath(input.Path);
                UploadImgResult uploadForeverMediaResult = await MediaApi.UploadImgAsync(accessToken, path);
                return uploadForeverMediaResult;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<List<ForeverNewsItem>> GetForeverNewsAsync(GetMediaInput input)
        {
            try
            {
                string accessToken = await GetAccessTokenAsync();
                GetNewsResultJson result = await MediaApi.GetForeverNewsAsync(accessToken, input.MediaId);
                return result.news_item;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}
