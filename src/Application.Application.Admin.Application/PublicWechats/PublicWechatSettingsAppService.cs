﻿using Abp.Authorization;
using Abp.Configuration;
using Abp.Timing;
using Abp.UI;
using Application.Configuration;
using Application.Web;
using Application.Wechat;
using Application.Wechat.PublicWechats;
using Application.Wechats.PublicWechats.Dto;
using System;
using System.Threading.Tasks;

namespace Application.Wechats.PublicWechats
{
    [AbpAuthorize("Pages.Administration.Tenant.PublicWechat")]
    public class PublicWechatSettingsAppService : ApplicationAppServiceBase, IPublicWechatSettingsAppService
    {
        readonly ISettingDefinitionManager _settingDefinitionManager;
        public WechatCommonManager WechatCommonManager { get; set; }
        public PublicWechatManager PublicWechatManager { get; set; }
        public ServerContextHelper ServerContextHelper { get; set; }

        public PublicWechatSettingsAppService(ISettingDefinitionManager settingDefinitionManager)
        {
            _settingDefinitionManager = settingDefinitionManager;
        }

        public async Task InstallCertificate (InstallCertificateInput input)
        {
            try
            {
                await WechatCommonManager.InstallCertificate(input.Password);
            }
            catch(Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<SettingsEditDto> GetSettings()
        {
            var timezone = await SettingManager.GetSettingValueForApplicationAsync(TimingSettingNames.TimeZone);
            var settings = new SettingsEditDto
            {
                General = new GeneralSettingsDto
                {
                    IPs= ServerContextHelper.GetServerIp(),
                    Token = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Token, AbpSession.TenantId.Value),
                    AppId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.AppId, AbpSession.TenantId.Value),
                    Secret = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.Secret, AbpSession.TenantId.Value),
                    MPVerifyFile = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.MPVerifyFile, AbpSession.TenantId.Value),
                    EncodingAESKey = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.EncodingAESKey, AbpSession.TenantId.Value),
                    SubscribeLink = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.General.SubscribeLink, AbpSession.TenantId.Value),
                },
                CustomerService = new CustomerServiceSettingsEditDto
                {
                    TriggerWord = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.CustomerService.TriggerWord, AbpSession.TenantId.Value),
                },
                Pay = new PaySettingsEditDto
                {
                    MchId = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.MchId, AbpSession.TenantId.Value),
                    Key = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.Key, AbpSession.TenantId.Value),
                    SslcertPath = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.Pay.SslcertPath, AbpSession.TenantId.Value)
                },
                TemplateMessage = new TemplateMessageEditDto
                {
                    NewCustomer = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.NewCustomer, AbpSession.TenantId.Value),
                    OrderCreated = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderCreated, AbpSession.TenantId.Value),
                    OrderPayed = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderPayed, AbpSession.TenantId.Value),
                    OrderShiped = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderShiped, AbpSession.TenantId.Value),
                    OrderRebate = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.OrderRebate, AbpSession.TenantId.Value),
                    WalletWithdraw = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.WalletWithdraw, AbpSession.TenantId.Value),
                    Upgrade = await SettingManager.GetSettingValueForTenantAsync(WechatSettings.TemplateMessage.Upgrade, AbpSession.TenantId.Value),
                }
            };
            return settings;
        }

        public async Task UpdateGeneral(GeneralSettingsDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.General.Token,
                input.Token);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.General.AppId,
                input.AppId);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.General.Secret,
                input.Secret);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.General.MPVerifyFile,
                input.MPVerifyFile);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.General.EncodingAESKey,
                input.EncodingAESKey);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.General.SubscribeLink,
                input.SubscribeLink);
            PublicWechat publicWechat = PublicWechatManager.GetPublicWechatByTenantId(AbpSession.TenantId.Value);
            ObjectMapper.Map(input, publicWechat.General);
        }

        public async Task UpdatePay (PaySettingsEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.Pay.MchId,
                input.MchId);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.Pay.Key,
                input.Key);
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.Pay.SslcertPath,
                input.SslcertPath);
            PublicWechat publicWechat = PublicWechatManager.GetPublicWechatByTenantId(AbpSession.TenantId.Value);
            ObjectMapper.Map(input, publicWechat.Pay);
        }

        public async Task UpdateCustomerService (CustomerServiceSettingsEditDto input)
        {
            await SettingManager.ChangeSettingForTenantAsync(
                AbpSession.TenantId.Value,
                WechatSettings.CustomerService.TriggerWord,
                input.TriggerWord);
            PublicWechat publicWechat = PublicWechatManager.GetPublicWechatByTenantId(AbpSession.TenantId.Value);
            ObjectMapper.Map(input, publicWechat.CustomerService);
        }
    }
}
