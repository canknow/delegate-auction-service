﻿using Application.Wechats.PublicWechats.Dto;
using Abp.Application.Services;
using System.Threading.Tasks;

namespace Application.Wechats.PublicWechats
{
    public interface IPublicWechatSettingsAppService : IApplicationService
    {
        Task InstallCertificate(InstallCertificateInput input);

        Task<SettingsEditDto> GetSettings();

        Task UpdateGeneral(GeneralSettingsDto input);

        Task UpdatePay(PaySettingsEditDto input);

        Task UpdateCustomerService(CustomerServiceSettingsEditDto input);
    }
}
