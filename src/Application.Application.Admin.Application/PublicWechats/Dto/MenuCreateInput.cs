﻿using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Menu;

namespace Application.Wechats.PublicWechats.Dto
{
    public class MenuCreateInput
    {
        public GetMenuResultFull ResultFull { get; set; }
        public MenuMatchRule MenuMatchRule { get; set; }

        public long? MenuId { get; set; }
    }
}
