﻿using Abp.AutoMapper;
using Application.Wechat.PublicWechats;

namespace Application.Wechats.PublicWechats.Dto
{
    [AutoMap(typeof(PublicWechat))]
    public class PublicWechatDto
    {
        public string Qrcode { get; set; }

        public string Name { get; set; }

        public string Account { get; set; }

        public string Password { get; set; }

        public string Description { get; set; }

        public General General { get; set; } = new General();

        public Pay Pay { get; set; } = new Pay();

        public CustomerService CustomerService { get; set; } = new CustomerService();
    }
}
