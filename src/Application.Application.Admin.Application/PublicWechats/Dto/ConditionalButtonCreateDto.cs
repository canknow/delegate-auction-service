﻿using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Menu;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Wechats.PublicWechats.Dto
{
    public class ConditionalButtonCreateDto: MenuFull_RootButton, IBaseButton
    {
    }
}
