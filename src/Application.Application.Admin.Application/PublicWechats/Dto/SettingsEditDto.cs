﻿using System.ComponentModel.DataAnnotations;

namespace Application.Wechats.PublicWechats.Dto
{
    public class SettingsEditDto
    {
        [Required]
        public GeneralSettingsDto General { get; set; }

        public CustomerServiceSettingsEditDto CustomerService { get; set; }

        public PaySettingsEditDto Pay { get; set; }

        public TemplateMessageEditDto TemplateMessage { get; set; }
    }
}
