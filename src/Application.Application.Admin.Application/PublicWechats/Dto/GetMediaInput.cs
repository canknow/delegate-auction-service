﻿namespace Application.Wechats.PublicWechats.Dto
{
    public class GetMediaInput
    {
        public string MediaId { get; set; }

        public string Type { get; set; }

        public GetMediaInput()
        {
            Type = "image";
        }
    }
}
