﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using Application.Wechat.PublicWechats;

namespace Application.Wechats.PublicWechats.Dto
{
    [AutoMap(typeof(General))]
    public class GeneralSettingsDto
    {
        public string Token { get; set; }

        public string AppId { get; set; }

        public string Secret { get; set; }

        public string MPVerifyFile { get; set; }

        public string EncodingAESKey { get; set; }

        [MaxLength(128)]
        public string SubscribeLink { get; set; }

        public string IPs { get; set; }
    }
}
