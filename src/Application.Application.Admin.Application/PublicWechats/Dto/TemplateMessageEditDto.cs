﻿namespace Application.Wechats.PublicWechats.Dto
{
    public class TemplateMessageEditDto
    {
        public string NewCustomer { get; set; }

        public string OrderCreated { get; set; }

        public string OrderPayed { get; set; }

        public string OrderShiped { get; set; }

        public string OrderRebate { get; set; }

        public string WalletWithdraw { get; set; }

        public string Upgrade { get; set; }
    }
}
