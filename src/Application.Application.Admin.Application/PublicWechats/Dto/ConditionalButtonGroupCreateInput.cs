﻿using Senparc.Weixin.MP.Entities.Menu;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Wechats.PublicWechats.Dto
{
    public class ConditionalButtonGroupCreateInput : ConditionalButtonGroup
    {
        public new List<ConditionalButtonCreateDto> button { get; set; }
    }
}
