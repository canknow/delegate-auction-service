﻿namespace Application.Wechats.PublicWechats.Dto
{
    public class UploadInput
    {
        public string Path { get; set; }
    }
}
