﻿using Abp.Application.Services.Dto;
using Senparc.Weixin.MP;

namespace Application.Wechats.PublicWechats.Dto
{
    public class GetMediaOthersInput:PagedAndSortedResultRequestDto
    {
        public UploadMediaFileType Type { get; set; }
    }
}
