﻿using Abp.AutoMapper;
using Application.Wechat.PublicWechats;

namespace Application.Wechats.PublicWechats.Dto
{
    [AutoMap(typeof(CustomerService))]
    public class CustomerServiceSettingsEditDto
    {
        public string TriggerWord { get; set; }
    }
}
