﻿using Abp.AutoMapper;
using Application.Wechat.PublicWechats;

namespace Application.Wechats.PublicWechats.Dto
{
    [AutoMap(typeof(Pay))]
    public class PaySettingsEditDto
    {
        public string MchId { get; set; }

        public string Key { get; set; }

        public string SslcertPath { get; set; }
    }
}
