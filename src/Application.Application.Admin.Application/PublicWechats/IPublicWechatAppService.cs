﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Wechats.PublicWechats.Dto;
using Senparc.Weixin.MP.AdvancedAPIs.Media;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using Senparc.Weixin.MP.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Wechat.PublicWechats
{
    public interface IPublicWechatAppService : IApplicationService
    {
        PublicWechatDto GetPublicWechat();

        Task UpdatePublicWechat(PublicWechatDto input);

        Task<GetMenuResult> GetMenuAsync();

        Task<string> RefreshAccessToken();

        Task CreateMenuAsync(ConditionalButtonGroupCreateInput input);

        Task<PagedResultDto<MediaList_News_Item>> GetMediaNews(PagedResultRequestDto input);

        Task<PagedResultDto<MediaList_Others_Item>> GetMediaImages(PagedResultRequestDto input);

        Task<PagedResultDto<MediaList_Others_Item>> GetMediaOthers(GetMediaOthersInput input);

        Task UploadMediaImage(UploadInput input);

        Task InstallTemplateMessageTempalte();

        Task<List<GetPrivateTemplate_TemplateItem>> GetTemplateMessageTemplates();

        Task<String> GetPictureById(GetMediaInput input);

        Task<UploadImgResult> UploadImgAsync(UploadInput input);

        Task<List<ForeverNewsItem>> GetForeverNewsAsync(GetMediaInput input);
    }
}
