﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Application
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(ApplicationCoreModule))]
    public class ApplicationApplicationAdminApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(CustomDtoMapper.CreateMappings);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApplicationApplicationAdminApplicationModule).GetAssembly());
        }
    }
}