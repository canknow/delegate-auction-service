﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Wallets.Dto;
using System.Linq;

namespace Application.Wallets
{
    [AbpAuthorize("Pages.Administration.Tenant.Finance")]
    public class WalletRecordAppService : CrudAppService<
        WalletRecord,
        WalletRecordDto,
        int,
        WalletRecordGetAllInput
        >, IWalletRecordAppService
    {
        public WalletManager WalletManager { get; set; }
        public WalletRecordAppService(IRepository<WalletRecord> repository) :base(repository)
        {

        }

        protected override IQueryable<WalletRecord> CreateFilteredQuery(WalletRecordGetAllInput input)
        {
            return Repository.GetAll()
                .WhereIf(input.UserId.HasValue, model => model.UserId == input.UserId)
                .WhereIf(input.Status != null, model => model.Status == input.Status)
                .WhereIf(input.StartTime != null, model => model.CreationTime >= input.StartTime)
                .WhereIf(input.EndTime != null, model => model.CreationTime <= input.EndTime);
        }
    }
}
