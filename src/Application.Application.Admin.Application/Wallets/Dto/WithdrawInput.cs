﻿namespace Application.Wallets.Dto
{
    public class WithdrawInput
    {
        public int Id { get; set; }
        public WithdrawMethod? WithdrawMethod { get; set; }
    }
}
