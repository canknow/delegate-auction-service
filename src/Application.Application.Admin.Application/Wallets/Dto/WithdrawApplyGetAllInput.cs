﻿using Abp.Application.Services.Dto;
using System;

namespace Application.Wallets.Dto
{
    public class WithdrawApplyGetAllInput : PagedAndSortedResultRequestDto
    {
        public long? UserId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public WithdrawStatus? Status { get; set; }

        public string SerialNumber { get; set; }

        public int? BankId { get; set; }
    }
}
