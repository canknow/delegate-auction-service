﻿using Application.Wallets.Dto;
using Abp.Application.Services.Dto;

namespace Application.Finance.Wallets.End.Dto
{
    public class WithdrawApplyGetAllOutput
    {
        public PagedResultDto<WithdrawApplyDto> PagedResult { get; set; }

        public decimal TotalMoney { get; set; }
    }
}
