﻿using Application.Admin.Banks.Dto;
using Application.FinanceAccounts.Entities;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Wallets.Dto
{
    [AutoMapFrom(typeof(FinanceAccount))]
    public class FinanceAccountDto : EntityDto
    {
        public string Account { get; set; }

        public bool IsDefault { get; set; }

        public string FullName { get; set; }

        public FinanceAccountType Type { get; set; }

        public string TypeText { get; set; }

        public BankDto Bank { get; set; }
    }
}
