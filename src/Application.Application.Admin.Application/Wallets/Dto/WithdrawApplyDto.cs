﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Groups.Dto;

namespace Application.Wallets.Dto
{
    [AutoMap(typeof(WithdrawApply))]
    public class WithdrawApplyDto : AuditedEntityDto
    {
        public CustomerDto User { get; set; }

        public string SerialNumber { get; set; }

        public decimal Money { get; set; }

        public PayType? PayType { get; set; }

        public string PayTypeText { get; set; }

        public WithdrawMethod WithdrawMethod { get; set; }

        public WithdrawStatus Status { get; set; }

        public string StatusText { get; set; }

        public string WithdrawMethodText { get; set; }

        public string FailReason { get; set; }

        public virtual FinanceAccountDto FinanceAccount { get; set; }
    }
}
