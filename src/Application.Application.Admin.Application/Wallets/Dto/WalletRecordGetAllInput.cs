﻿using Abp.Application.Services.Dto;
using System;

namespace Application.Wallets.Dto
{
    public class WalletRecordGetAllInput : PagedAndSortedResultRequestDto
    {
        public long? UserId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public WalletRecordStatus? Status { get; set; }
    }
}
