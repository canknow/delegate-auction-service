﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Application.Dto;
using Application.Finance.Wallets.End.Dto;
using Application.Wallets.Dto;
using Application.Wallets.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Wallets
{
    [AbpAuthorize("Pages.Administration.Tenant.Finance")]
    public class WithdrawApplyService : CrudAppService<
        WithdrawApply,
        WithdrawApplyDto,
        int,
        WithdrawApplyGetAllInput
        >, IWithdrawApplyService
    {
        public WalletManager WalletManager { get; set; }
        public WithdrawManager WithdrawManager { get; set; }
        public WithdrawApplyListExcelExporter WithdrawApplyListExcelExporter { get; set; }

        public WithdrawApplyService(IRepository<WithdrawApply> repository) :base(repository)
        {

        }

        protected override IQueryable<WithdrawApply> CreateFilteredQuery(WithdrawApplyGetAllInput input)
        {
            return Repository.GetAll()
                .WhereIf(input.UserId.HasValue, model => model.UserId == input.UserId)
                .WhereIf(input.BankId.HasValue, model => model.FinanceAccount!=null&&model.FinanceAccount.BankId == input.BankId)
                .WhereIf(!string.IsNullOrEmpty(input.SerialNumber), model => model.SerialNumber == input.SerialNumber)
                .WhereIf(input.Status.HasValue, model => model.Status == input.Status)
                .WhereIf(input.StartTime != null, model => model.CreationTime >= input.StartTime)
                .WhereIf(input.EndTime != null, model => model.CreationTime <= input.EndTime);
        }

        public WithdrawApplyGetAllOutput GetWithdrawApplyAllOfPage(WithdrawApplyGetAllInput input)
        {
            WithdrawApplyGetAllOutput withdrawApplyGetAllOutput = new WithdrawApplyGetAllOutput()
            {
                PagedResult = GetAll(input)
            };
            var query = CreateFilteredQuery(input);
            withdrawApplyGetAllOutput.TotalMoney = query.OrderByDescending(model=>model.Id)
                .Sum(model => (decimal?)model.Money).GetValueOrDefault();
            return withdrawApplyGetAllOutput;
        }

        public FileDto GetWithdrawApplyToExcel(WithdrawApplyGetAllInput input)
        {
            var query = CreateFilteredQuery(input);
            var withdrawApplys = query.ToList();
            var list = withdrawApplys.MapTo<List<WithdrawApplyDto>>();
            return WithdrawApplyListExcelExporter.ExportToFile(list);
        }

        public async Task Withdraw(WithdrawInput input)
        {
            try
            {
                if (input.WithdrawMethod.HasValue)
                {
                    await WithdrawManager.ProcessWithdrawAsync(input.Id,input.WithdrawMethod.Value);
                }
                else
                {
                    await WithdrawManager.ProcessWithdrawAsync(input.Id);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}
