﻿using Application.Finance.Wallets.End.Dto;
using Application.IO;
using Application.Wallets.Dto;
using Abp.Application.Services;
using System.Threading.Tasks;
using Application.Dto;

namespace Application.Wallets
{
    public interface IWithdrawApplyService : ICrudAppService<
        WithdrawApplyDto,
        int,
        WithdrawApplyGetAllInput>
    {
        WithdrawApplyGetAllOutput GetWithdrawApplyAllOfPage(WithdrawApplyGetAllInput input);

        FileDto GetWithdrawApplyToExcel(WithdrawApplyGetAllInput input);

        Task Withdraw(WithdrawInput input);
    }
}
