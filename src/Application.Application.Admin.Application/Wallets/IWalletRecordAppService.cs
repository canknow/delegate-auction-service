﻿using Application.Finance.Wallets.End.Dto;
using Application.Wallets.Dto;
using Abp.Application.Services;

namespace Application.Wallets
{
    public interface IWalletRecordAppService : ICrudAppService<
        WalletRecordDto,
        int,
        WalletRecordGetAllInput>
    {
    }
}
