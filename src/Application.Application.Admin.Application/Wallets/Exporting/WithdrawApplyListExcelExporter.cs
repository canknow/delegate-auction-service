﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Application.Dto;
using Application.IO;
using Application.Wallets.Dto;
using System.Collections.Generic;

namespace Application.Wallets.Exporting
{
    public class WithdrawApplyListExcelExporter : EpPlusExcelExporterBase, IWithdrawApplyListExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _infrastructureSession;

        public WithdrawApplyListExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession infrastructureSession)
        {
            _timeZoneConverter = timeZoneConverter;
            _infrastructureSession = infrastructureSession;
        }

        public FileDto ExportToFile(List<WithdrawApplyDto> dtos)
        {
            return CreateExcelPackage(
                "WithdrawApplyList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("WithdrawApply"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("SerialNumber"),
                        L("Money"),
                        L("Status"),
                        L("WithdrawMethod"),
                        L("FailReason")
                        );

                    AddObjects(
                        sheet,
                        2, 
                        dtos,
                        _ => _.SerialNumber,
                        _ => _.Money,
                        _ => L(_.StatusText),
                        _ => L(_.WithdrawMethodText),
                        _ => _.FailReason
                        );

                    //Formatting cells
                    for (var i = 1; i <= 11;i ++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
