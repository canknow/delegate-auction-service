﻿using Application.Dto;
using Application.Wallets.Dto;
using System.Collections.Generic;

namespace Application.Wallets.Exporting
{
    public interface IWithdrawApplyListExcelExporter
    {
        FileDto ExportToFile(List<WithdrawApplyDto> dtos);
    }
}
