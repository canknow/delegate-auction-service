﻿using Application.Shops.Dtos;
using Abp.Application.Services;
using System.Threading.Tasks;

namespace Application.Shops
{
    public interface IShopAppService : ICrudAppService<ShopDto>
    {
        ShopDto GetShop();

        ShopDto UpdateShop(ShopUpdateInput input);
    }
}