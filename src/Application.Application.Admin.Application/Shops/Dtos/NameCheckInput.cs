﻿namespace Application.Shops.Dtos
{
    public class NameCheckInput
    {
        public string Name { get; set; }
    }
}
