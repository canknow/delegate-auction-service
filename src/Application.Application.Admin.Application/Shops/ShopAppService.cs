﻿using Application.Authorization;
using Application.Authorization.Users;
using Application.Editions;
using Application.MultiTenancy;
using Application.Notifications;
using Application.Shops.Dtos;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Configuration.Startup;
using Abp.Domain.Repositories;

namespace Application.Shops
{
    public class ShopAppService : CrudAppService<Shop, ShopDto>, IShopAppService
    {
        public ShopManager ShopManager { get; set; }
        public IMultiTenancyConfig _multiTenancyConfig { get; set; }
        public TenantManager _tenantManager { get; set; }
        public UserManager _userManager { get; set; }
        public LogInManager _logInManager { get; set; }
        public EditionManager _editionManager { get; set; }
        public IAppNotifier _appNotifier { get; set; }

        public ShopAppService(IRepository<Shop> respository) : base(respository)
        {

        }

        public ShopDto GetShop()
        {
            return ShopManager.GetShop().MapTo<ShopDto>();
        }

        public ShopDto UpdateShop(ShopUpdateInput input)
        {
            Shop entity = ShopManager.GetShop();
            ObjectMapper.Map(input, entity);
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(entity);
        }
    }
}
