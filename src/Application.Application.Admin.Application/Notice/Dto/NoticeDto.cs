﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Application.Notices.Dto
{
    [AutoMap(typeof(Notice))]
    public class NoticeDto : FullAuditedEntityDto
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public NoticeStatus Status { get; set; }
    }
}
