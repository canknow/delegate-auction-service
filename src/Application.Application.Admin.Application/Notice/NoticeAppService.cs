﻿using Application.Notices.Dto;
using Abp.Application.Services;
using Abp.Domain.Repositories;

namespace Application.Notices
{
    public class NoticeAppService:CrudAppService<Notice, NoticeDto>, INoticeAppService
    {
        public NoticeAppService(IRepository<Notice> respository) :base(respository)
        {
        }
    }
}
