﻿using Abp.Application.Services;
using Application.Notices.Dto;

namespace Application.Notices
{
    public interface INoticeAppService: ICrudAppService<NoticeDto>
    {
    }
}
