﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.Bids.Dto;
using System.Linq;

namespace Application.Bids
{
    public class BidAppService : CrudAppService<Bid, BidDto, int, BidGetAllInput>, IBidAppService
    {
        public BidAppService(IRepository<Bid> respository) : base(respository)
        {

        }

        protected override IQueryable<Bid> CreateFilteredQuery(BidGetAllInput input)
        {
            return Repository.GetAll();
        }
    }
}
