﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Bids.Dto
{
    [AutoMap(typeof(Bid))]
    public class BidDto : FullAuditedEntityDto
    {
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string IDNumber { get; set; }

        public string Account { get; set; }

        public string Password { get; set; }

        public string BidPicture { get; set; }

        public string FrontOfIDCard { get; set; }

        public string BackOfIDCard { get; set; }
    }
}
