﻿using Abp.Application.Services;
using Application.Bids.Dto;

namespace Application.Bids
{
    public interface IBidAppService : ICrudAppService<BidDto, int, BidGetAllInput>
    {
    }
}