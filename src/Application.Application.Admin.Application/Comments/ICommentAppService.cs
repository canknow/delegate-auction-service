﻿using Abp.Application.Services;
using Application.Comments.Dto;

namespace Application.Comments
{
    public interface ICommentAppService : ICrudAppService<CommentDto>
    {
    }
}
