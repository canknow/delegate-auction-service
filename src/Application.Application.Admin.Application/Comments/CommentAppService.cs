﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.Comments.Dto;

namespace Application.Comments
{
    public class CommentAppService : CrudAppService<Comment, CommentDto>,
        ICommentAppService
    {
        public CommentAppService(IRepository<Comment> respository)
            : base(respository)
        {
        }
    }
}
