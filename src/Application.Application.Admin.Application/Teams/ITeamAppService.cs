﻿using Abp.Application.Services;
using Application.Teams.Dto;

namespace Application.Teams
{
    public interface ITeamAppService : ICrudAppService<TeamDto, int, TeamGetAllInput>
    {
    }
}