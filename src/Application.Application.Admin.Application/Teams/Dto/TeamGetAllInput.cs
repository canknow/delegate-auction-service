﻿using Abp.Application.Services.Dto;

namespace Application.Teams.Dto
{
    public class TeamGetAllInput : PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
    }
}
