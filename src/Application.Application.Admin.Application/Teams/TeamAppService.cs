﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Application.Teams.Dto;
using System.Linq;

namespace Application.Teams
{
    public class TeamAppService : CrudAppService<Team, TeamDto, int, TeamGetAllInput>, ITeamAppService
    {
        public TeamAppService(IRepository<Team> respository) : base(respository)
        {

        }

        protected override IQueryable<Team> CreateFilteredQuery(TeamGetAllInput input)
        {
            return Repository.GetAll().WhereIf(!string.IsNullOrEmpty(input.Name), model => model.Name == input.Name);
        }
    }
}
