﻿using Abp.Application.Services;
using Application.QuestionAndAnswers.Dto;

namespace Application.QuestionAndAnswers
{
    public interface IQuestionAndAnswerAppService: ICrudAppService<QuestionAndAnswerDto>
    {
    }
}
