﻿using Application.QuestionAndAnswers.Dto;
using Abp.Application.Services;
using Abp.Domain.Repositories;

namespace Application.QuestionAndAnswers
{
    public class QuestionAndAnswerAppService:CrudAppService<QuestionAndAnswer, QuestionAndAnswerDto>, IQuestionAndAnswerAppService
    {
        public QuestionAndAnswerAppService(IRepository<QuestionAndAnswer> respository) :base(respository)
        {
        }
    }
}
