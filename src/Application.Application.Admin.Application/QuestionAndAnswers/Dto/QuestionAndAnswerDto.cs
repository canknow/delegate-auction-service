﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.QuestionAndAnswers.Dto
{
    [AutoMap(typeof(QuestionAndAnswer))]
    public class QuestionAndAnswerDto : FullAuditedEntityDto
    {
        public string Question { get; set; }

        public string Answer { get; set; }
    }
}
