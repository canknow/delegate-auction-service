﻿using Application.Admin.SpreadPosterTemplates.Dto;
using Abp.Application.Services.Dto;
using Abp.Application.Services;

namespace Application.Admin.SpreadPosterTemplates
{
    public interface ISpreadPosterTemplateAppService : ICrudAppService<SpreadPosterTemplateDto>
    {
        CreateOrEditSpreadPosterTemplateDto GetSpreadPosterTemplateForCreateOrEdit(NullableIdDto input);

        CreateOrEditSpreadPosterTemplateDto CreateOrEdit(CreateOrEditSpreadPosterTemplateDto input);

        void SetAsDefault(SpreadPosterGetInput input);

        void ToggleStatus(SpreadPosterGetInput input);
    }
}
