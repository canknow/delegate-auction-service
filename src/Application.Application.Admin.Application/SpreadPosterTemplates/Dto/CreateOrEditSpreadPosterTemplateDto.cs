﻿using Application.PictureTemplates.Dto;
using Application.SpreadPosters.SpreadPosterTemplates;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;

namespace Application.Admin.SpreadPosterTemplates.Dto
{
    [AutoMap(typeof(SpreadPosterTemplate))]
    public class CreateOrEditSpreadPosterTemplateDto: NullableIdDto
    {
        public string Name { get; set; }

        public string Template { get; set; }

        public List<TemplateParameterDto> Parameters { get; set; }

        public bool IsDefault { get; set; }

        public bool Status { get; set; }
    }
}
