﻿using Abp.Application.Services;
using Application.Admin.FinanceAccounts.Dtos;

namespace Application.Admin.FinanceAccounts
{
    public interface IFinanceAccountAppService : ICrudAppService<FinanceAccountDto,int, FinanceAccountGetAllInput>
    {
    }
}
