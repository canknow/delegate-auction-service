﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Application.Admin.FinanceAccounts.Dtos;
using Application.FinanceAccounts;
using Application.FinanceAccounts.Entities;
using System.Linq;

namespace Application.Admin.FinanceAccounts
{
    [AbpAuthorize]
    public class FinanceAccountAppService : CrudAppService<FinanceAccount, FinanceAccountDto,int, FinanceAccountGetAllInput>,IFinanceAccountAppService
    {
        public FinanceAccountManager FinanceAccountManager { get; set; }
        public FinanceAccountAppService(
            IRepository<FinanceAccount> respository) 
            :base(respository)
        {
        }

        protected override IQueryable<FinanceAccount> CreateFilteredQuery(FinanceAccountGetAllInput input)
        {
            return Repository.GetAll().Where(model=>model.UserId==AbpSession.UserId.Value);
        }
    }
}
