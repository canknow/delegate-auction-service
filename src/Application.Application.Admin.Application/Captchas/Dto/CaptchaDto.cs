﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Captchas.Dto
{
    [AutoMap(typeof(Captcha))]
    public class CaptchaDto:FullAuditedEntityDto
    {
        public string Code { get; set; }

        public string ProviderKey { get; set; }

        public string Provider { get; set; }

        public VerificationState VerificationState { get; set; }
    }
}
