﻿using Abp.Application.Services;
using Application.Captchas.Dto;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace Application.Captchas
{
    public interface ICaptchaAppService : ICrudAppService<CaptchaDto, int, CaptchaGetAllInput>
    {
        Task ReSend(Entity input);
    }
}