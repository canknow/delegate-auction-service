﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Application.Captchas.Dto;
using Abp.Domain.Entities;
using System.Threading.Tasks;
using Application.Security.Recaptcha;

namespace Application.Captchas
{
    public class CaptchaAppService : CrudAppService<Captcha, CaptchaDto, int, CaptchaGetAllInput>, ICaptchaAppService
    {
        public IRecaptchaValidator RecaptchaValidator{get;set;}

        public CaptchaAppService(IRepository<Captcha> respository) : base(respository)
        {

        }

        public async Task ReSend(Entity input) {
            Captcha captcha = Repository.Get(input.Id);
            await RecaptchaValidator.ReSend(captcha);
        }
    }
}
