﻿using Application.Authorization.Users;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Application.Customers.Dtos
{
    [AutoMapFrom(typeof(User))]
    public class UserParentDto : AuditedEntityDto<long>
    {
        public string Number { get; set; }

        public string Avatar { get; set; }

        public string NickName { get; set; }

        public string UserName { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsSpreader { get; set; }

        public decimal Sales { get; set; }
    }
}
