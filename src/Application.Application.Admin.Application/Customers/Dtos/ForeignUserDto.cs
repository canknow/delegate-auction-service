﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Authorization.Users;
using System;

namespace Application.Customers.Dtos
{
    [AutoMapFrom(typeof(User))]
    public class ForeignUserDto : EntityDto<long>
    {
        public string NickName { get; set; }

        public string UserName { get; set; }

        public string Avatar { get; set; }

        public UserSource Source { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
