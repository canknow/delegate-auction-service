﻿using Application.Authorization.Users;

namespace Application.Customers.Dtos
{
    public class UserSourceStatisticsDto
    {
        public int Count { get; set; }

        public UserSource UserSource { get; set; }
    }
}
