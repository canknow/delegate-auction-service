﻿using Abp.Application.Services.Dto;

namespace Application.Customers.Dtos
{
    public class GetUserForEditInput : NullableIdDto<long>
    {
        public long? ParentUserId { get; set; }
    }
}
