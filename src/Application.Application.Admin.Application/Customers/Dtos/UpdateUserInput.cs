﻿using Application.Authorization.Users;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Application.Customers.Dtos
{
    [AutoMap(typeof(User))]
    public class UpdateUserInput : EntityDto<long>
    {
        [Required]
        [StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(User.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [StringLength(User.MaxNickNameLength)]
        public string NickName { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsSpreader { get; set; }

        public string Avatar { get; set; }

        public bool IsActive { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }
    }
}
