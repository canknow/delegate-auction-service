﻿using System;

namespace Application.Customers.Dtos
{
    public class GetUserForEditOutput
    {
        public Guid? ProfilePictureId { get; set; }

        public UserEditDto User { get; set; }
    }
}
