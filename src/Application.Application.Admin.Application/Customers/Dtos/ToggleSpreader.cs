﻿using Application.Dto;

namespace Application.Customers.Dtos
{
    public class ToggleSpreader: IdInput
    {
        public bool IsSpreader { get; set; }
    }
}
