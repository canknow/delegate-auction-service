﻿using Application.Authorization.Users;
using Abp.Application.Services.Dto;

namespace Application.Customers.Dtos
{
    public class UserGetAllInput : PagedAndSortedResultRequestDto
    {
        public string UserName { get; set; }

        public string Number { get; set; }

        public string NickName { get; set; }

        public bool? IsSpreader { get; set; }

        public UserSource? UserSource { get; set; }

        public string PhoneNumber { get; set; }
    }
}
