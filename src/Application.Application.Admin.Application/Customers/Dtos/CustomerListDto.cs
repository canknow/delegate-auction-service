﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Application.Authorization.Users;
using System;

namespace Application.Customers.Dtos
{
    [AutoMapFrom(typeof(User))]
    public class CustomerListDto : EntityDto<long>
    {
        public UserParentDto ParentUser { get; set; }

        public string Number { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string NickName { get; set; }

        public UserSource Source { get; set; }

        public string PhoneNumber { get; set; }

        public string Avatar { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationTime { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }
    }
}
