﻿namespace Application.Customers.Dtos
{
    public class CustomerDetailOutput
    {
        public CustomerDetailDto Customer { get; set; }
    }
}
