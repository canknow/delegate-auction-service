﻿namespace Application.Customers.Dtos
{
    public class BindParentInput
    {
        public long SourceUserId { get; set; }
        public long TargetUserId { get; set; }
    }
}
