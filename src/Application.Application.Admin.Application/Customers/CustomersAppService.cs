﻿using Abp;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.UI;
using Application.Authorization;
using Application.Authorization.Roles;
using Application.Authorization.Users;
using Application.Authorization.Users.NumberProviders;
using Application.Customers.Dtos;
using Application.Dto;
using Application.Spread;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Customers.Admin
{
    public class CustomersAppService
        : CrudAppService<User, CustomerDto, long, UserGetAllInput>,
        ICustomersAppService
    {
        public RoleManager RoleManager { get; set; }
        public UserManager UserManager { get; set; }
        public SpreadManager SpreadManager { get; set; }
        public INumberProvider NumberProvider { get; set; }
        public ApplicationAuthorizationHelper ApplicationAuthorizationHelper { get; set; }

        private readonly IPermissionManager _permissionManager;
        private readonly IEmailSender _emailSender;
        private readonly IUserEmailer _userEmailer;
        private readonly IBackgroundJobManager _backgroundJobManager;

        public CustomersAppService(
            IRepository<User, long> userRepository,
            IPermissionManager permissionManager,
            IEmailSender emailSender,
            IBackgroundJobManager backgroundJobManager,
            IUserEmailer userEmailer)
            : base(userRepository)
        {
            _permissionManager = permissionManager;
            _emailSender = emailSender;
            _userEmailer = userEmailer;
            _backgroundJobManager = backgroundJobManager;
        }

        public CustomerDetailOutput GetCustomerDetail(IdInput input)
        {
            CustomerDetailOutput customerDetailDto = new CustomerDetailOutput()
            {
                Customer = Repository.Get(input.Id).MapTo<CustomerDetailDto>()
            };
            return customerDetailDto;
        }

        public async Task ResetPassword(IdInput input)
        {
            User user = Repository.Get(input.Id);
            await UserManager.ChangePasswordAsync(user,User.DefaultPassword);
        }

        public List<UserSourceStatisticsDto> GetUserStatistics()
        {
            var query = from user in Repository.GetAll()
                        group user by user.Source into userGroup
                        select new UserSourceStatisticsDto()
                        {
                            Count = userGroup.Count(),
                            UserSource = userGroup.Key
                        };
            return query.ToList();
        }

        public async Task BindParent(BindParentInput input)
        {
            User sourceUser = Repository.Get(input.SourceUserId);
            User targetUser = Repository.Get(input.TargetUserId);

            try
            {
                await UserManager.BindParentAsync(sourceUser, targetUser, true, true, BindParentType.Admin);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task ToggleActive(EntityDto input)
        {
            var user = await Repository.GetAsync(input.Id);
            user.IsActive = !user.IsActive;
            await Repository.UpdateAsync(user);
        }

        public async Task SetAsSpreader(ToggleSpreader input)
        {
            if (input.IsSpreader)
            {
                await SpreadManager.SetAsSpreader(new UserIdentifier(AbpSession.TenantId, input.Id));
            }
            else
            {
                User user = Repository.Get(input.Id);
                user.IsSpreader = false;
                Repository.Update(user);
            }
        }

        protected override IQueryable<User> CreateFilteredQuery(UserGetAllInput input)
        {
            var query = Repository.GetAll()
                .Where(model => model.UserType == UserType.User)
                .WhereIf(input.IsSpreader.HasValue, model => model.IsSpreader == input.IsSpreader)
                .WhereIf(!string.IsNullOrEmpty(input.NickName), model => model.NickName.Contains(input.NickName))
                .WhereIf(!string.IsNullOrEmpty(input.UserName), model => model.UserName.Contains(input.UserName))
                .WhereIf(!string.IsNullOrEmpty(input.Number), model => model.Number.Contains(input.Number))
                .WhereIf(input.UserSource.HasValue, model => model.Source == input.UserSource);
            return query;
        }

        public async Task<PagedResultDto<CustomerDto>> GetUsers(UserGetAllInput input)
        {
            PagedResultDto<CustomerDto> pagedResultDto = base.GetAll(input);
            return pagedResultDto;
        }

        public async Task<GetUserForEditOutput> GetUserForEdit(GetUserForEditInput input)
        {
            var output = new GetUserForEditOutput
            {
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new UserEditDto
                {
                    IsActive = true,
                    Password = User.DefaultPassword,
                    ShouldChangePasswordOnNextLogin = true
                };

                if (input.ParentUserId.HasValue)
                {
                    output.User.ParentUserId = input.ParentUserId;
                    output.User.ParentUser = Repository.Get(input.ParentUserId.Value).MapTo<UserParentDto>();
                }
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);
                output.User = user.MapTo<UserEditDto>();
            }
            return output;
        }
    }
}
