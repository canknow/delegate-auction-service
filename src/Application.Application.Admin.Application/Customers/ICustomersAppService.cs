﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Application.Customers.Dtos;
using Application.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Customers.Admin
{
    public interface ICustomersAppService : ICrudAppService<CustomerDto, long, UserGetAllInput>
    {
        CustomerDetailOutput GetCustomerDetail(IdInput input);

        Task SetAsSpreader(ToggleSpreader input);

        Task BindParent(BindParentInput input);

        Task ResetPassword(IdInput input);

        List<UserSourceStatisticsDto> GetUserStatistics();

        Task<GetUserForEditOutput> GetUserForEdit(GetUserForEditInput input);

        Task ToggleActive(EntityDto input);
    }
}
