FROM microsoft/dotnet:2.1-sdk AS builder

RUN apt-get update
RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

RUN apt-get install -y --no-install-recommends libgdiplus libc6-dev 

WORKDIR /source

# copies the rest of your code
COPY . .
WORKDIR /source/src/Application.Web.Host
RUN dotnet restore
RUN dotnet publish --output /app/ --configuration Release

# Stage 2
FROM microsoft/dotnet:2.1-aspnetcore-runtime
WORKDIR /app
EXPOSE 21021
COPY --from=builder /app .
ENTRYPOINT ["dotnet", "Application.Web.Host.dll"]